package com.belosh.movieland.service.impl.util.enrichment;

import com.belosh.movieland.entity.Country;
import com.belosh.movieland.entity.Genre;
import com.belosh.movieland.entity.Movie;
import com.belosh.movieland.entity.Review;
import com.belosh.movieland.model.Currency;
import com.belosh.movieland.service.CountryService;
import com.belosh.movieland.service.EnrichService;
import com.belosh.movieland.service.GenreService;
import com.belosh.movieland.service.ReviewService;
import com.belosh.movieland.service.impl.util.CurrencyRateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.*;

@Service
public class DefaultEnrichService implements EnrichService {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private CountryService countryService;
    private GenreService genreService;
    private ReviewService reviewService;
    private CurrencyRateService currencyRateService;

    public void injectGenreAndCountry(List<Movie> movies) {
        log.info("Start injecting genre and country for movies: {}", movies);
        Set<Integer> movieIDs = new HashSet<>();
        for (Movie movie : movies) {
            movieIDs.add(movie.getId());
        }

        Map<Integer, List<Country>> movieCountriesMap = countryService.getMovieCountiesMap(movieIDs);
        Map<Integer, List<Genre>> movieGenreMap = genreService.getMovieGenreMap(movieIDs);

        for (Movie movie : movies) {
            Integer movieID = movie.getId();

            List<Country> countries = movieCountriesMap.get(movieID);
            movie.setCountries(countries);

            List<Genre> genres = movieGenreMap.get(movieID);
            movie.setGenres(genres);
        }
        log.info("Finish injecting genre and country");
    }

    public void injectMovieData(Movie movie) {
        log.info("Start injecting genre and country and review for movie: {}", movie);

        int movieId = movie.getId();

        List<Genre> movieGenres = genreService.getByMovie(movieId);
        List<Country> movieCountries = countryService.getByMovie(movieId);
        List<Review> movieReviews = reviewService.getByMovie(movieId);

        movie.setGenres(movieGenres);
        movie.setCountries(movieCountries);
        movie.setReviews(movieReviews);
        log.info("Finish injecting genre and country and review");

    }

    public void recalculatePrice(Movie movie, Currency currency) {
        double rate = currencyRateService.getRate(currency);
        double defaultPrice =  movie.getPrice();
        double recalculatedPrice = defaultPrice / rate;
        movie.setPrice(recalculatedPrice);
        log.info("Price recalculated for movie: {} with currency: {}, rate: {}, recalculated price: {}",
                movie, currency, rate, recalculatedPrice);
    }

    @Autowired
    public void setCountryService(CountryService countryService) {
        this.countryService = countryService;
    }

    @Autowired
    public void setGenreService(GenreService genreService) {
        this.genreService = genreService;
    }

    @Autowired
    public void setReviewService(ReviewService reviewService) {
        this.reviewService = reviewService;
    }

    @Autowired
    public void setCurrencyRateService(CurrencyRateService currencyRateService) {
        this.currencyRateService = currencyRateService;
    }
}
