package com.belosh.movieland.service.impl.report.generator;

import com.belosh.movieland.report.ReportUser;
import com.belosh.movieland.service.impl.report.extention.PDFDocument;
import com.itextpdf.text.*;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReportUserGenerator extends ReportGenerator<ReportUser> {
    private final Logger log = LoggerFactory.getLogger(getClass());

    @Override
    void prepareXLSXReport(Sheet sheet, List<ReportUser> reportData) {
        Row row = sheet.createRow(0);
        row.createCell(0).setCellValue("User ID");
        row.createCell(1).setCellValue("Email");
        row.createCell(2).setCellValue("Reviews Count");
        row.createCell(3).setCellValue("Avarage Rate");

        int index = 0;
        for (ReportUser reportUser : reportData) {
            row = sheet.createRow(index + 1);
            row.createCell(0).setCellValue(reportUser.getUserId());
            row.createCell(1).setCellValue(reportUser.getEmail());
            row.createCell(2).setCellValue(reportUser.getCountReviews());
            row.createCell(3).setCellValue(reportUser.getAvgRate());
            index++;
        }
    }

    @Override
    void preparePDFDocument(PDFDocument document, List<ReportUser> reportData) {
        Font font = super.pdfFont;

        try {
            Paragraph paragraph = new Paragraph("Список найболее активных пользователей", font);
            document.add(paragraph);

            for (ReportUser reportUser : reportData) {
                document.add(new Paragraph("User ID: " + reportUser.getUserId(), font));
                document.add(new Paragraph("Email: " + reportUser.getEmail(), font));
                document.add(new Paragraph("Количество обзоров: " + reportUser.getCountReviews(), font));
                document.add(new Paragraph("Средняя оценка: " + reportUser.getAvgRate(), font));
                document.add(new Paragraph("-------------------------------------------------"));
            }
        } catch (DocumentException e) {
            log.error("Unable to prepare PDF Document for User Report", e);
        }
    }
}
