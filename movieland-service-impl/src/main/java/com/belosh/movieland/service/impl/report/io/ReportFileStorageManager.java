package com.belosh.movieland.service.impl.report.io;

import com.belosh.movieland.report.ReportRequest;
import com.belosh.movieland.service.impl.report.extention.PDFDocument;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;

@Service
public class ReportFileStorageManager {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private String reportDirectoryName;
    private Path reportPath;

    public void saveFile(ReportRequest reportRequest, PDFDocument document) throws FileNotFoundException, DocumentException {
        Path path = getReportPath(reportRequest);
        FileOutputStream fileOutputStream = new FileOutputStream(path.toFile());
        PdfWriter.getInstance(document, fileOutputStream);
    }

    public void saveFile(ReportRequest reportRequest, Workbook workbook) throws IOException {
        Path path = Files.createFile(getReportPath(reportRequest));
        FileOutputStream fileOutputStream = new FileOutputStream(path.toFile());
        try (BufferedOutputStream fileOut = new BufferedOutputStream(fileOutputStream)) {
            workbook.write(fileOut);
        }
    }

    public void removeReport(ReportRequest reportRequest) {
        Path path = getReportPath(reportRequest);

        try {
            Files.deleteIfExists(path);
        } catch (IOException e) {
            log.error("Report: {} has NOT been deleted due to internal problem", path);
        }
        log.info("Report: {} has been successfully deleted", path);
    }

    public void removeAllReports() {
        try {
            Files.walk(reportPath)
                    .sorted(Comparator.reverseOrder())
                    .map(Path::toFile)
                    .peek(System.out::println)
                    .forEach(File::delete);
        } catch (IOException e) {
            throw new RuntimeException("Unable to remove list of files in reports directory: " + reportPath);
        }
    }

    public InputStream getReportFileStream(String filename) {
        log.info("Start to get report {}", filename);

        Path path = Paths.get(reportPath.toString(), filename);
        InputStream inputStream;

        try {
            FileInputStream fileInputStream = new FileInputStream(path.toFile());
            inputStream = new BufferedInputStream(fileInputStream);
        } catch (FileNotFoundException e) {
            log.error("File with path: {} was not found.", path);
            throw new RuntimeException("File with path: " + path + " was not found.");
        }

        log.info("Finish to get report {}", filename);
        return inputStream;
    }

    private Path getReportPath(ReportRequest reportRequest) {
        return Paths.get(reportPath.toString(), reportRequest.getReportFileName());
    }

    @PostConstruct
    private void init() {
        reportPath = Paths.get(System.getProperty("user.dir"), reportDirectoryName);
        if (!Files.exists(reportPath)) {
            try {
                Files.createDirectories(reportPath);
            } catch (IOException e) {
                throw new RuntimeException("Unable to create folder for reports in filesystem");
            }
        }
    }

    @Value("${report.directory}")
    public void setReportDirectoryName(String reportDirectoryName) {
        this.reportDirectoryName = reportDirectoryName;
    }


}
