package com.belosh.movieland.service.impl;

import com.belosh.movieland.dao.GenreDao;
import com.belosh.movieland.entity.Genre;
import com.belosh.movieland.service.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

@Service
public class DefaultGenreService implements GenreService {

    private GenreDao genreDao;

    @Autowired
    public DefaultGenreService(GenreDao genreDao) {
        this.genreDao = genreDao;
    }

    @Override
    public Map<Integer, List<Genre>> getMovieGenreMap(Set<Integer> movieIDs) {
        return genreDao.getMovieGenresMap(movieIDs);
    }

    @Override
    public List<Genre> getAll() {
        return genreDao.getAll();
    }

    @Override
    public List<Genre> getByMovie(int movieId) {
        return genreDao.getByMovie(movieId);
    }

}
