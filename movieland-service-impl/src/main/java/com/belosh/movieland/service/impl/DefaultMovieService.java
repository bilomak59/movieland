package com.belosh.movieland.service.impl;

import com.belosh.movieland.dao.CountryDao;
import com.belosh.movieland.dao.GenreDao;
import com.belosh.movieland.dao.MovieDao;
import com.belosh.movieland.entity.Movie;
import com.belosh.movieland.model.Currency;
import com.belosh.movieland.model.SortStrategy;
import com.belosh.movieland.service.EnrichService;
import com.belosh.movieland.service.MovieService;

import com.belosh.movieland.service.impl.util.enrichment.DefaultEnrichService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class DefaultMovieService implements MovieService {

    private MovieDao movieDao;
    private CountryDao countryDao;
    private GenreDao genreDao;
    private DefaultEnrichService enrichService;

    @Override
    public List<Movie> getAll(List<SortStrategy> sortStrategies) {
        return movieDao.getAll(sortStrategies);
    }

    @Override
    public List<Movie> getThreeRandom() {
        List<Movie> movies = movieDao.getThreeRandom();
        enrichService.injectGenreAndCountry(movies);
        return movies;
    }

    @Override
    public List<Movie> getByGenre(int genreId, List<SortStrategy> sortStrategies) {
        return movieDao.getByGenre(genreId, sortStrategies);
    }

    @Override
    public Movie getById(int movieId, Currency currency) {
        Movie movie = movieDao.getById(movieId);
        return movie;
    }

    @Override
    @Transactional
    public void add(Movie movie) {
        movieDao.add(movie);
        countryDao.addToMovie(movie);
        genreDao.addToMovie(movie);
    }

    @Override
    @Transactional
    public void update(Movie movie) {
        movieDao.update(movie);

        countryDao.removeFromMovie(movie);
        countryDao.addToMovie(movie);

        genreDao.removeFromMovie(movie);
        genreDao.addToMovie(movie);
    }

    @Override
    public List<Movie> searchByTitle(String searchWord, int page) {
        return movieDao.searchByTitle(searchWord, page);
    }

    @Override
    public List<Movie> exportMovies() {
        List<Movie> movies = movieDao.getMoviesForExport();
        enrichService.injectGenreAndCountry(movies);
        return movies;
    }

    @Override
    @Transactional
    public void importMovies(List<Movie> movies) {
        for (Movie movie : movies) {
            movieDao.add(movie);
            countryDao.addToImportedMovie(movie);
            genreDao.addToImportedMovie(movie);
        }
    }

    @Autowired
    public void setMovieDao(MovieDao movieDao) {
        this.movieDao = movieDao;
    }

    @Autowired
    public void setCountryDao(CountryDao countryDao) {
        this.countryDao = countryDao;
    }

    @Autowired
    public void setGenreDao(GenreDao genreDao) {
        this.genreDao = genreDao;
    }

    @Autowired
    public void setEnrichService(DefaultEnrichService enrichService) {
        this.enrichService = enrichService;
    }
}
