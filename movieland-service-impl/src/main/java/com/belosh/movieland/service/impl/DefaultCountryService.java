package com.belosh.movieland.service.impl;

import com.belosh.movieland.dao.CountryDao;
import com.belosh.movieland.entity.Country;
import com.belosh.movieland.entity.Genre;
import com.belosh.movieland.service.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
public class DefaultCountryService implements CountryService {

    private CountryDao countryDao;

    @Autowired
    public DefaultCountryService(CountryDao countryDao) {
        this.countryDao = countryDao;
    }

    @Override
    public Map<Integer, List<Country>> getMovieCountiesMap(Set<Integer> movieIDs) {
        return countryDao.getMovieCountiesMap(movieIDs);
    }

    @Override
    public List<Country> getByMovie(int movieId) {
        return countryDao.getByMovie(movieId);
    }

    @Override
    public List<Country> getAll() {
        return countryDao.getAll();
    }
}
