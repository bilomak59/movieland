package com.belosh.movieland.service.impl.report.generator;

import com.belosh.movieland.report.ReportOutputType;
import com.belosh.movieland.report.ReportRequest;
import com.belosh.movieland.report.ReportStatus;
import com.belosh.movieland.service.impl.report.extention.PDFDocument;
import com.belosh.movieland.service.impl.report.io.ReportFileStorageManager;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.pdf.BaseFont;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.List;

public abstract class ReportGenerator<T> {
    private final Logger log = LoggerFactory.getLogger(getClass());

    @Value("${report.font}")
    private String pdfFontPath;

    protected Font pdfFont;

    private ReportFileStorageManager reportFileStorageManager;

    public void generateReport(ReportRequest reportRequest, List<T> data) {
        ReportOutputType outputType = reportRequest.getReportOutputType();
        try {

            if (outputType.equals(ReportOutputType.PDF)) {
                generatePDFReport(reportRequest, data);

            } else if (outputType.equals(ReportOutputType.XLSX)) {
                generateXLSXReport(reportRequest, data);

            } else {
                log.error("Unknown output type of report: {}. Processing Skipped", outputType);
                reportRequest.setReportStatus(ReportStatus.FAILED);
            }

            reportRequest.setReportStatus(ReportStatus.FINISHED);

        } catch (IOException | DocumentException e) {
            reportRequest.setReportStatus(ReportStatus.FAILED);
            log.error("Unable to prepare report. Generation failed", e);
        }

    }

    private void generateXLSXReport(ReportRequest reportRequest, List<T> reportData) throws IOException {
        log.info("Start to generate XLSX report for request");

        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet(reportRequest.getReportType().toString());

        prepareXLSXReport(sheet, reportData);
        reportFileStorageManager.saveFile(reportRequest, workbook);

        log.info("Finish to generate XLSX report for request = {}", reportRequest.getReportId());
    }

    private void generatePDFReport(ReportRequest reportRequest, List<T> reportData) throws IOException, DocumentException {
        log.info("Start to generate PDF report for request");

        try (PDFDocument document = new PDFDocument()) {
            reportFileStorageManager.saveFile(reportRequest, document);
            document.open();
            preparePDFDocument(document, reportData);
        }

        log.info("Finish to generate PDF report for request = {}", reportRequest.getReportId());
    }

    abstract void prepareXLSXReport(Sheet sheet, List<T> reportData);

    abstract void preparePDFDocument(PDFDocument document, List<T> reportData);

    @Autowired
    public void setReportFileStorageManager(ReportFileStorageManager reportFileStorageManager) {
        this.reportFileStorageManager = reportFileStorageManager;
    }

    @PostConstruct
    private void init() {
        try {
            BaseFont bf = BaseFont.createFont(pdfFontPath, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            pdfFont = new Font(bf);
        } catch (DocumentException | IOException e) {
            String message = "Unable to load font for PDF report generator";
            log.error(message);
            throw new RuntimeException(message);
        }
    }
}
