package com.belosh.movieland.service.impl.report.generator;

import com.belosh.movieland.report.ReportMovie;
import com.belosh.movieland.service.impl.report.extention.PDFDocument;
import com.itextpdf.text.*;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class ReportMovieGenerator extends ReportGenerator<ReportMovie> {
    private final Logger log = LoggerFactory.getLogger(getClass());

    void prepareXLSXReport(Sheet sheet, List<ReportMovie> reportMovies) {
        Row row = sheet.createRow(0);
        row.createCell(0).setCellValue("Movie ID");
        row.createCell(1).setCellValue("Title");
        row.createCell(2).setCellValue("Description");
        row.createCell(3).setCellValue("Genres");
        row.createCell(4).setCellValue("Price");
        row.createCell(5).setCellValue("Rating");
        row.createCell(6).setCellValue("ReviewCount");

        int index = 0;
        for (ReportMovie reportMovie : reportMovies) {
            row = sheet.createRow(index + 1);
            row.createCell(0).setCellValue(reportMovie.getMovieId());
            row.createCell(1).setCellValue(reportMovie.getTitle());
            row.createCell(2).setCellValue(reportMovie.getDescription());
            row.createCell(3).setCellValue(reportMovie.getGenre());
            row.createCell(4).setCellValue(reportMovie.getPrice());
            row.createCell(5).setCellValue(reportMovie.getRating());
            row.createCell(6).setCellValue(reportMovie.getReviewsCount());
            index++;
        }
    }

    void preparePDFDocument(PDFDocument document, List<ReportMovie> reportMovies) {
        Font font = super.pdfFont;

        try {
            Paragraph paragraph = new Paragraph("Список фильмов", font);
            document.add(paragraph);

            for (ReportMovie reportMovie : reportMovies) {
                document.add(new Paragraph("ID: " + reportMovie.getMovieId() + ". Название: " + reportMovie.getTitle(), font));
                document.add(new Paragraph("Description: " + reportMovie.getDescription(), font));
                document.add(new Paragraph("Жанры: " + reportMovie.getGenre(), font));
                document.add(new Paragraph("Стоимость: " + reportMovie.getPrice(), font));
                document.add(new Paragraph("Рейтинг: " + reportMovie.getRating(), font));
                document.add(new Paragraph("Количество комментариев: " + reportMovie.getReviewsCount(), font));
                document.add(new Paragraph("------------------------------------------------------"));
            }
        } catch (DocumentException e) {
            log.error("Unable to prepare PDF Document for Movie Report", e);
        }
    }
}
