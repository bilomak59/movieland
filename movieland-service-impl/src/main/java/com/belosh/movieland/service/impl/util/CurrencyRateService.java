package com.belosh.movieland.service.impl.util;

import com.belosh.movieland.model.Currency;
import com.belosh.movieland.service.impl.vo.RateVO;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

@Service
public class CurrencyRateService {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private final ReadWriteLock readWriteLock = new ReentrantReadWriteLock();
    private final Lock writeLock = readWriteLock.writeLock();
    private final Lock readLock = readWriteLock.readLock();

    private final Map<String, Double> currencyRateMap = new HashMap<>();

    private String nbuApi;
    private RestTemplate restTemplate;

    @PostConstruct
    @Scheduled(cron = "${currency.rate.cache.refresh.cron}")
    public void refreshRates() {
        log.info("Start get all currency rates from NBU API");
        long startTime = System.currentTimeMillis();

        RateVO[] rates = restTemplate.getForObject(nbuApi, RateVO[].class);

        if (rates == null) {
            throw new RuntimeException("Failed to get rates from NBU API");
        }

        try {
            writeLock.lock();
            for (RateVO rateVO : rates) {
                currencyRateMap.put(rateVO.getCurrencyCode(), rateVO.getRate());
            }
        } finally {
            writeLock.unlock();
        }

        log.info("Current rates: {}", Arrays.toString(rates));
        log.info("Finish get all currency rates from NBU API. It took {} ms", System.currentTimeMillis() - startTime);
    }

    public double getRate(Currency currency) {
        try {
            readLock.lock();
            return currencyRateMap.get(currency.getName());
        } finally {
            readLock.unlock();
        }
    }

    @Value("${currency.rate.nbu.api}")
    public void setNbuApi(String nbuApi) {
        this.nbuApi = nbuApi;
    }

    @Autowired
    public void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }
}
