package com.belosh.movieland.service.impl.cache;

import com.belosh.movieland.entity.Movie;
import com.belosh.movieland.model.Currency;
import com.belosh.movieland.model.SortStrategy;
import com.belosh.movieland.service.MovieService;
import com.belosh.movieland.service.impl.util.enrichment.DefaultEnrichService;
import com.belosh.movieland.service.impl.util.MovieRateService;
import com.belosh.movieland.service.impl.util.enrichment.ParallelEnrichmentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Service;

import java.lang.ref.SoftReference;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Primary
@Service
@ManagedResource(objectName = "bean:name=movieCache")
public class MovieCache implements MovieService {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private final Map<Integer, SoftReference<Movie>> movieCache = new ConcurrentHashMap<>();

    private MovieService movieService;
    private DefaultEnrichService defaultEnrichService;
    private ParallelEnrichmentService parallelEnrichmentService;
    private MovieRateService movieRateService;

    @Override
    public Movie getById(int movieId, Currency currency) {
        log.info("Start getting movie from cache by ID: {}", movieId);
        long startTime = System.currentTimeMillis();

        Movie cachedMovie;
        SoftReference<Movie> reference =  movieCache.get(movieId);

        if (reference == null || (cachedMovie = reference.get()) == null) { // if reference missed or lose its value then...
            log.info("Movie not found in cache and will be loaded from database");

            cachedMovie = movieService.getById(movieId, currency);
            movieCache.put(movieId, new SoftReference<>(cachedMovie));
        }

        log.info("Init enriching movie with id: {}", movieId);
        parallelEnrichmentService.injectMovieData(cachedMovie);

        // Create copy of movie in order to avoid cache corruption
        Movie movie = new Movie(cachedMovie);

        if (!Currency.UAH.equals(currency)) {
            defaultEnrichService.recalculatePrice(movie, currency);
        }

        double rating = movieRateService.getActualRating(movieId);
        movie.setRating(rating);

        log.info("Finished getting movie from cache by ID. Elapsed time: {}", System.currentTimeMillis() - startTime);
        return movie;
    }

    @Override
    public void update(Movie movie) {
        movieService.update(movie);

        int movieId = movie.getId();
        if (movieCache.containsKey(movieId)) {
            movieCache.put(movieId, new SoftReference<>(movie));
        }
    }

    @ManagedOperation
    public void invalidateCache() {
        log.info("JMX cache invalidation requested for movies");
        movieCache.clear();
        log.info("JMX cache invalidation requested for movies successfully finished");
    }

    /* BELOW METHODS WERE NOT CHANGED */

    @Override
    public List<Movie> searchByTitle(String searchWord, int page) {
        return movieService.searchByTitle(searchWord, page);
    }

    @Override
    public List<Movie> exportMovies() {
        return movieService.exportMovies();
    }

    @Override
    public void importMovies(List<Movie> movies) {
        movieService.importMovies(movies);
    }

    @Override
    public List<Movie> getAll(List<SortStrategy> sortStrategies) {
        return movieService.getAll(sortStrategies);
    }

    @Override
    public List<Movie> getByGenre(int genreId, List<SortStrategy> sortStrategies) {
        return movieService.getByGenre(genreId, sortStrategies);
    }

    @Override
    public void add(Movie movie) {
        movieService.add(movie);
    }

    @Override
    public List<Movie> getThreeRandom() {
        return movieService.getThreeRandom();
    }

    @Autowired
    public void setMovieService(MovieService movieService) {
        this.movieService = movieService;
    }

    @Autowired
    public void setDefaultEnrichService(DefaultEnrichService defaultEnrichService) {
        this.defaultEnrichService = defaultEnrichService;
    }

    @Autowired
    public void setMovieRateService(MovieRateService movieRateService) {
        this.movieRateService = movieRateService;
    }

    @Autowired
    public void setParallelEnrichmentService(ParallelEnrichmentService parallelEnrichmentService) {
        this.parallelEnrichmentService = parallelEnrichmentService;
    }
}
