package com.belosh.movieland.service.impl;

import com.belosh.movieland.dao.ReviewDao;
import com.belosh.movieland.entity.Review;
import com.belosh.movieland.service.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DefaultReviewService implements ReviewService {

    private ReviewDao reviewDao;

    @Autowired
    public DefaultReviewService(ReviewDao reviewDao) {
        this.reviewDao = reviewDao;
    }

    @Override
    public List<Review> getByMovie(int id) {
        return reviewDao.getByMovie(id);
    }

    @Override
    public void add(Review review) {
        reviewDao.add(review);
    }
}
