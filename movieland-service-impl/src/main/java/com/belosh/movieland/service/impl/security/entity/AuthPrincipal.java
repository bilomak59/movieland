package com.belosh.movieland.service.impl.security.entity;

import com.belosh.movieland.entity.User;

import java.security.Principal;
import java.time.LocalDateTime;

public class AuthPrincipal implements Principal {
    private User user;
    private LocalDateTime expire;

    public AuthPrincipal(User user) {
        this.user = user;
    }

    @Override
    public String getName() {
        return user.getNickname();
    }

    public User getUser() {
        return user;
    }

    public LocalDateTime getExpire() {
        return expire;
    }

    public void setExpire(LocalDateTime expire) {
        this.expire = expire;
    }
}
