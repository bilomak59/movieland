package com.belosh.movieland.service.impl.security;

import com.belosh.movieland.entity.User;
import com.belosh.movieland.service.impl.security.entity.AuthPrincipal;
import com.belosh.movieland.service.impl.security.exception.AuthRequiredException;
import com.belosh.movieland.service.impl.security.exception.UuidExpirationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Service
@ManagedResource(objectName = "bean:name=securityService")
public class SecurityService {
    private final Logger log = LoggerFactory.getLogger(getClass());
    private final Map<String, AuthPrincipal> UUID_PRINCIPAL_CACHE = new ConcurrentHashMap<>();

    @Value("${security.principal.default.expire.hours}")
    private int defaultPrincipleExpireHours;

    public AuthPrincipal getPrincipalFromCache(String uuid) {
        log.debug("Getting principal by UUID: ", uuid);
        return UUID_PRINCIPAL_CACHE.compute(uuid, this::computePrincipal);
    }

    public void removeFromCache(String uuid) {
        AuthPrincipal principal = UUID_PRINCIPAL_CACHE.remove(uuid);
        log.info("User with nickname {} and session uuid {} has been removed from cache", principal.getName(), uuid);
    }

    public String cacheAuthUser(User user) {
        String uuid = UUID.randomUUID().toString();

        AuthPrincipal principal = new AuthPrincipal(user);
        prolongExpire(principal);

        UUID_PRINCIPAL_CACHE.put(uuid, principal);
        log.info("User with nickname {} and session uuid {} cached", principal.getName(), uuid);

        return uuid;
    }

    private AuthPrincipal computePrincipal(String uuid, AuthPrincipal principal) {
        if (principal == null) {
            String message = String.format("Provided UUID: %s is not valid. Authentication required.", uuid);
            log.warn(message);
            throw new AuthRequiredException(message);
        }

        if (principal.getExpire().isBefore(LocalDateTime.now())) {
            removeFromCache(uuid);
            String message = String.format("Provided UUID: %s has been expired.", uuid);
            log.warn(message);
            throw new UuidExpirationException(message);
        }

        prolongExpire(principal);
        return principal;
    }

    @ManagedOperation
    public void invalidateCache() {
        log.info("JMX Cache invalidation stated for SecurityService");
        UUID_PRINCIPAL_CACHE.clear();
        log.info("JMX Cache invalidation finished for SecurityService");
    }

    private void prolongExpire(AuthPrincipal principal) {
        LocalDateTime prolongedTime = LocalDateTime.now().plusHours(defaultPrincipleExpireHours);
        principal.setExpire(prolongedTime);
        log.info("Session for user: {} has been prolonged for: {} hours", principal.getName(), prolongedTime);
    }
}
