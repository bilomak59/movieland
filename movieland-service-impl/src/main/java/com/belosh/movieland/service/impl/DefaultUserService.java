package com.belosh.movieland.service.impl;

import com.belosh.movieland.dao.UserDao;
import com.belosh.movieland.entity.User;
import com.belosh.movieland.model.Credentials;
import com.belosh.movieland.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class DefaultUserService implements UserService {

    private UserDao userDao;

    @Autowired
    public DefaultUserService(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public User getUserByCredentials(Credentials credentials) {
        return userDao.getUserByCredentials(credentials);
    }
}
