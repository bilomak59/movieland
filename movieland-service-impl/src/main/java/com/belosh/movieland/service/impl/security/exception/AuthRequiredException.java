package com.belosh.movieland.service.impl.security.exception;

public class AuthRequiredException extends RuntimeException {
    public AuthRequiredException(String message) {
        super(message);
    }
}
