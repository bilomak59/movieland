package com.belosh.movieland.service.impl;

import com.belosh.movieland.dao.ReportDao;
import com.belosh.movieland.entity.User;
import com.belosh.movieland.report.*;
import com.belosh.movieland.service.EmailService;
import com.belosh.movieland.service.ReportService;
import com.belosh.movieland.service.impl.report.generator.ReportMovieGenerator;
import com.belosh.movieland.service.impl.report.generator.ReportUserGenerator;
import com.belosh.movieland.service.impl.report.io.ReportFileStorageManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Service
@ManagedResource(objectName = "bean:name=defaultReportCache")
public class DefaultReportService implements ReportService {
    private final Logger log = LoggerFactory.getLogger(getClass());

    private final Map<String, ReportRequest> REPORT_REQUEST_CACHE = new ConcurrentHashMap<>();

    private String appApiLink;

    private ReportDao reportDao;
    private ReportMovieGenerator reportMovieGenerator;
    private ReportUserGenerator reportUserGenerator;
    private ReportFileStorageManager reportFileStorageManager;
    private EmailService emailService;

    @Override
    public void addReportRequest(ReportRequest reportRequest) {
        reportRequest.setReportStatus(ReportStatus.NEW);

        String reportId = UUID.randomUUID().toString();
        reportRequest.setReportId(reportId);

        REPORT_REQUEST_CACHE.put(reportId, reportRequest);
        log.info("Saved report to cache by ID: {}", reportId);
    }

    @Override
    public void removeReport(String reportId) {
        log.info("Start removing report with ID: {} from cache and file from file store", reportId);

        ReportRequest cachedReportRequest = REPORT_REQUEST_CACHE.remove(reportId);

        log.info("Removed report form cache by ID: {}. Report Request: {}", reportId, cachedReportRequest);
    }

    @Override
    public List<ReportRequest> checkStatus(User user) {
        log.info("Start collecting active Report Requests for user: {}", user);
        List<ReportRequest> requests = REPORT_REQUEST_CACHE.entrySet().stream()
                .filter(entry -> user.equals(entry.getValue().getRequester()))
                .map(Map.Entry::getValue)
                .collect(Collectors.toList());
        log.info("Finished collecting active Report Requests for user: {}", user);
        return requests;
    }

    @Override
    public InputStream getReport(String filename) {
        return reportFileStorageManager.getReportFileStream(filename);
    }

    @Override
    public List<ReportRequest> getReportLinks(User user) {
        return reportDao.getFinishedReportRequests(user);
    }

    @Scheduled(fixedRateString = "${scheduler.report.process.fixed.rate}",
            initialDelayString = "${scheduler.report.process.fixed.rate}")
    private void processReportRequest() {
        log.info("Start processing report requests from cache");
        long startTime = System.currentTimeMillis();

        REPORT_REQUEST_CACHE.forEach((reportId, reportRequest) -> {
            log.info("Start processing for ReportRequest with ID: {}", reportId);

            ReportStatus reportStatus = reportRequest.getReportStatus();
            if (reportStatus.equals(ReportStatus.NEW)) {
                generateReport(reportRequest);
                configureReportLink(reportRequest);
                reportDao.saveReportDetails(reportRequest);
                emailService.sendEmail(reportRequest);
            } else {
                log.warn("Unable to process ReportRequest with ID: {}, due to wrong status: {}", reportId, reportStatus);
            }
        });

        log.info("Finish processing report requests from cache. It took: {} ms", System.currentTimeMillis() - startTime);
    }

    private void generateReport(ReportRequest reportRequest) {
        String reportId = reportRequest.getReportId();
        log.info("Start processing report with ID: {}", reportId);

        reportRequest.setReportStatus(ReportStatus.IN_PROGRESS);
        ReportType reportType = reportRequest.getReportType();

        try {
            if (reportType.equals(ReportType.ALL_MOVIES)) {
                List<ReportMovie> allMovies = reportDao.getAllMovies();
                reportMovieGenerator.generateReport(reportRequest, allMovies);

            } else if (reportType.equals(ReportType.ADDED_DURING_PERIOD)) {
                LocalDate period = reportRequest.getPeriod();
                List<ReportMovie> moviesByPeriod = reportDao.getMoviesByPeriod(period);
                reportMovieGenerator.generateReport(reportRequest, moviesByPeriod);

            } else if (reportType.equals(ReportType.TOP_ACTIVE_USERS)) {
                List<ReportUser> topActiveUsers = reportDao.getTopActiveUsers();
                reportUserGenerator.generateReport(reportRequest, topActiveUsers);

            } else {
                log.error("Unknown type of report: {}. Processing Skipped", reportType);
                reportRequest.setReportStatus(ReportStatus.FAILED);
            }
            log.info("Finish processing report with ID: {}", reportId);
        } catch (Exception e) {
            log.error("Report processing failed due to: ", e);
            reportRequest.setReportStatus(ReportStatus.FAILED);
        } finally {
            REPORT_REQUEST_CACHE.remove(reportId);
        }
    }

    private void configureReportLink(ReportRequest reportRequest) {
        String reportLink = appApiLink + reportRequest.getReportFileName() + "/";
        reportRequest.setReportLink(reportLink);
    }

    @Scheduled(cron = "${scheduler.report.cleanup.cron}")
    private void cleanupReports() {
        log.info("Start removing all reports by CRON scheduler");
        long startTime = System.currentTimeMillis();

        reportFileStorageManager.removeAllReports();

        log.info("Finish removing all reports by CRON scheduler. It took: {} ms", System.currentTimeMillis() - startTime);
    }

    @ManagedOperation
    public void invalidateCache() {
        log.info("Start invalidation for REPORT_REQUEST_CACHE");

        REPORT_REQUEST_CACHE.forEach((reportId, reportRequest) -> {
            log.info("Canceling ReportRequest with ID: {}", reportId);
            reportRequest.setReportStatus(ReportStatus.CANCELED);
        });

        log.info("Finished invalidation for REPORT_REQUEST_CACHE");

    }

    @Value("${report.api.link}")
    public void setAppApiLink(String appApiLink) {
        this.appApiLink = appApiLink;
    }

    @Autowired
    public void setReportDao(ReportDao reportDao) {
        this.reportDao = reportDao;
    }

    @Autowired
    public void setReportMovieGenerator(ReportMovieGenerator reportMovieGenerator) {
        this.reportMovieGenerator = reportMovieGenerator;
    }

    @Autowired
    public void setReportUserGenerator(ReportUserGenerator reportUserGenerator) {
        this.reportUserGenerator = reportUserGenerator;
    }

    @Autowired
    public void setReportFileStorageManager(ReportFileStorageManager reportFileStorageManager) {
        this.reportFileStorageManager = reportFileStorageManager;
    }

    @Autowired
    public void setEmailService(EmailService emailService) {
        this.emailService = emailService;
    }
}
