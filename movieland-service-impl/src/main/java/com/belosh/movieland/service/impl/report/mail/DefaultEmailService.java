package com.belosh.movieland.service.impl.report.mail;

import com.belosh.movieland.report.ReportRequest;
import com.belosh.movieland.service.EmailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Service
public class DefaultEmailService implements EmailService {
    private final Logger log = LoggerFactory.getLogger(getClass());

    private String sourceAddress;

    private JavaMailSender mailSender;

    @Override
    public void sendEmail(ReportRequest reportRequest) {
        log.info("Start to create email message");

        String targetAddress = reportRequest.getRequester().getEmail();
        String content = generateContent(reportRequest);

        try {
            MimeMessage mimeMessage = mailSender.createMimeMessage();
            MimeMessageHelper mailMessage = new MimeMessageHelper(mimeMessage);
            mailMessage.setTo(targetAddress);
            mailMessage.setFrom(sourceAddress);

            mailMessage.setSubject("Movieland Report");
            mailMessage.setText(content);

            mailSender.send(mimeMessage);

        } catch (MessagingException e) {
            log.error("Unable to create email for ReportRequest: {}", reportRequest);
        }

        log.info("Finish to create email message");
    }

    private String generateContent(ReportRequest reportRequest) {
        String nickname = reportRequest.getRequester().getNickname();
        String reportLink = reportRequest.getReportLink();

        return "Dear " + nickname + "," +
                "\n\nPlease find below link for requested report:" +
                "\n" + reportLink;
    }



    @Autowired
    public void setMailSender(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    @Value("${mail.username}")
    public void setSourceAddress(String sourceAddress) {
        this.sourceAddress = sourceAddress;
    }

}
