package com.belosh.movieland.service.impl.security.exception;

public class UuidExpirationException extends RuntimeException {
    public UuidExpirationException(String message) {
        super(message);
    }
}
