package com.belosh.movieland.service.impl.util.enrichment;

import com.belosh.movieland.entity.Movie;
import com.belosh.movieland.service.CountryService;
import com.belosh.movieland.service.EnrichService;
import com.belosh.movieland.service.GenreService;
import com.belosh.movieland.service.ReviewService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.concurrent.*;

@Service
public class ParallelEnrichmentService implements EnrichService{

    private final Logger log = LoggerFactory.getLogger(getClass());
    
    private Executor threadPoolExecutor;
    private CountryService countryService;
    private GenreService genreService;
    private ReviewService reviewService;

    private long enrichmentTaskTimeout;

    /**
     *
     * @param movie
     * @return TRUE if enrichment finished successfully otherwise false.
     */
    public void injectMovieData(Movie movie) {
        int movieId = movie.getId();
        log.info("Start injection of movie data for movie with id: {}", movieId);

        CompletableFuture<Void> futureCountries =
                CompletableFuture.supplyAsync(() -> countryService.getByMovie(movieId), threadPoolExecutor)
                        .thenAccept(movie::setCountries);

        CompletableFuture<Void> futureGenres =
                CompletableFuture.supplyAsync(() -> genreService.getByMovie(movieId), threadPoolExecutor)
                        .thenAccept(movie::setGenres);

        CompletableFuture<Void> futureReviews =
                CompletableFuture.supplyAsync(() -> reviewService.getByMovie(movieId), threadPoolExecutor)
                        .thenAccept(movie::setReviews);
        
        CompletableFuture<Void> future = CompletableFuture.allOf(futureCountries, futureGenres, futureReviews);
        try {
            future.get(enrichmentTaskTimeout, TimeUnit.SECONDS);
            log.info("Finished injection of movie data for movie with id: {}", movieId);

        } catch (InterruptedException | ExecutionException e) {
            log.error("Parallel movie data enricher failed during execution", e);

        } catch (TimeoutException e) {
            log.warn("Parallel movie data enricher was unable to finish enrichment within {} seconds", enrichmentTaskTimeout);
            verifyFinished(futureCountries, "country");
            verifyFinished(futureGenres, "genre");
            verifyFinished(futureReviews, "review");
        }
    }

    private void verifyFinished(CompletableFuture<?> future, String purpose) {
        if (!future.isDone()) {
            log.warn("Enrichment for {} has not been finished", purpose);

//            CompletableFuture cancel works another way and doesn't throws Interrupted exception
//            if (!future.cancel(true)) {
//                log.error("Unable to cancel enrichment for {}", purpose);
//            }
        }
    }

    @Autowired
    public void setThreadPoolExecutor(ThreadPoolTaskExecutor threadPoolExecutor) {
        this.threadPoolExecutor = threadPoolExecutor;
    }

    @Autowired
    public void setCountryService(CountryService countryService) {
        this.countryService = countryService;
    }

    @Autowired
    public void setGenreService(GenreService genreService) {
        this.genreService = genreService;
    }

    @Autowired
    public void setReviewService(ReviewService reviewService) {
        this.reviewService = reviewService;
    }

    @Value("${executor.enrichment.task.timeout}")
    public void setEnrichmentTaskTimeout(long enrichmentTaskTimeout) {
        this.enrichmentTaskTimeout = enrichmentTaskTimeout;
    }
}
