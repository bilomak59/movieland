package com.belosh.movieland.service.impl.util;

import com.belosh.movieland.dao.RatingDao;
import com.belosh.movieland.model.TotalRating;
import com.belosh.movieland.model.UserRating;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

@Service
public class MovieRateService {
    private final Logger log = LoggerFactory.getLogger(getClass());

    private final ReadWriteLock readWriteLock = new ReentrantReadWriteLock();
    private final Lock writeLock = readWriteLock.writeLock();
    private final Lock readLock = readWriteLock.readLock();

    private volatile Map<Integer, TotalRating> RATING = new ConcurrentHashMap<>();
    private Map<Integer, List<UserRating>> RATING_BUFFER = new ConcurrentHashMap<>();

    private RatingDao ratingDao;

    public void rateMovie(int movieId, UserRating userRating) {
        try {
            writeLock.lock();

            List<UserRating> userRatings = RATING_BUFFER.computeIfAbsent(movieId, k -> new ArrayList<>());
            userRatings.add(userRating);

            TotalRating totalRating = RATING.computeIfAbsent(movieId, k -> new TotalRating(0, 0));

            recalculateRating(userRatings, totalRating);
        } finally {
            writeLock.unlock();
        }
    }

    public double getActualRating(int movieId) {
        try {
            readLock.lock();
            return RATING.get(movieId).getRating();
        } finally {
            readLock.unlock();
        }
    }

    private void recalculateRating(List<UserRating> userRatings, TotalRating totalRating) {
        double bufferRatingSum = 0;
        for (UserRating userRating : userRatings) {
            bufferRatingSum += userRating.getRating();
        }

        int votes = totalRating.getVotes() + userRatings.size(); // Overall votes plus buffered votes cunt
        double rating = totalRating.getRating() + bufferRatingSum / votes;

        totalRating.setRating(rating);
        totalRating.setVotes(votes);
    }

    @Scheduled(fixedRateString = "${scheduler.rating.cache.fixed.rate}",
            initialDelayString = "${scheduler.rating.cache.fixed.rate}")
    @Transactional
    void saveRatings() {
        log.info("Start saving rating data to database");
        long startTime = System.currentTimeMillis();

        if (!RATING_BUFFER.isEmpty()) {
            ratingDao.deleteRelatedRatings(RATING_BUFFER); // clean up previous rating for movieId <-> userId
            ratingDao.saveBufferRatings(RATING_BUFFER);

            RATING_BUFFER.clear();

            loadActualRating();
        } else {
            log.info("Empty rate buffer. Nothing to save.");
        }

        log.info("Finish saving rating data to database: It took {} ms", System.currentTimeMillis() - startTime);
    }

    @PostConstruct
    void loadActualRating() {
        log.info("Start fetching rating data from database to cache");
        long startTime = System.currentTimeMillis();
        RATING = ratingDao.getRatings();
        log.info("Finish fetching rating data from database to cache: It took {} ms", System.currentTimeMillis() - startTime);
    }

    @Autowired
    public void setRatingDao(RatingDao ratingDao) {
        this.ratingDao = ratingDao;
    }
}
