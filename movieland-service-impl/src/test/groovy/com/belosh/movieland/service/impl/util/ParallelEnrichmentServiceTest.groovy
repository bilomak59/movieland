package com.belosh.movieland.service.impl.util

import com.belosh.movieland.entity.Country
import com.belosh.movieland.entity.Genre
import com.belosh.movieland.entity.Movie
import com.belosh.movieland.entity.Review
import com.belosh.movieland.entity.User
import com.belosh.movieland.service.CountryService
import com.belosh.movieland.service.GenreService
import com.belosh.movieland.service.ReviewService
import com.belosh.movieland.service.impl.util.enrichment.ParallelEnrichmentService
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.invocation.InvocationOnMock
import org.mockito.stubbing.Answer
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor

import static org.mockito.Mockito.when

class ParallelEnrichmentServiceTest {

    @Mock
    private GenreService mockGenreService

    @Mock
    private CountryService mockCountryService

    @Mock
    private ReviewService mockReviewService

    @InjectMocks
    private ParallelEnrichmentService enrichService

    def movieGenreMap
    def movieCountriesMap

    def movieGenreList
    def movieCountryList
    def movieReviewList

    def review

    @Before
    void setup() {
        MockitoAnnotations.initMocks(this)

        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor()
        taskExecutor.initialize()
        enrichService.setThreadPoolExecutor(taskExecutor)

        enrichService.setEnrichmentTaskTimeout(5)

        def countryUSA = new Country()
        countryUSA.setId(1)
        countryUSA.setName('США')
        def countryFrance = new Country()
        countryFrance.setId(2)
        countryFrance.setName('Франция')

        def genreDramma = new Genre(1, 'драма')
        def genreCrime = new Genre(2, 'криминал')
        def genreComedy = new Genre(3,'комедия')
        def genreBiography = new Genre(4, 'биография')

        def user = new User()
        user.setId(1)
        user.setNickname("Maks")

        review = new Review()
        review.setId(1)
        review.setText("Acceptable with bear")
        review.setUser(user)

        movieCountriesMap = [1 : [countryUSA], 2 : [countryFrance]]
        movieGenreMap = [1 : [genreDramma, genreCrime], 2 : [genreDramma, genreComedy, genreBiography]]

        movieGenreList = [genreDramma, genreCrime]
        movieCountryList = [countryUSA, countryFrance]
        movieReviewList = [review]
    }

    @Test
    void injectMovieDataTestPositive() {
        int movieId = 1;

        Movie movie = new Movie()
        movie.setId(movieId)

        when(mockReviewService.getByMovie(movieId)).thenReturn(movieReviewList)
        when(mockCountryService.getByMovie(movieId)).thenReturn(movieCountryList)
        when(mockGenreService.getByMovie(movieId)).then(new Answer<Object>() {
            @Override
            Object answer(InvocationOnMock invocation) throws Throwable {
                try {
                    Thread.sleep(7000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                return movieGenreList
            }
        })

        enrichService.injectMovieData(movie)

        List<Country> movieCountries = movie.getCountries()
        Assert.assertEquals(2, movieCountries.size())
        Assert.assertEquals('США', movieCountries.get(0).getName())
        Assert.assertEquals('Франция', movieCountries.get(1).getName())

        List<Genre> movieGenres = movie.getGenres()
        Assert.assertNull(movieGenres)
//        Assert.assertEquals(2, movieGenres.size())
//        Assert.assertEquals('драма', movieGenres.get(0).getName())
//        Assert.assertEquals('криминал', movieGenres.get(1).getName())

        List<Review> movieReviews = movie.getReviews()
        Assert.assertEquals(1, movieReviews.size())
        Assert.assertEquals(review, movieReviews.get(0))

    }
}
