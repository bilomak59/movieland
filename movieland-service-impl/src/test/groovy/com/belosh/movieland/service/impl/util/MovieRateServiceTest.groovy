package com.belosh.movieland.service.impl.util

import com.belosh.movieland.dao.RatingDao
import com.belosh.movieland.model.TotalRating
import com.belosh.movieland.service.impl.SpringTestConfiguration
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.MockitoAnnotations

import static org.mockito.Mockito.when

class MovieRateServiceTest {

    private Map<Integer, TotalRating> RATING = new HashMap<>();

    @Mock
    private RatingDao mockRatingDao

    @InjectMocks
    private MovieRateService movieRateService

    @Before
    void setup() {
        MockitoAnnotations.initMocks(this)
        TotalRating totalRating = new TotalRating(100, 7.7)
        RATING.put(1,totalRating)
    }

    @Test
    void testRateMovie() {
        movieRateService.rateMovie(1, 1, 9.1)
        def rating = movieRateService.getActualRating(1)
        Assert.assertEquals(9.1, rating, 0.01)

        movieRateService.rateMovie(1, 1, 4)
        rating = movieRateService.getActualRating(1)
        Assert.assertEquals(9.1, rating, 0.01)
    }

    @Test
    void testGetActualRating() {
        MovieRateService movieRateService = new MovieRateService()
        when(mockRatingDao.getRatings()).thenReturn(RATING)

        def rating = movieRateService.getActualRating(1)
        Assert.assertEquals(7.7, rating, 0.01)
    }
}
