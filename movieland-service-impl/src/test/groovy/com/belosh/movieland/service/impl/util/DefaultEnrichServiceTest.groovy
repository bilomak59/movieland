package com.belosh.movieland.service.impl.util

import com.belosh.movieland.entity.Country
import com.belosh.movieland.entity.Genre
import com.belosh.movieland.entity.Movie
import com.belosh.movieland.entity.Review
import com.belosh.movieland.entity.User
import com.belosh.movieland.service.CountryService
import com.belosh.movieland.service.GenreService
import com.belosh.movieland.service.ReviewService
import com.belosh.movieland.service.impl.util.enrichment.DefaultEnrichService
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.invocation.InvocationOnMock
import org.mockito.stubbing.Answer

import static org.mockito.Mockito.when

class DefaultEnrichServiceTest {

    @Mock
    private GenreService mockGenreService

    @Mock
    private CountryService mockCountryService

    @Mock
    private ReviewService mockReviewService

    @InjectMocks
    private DefaultEnrichService enrichService

    def movieGenreMap
    def movieCountriesMap

    def movieGenreList
    def movieCountryList
    def movieReviewList

    def review

    @Before
    void setup() {
        MockitoAnnotations.initMocks(this)

        def countryUSA = new Country()
        countryUSA.setId(1)
        countryUSA.setName('США')
        def countryFrance = new Country()
        countryFrance.setId(2)
        countryFrance.setName('Франция')

        def genreDramma = new Genre(1, 'драма')
        def genreCrime = new Genre(2, 'криминал')
        def genreComedy = new Genre(3,'комедия')
        def genreBiography = new Genre(4, 'биография')

        def user = new User()
        user.setId(1)
        user.setNickname("Maks")

        review = new Review()
        review.setId(1)
        review.setText("Acceptable with bear")
        review.setUser(user)

        movieCountriesMap = [1 : [countryUSA], 2 : [countryFrance]]
        movieGenreMap = [1 : [genreDramma, genreCrime], 2 : [genreDramma, genreComedy, genreBiography]]

        movieGenreList = [genreDramma, genreCrime]
        movieCountryList = [countryUSA, countryFrance]
        movieReviewList = [review]
    }

    @Test
    void testInjectGenreAndCountry() {
        def set = [1, 2].toSet()

        when(mockGenreService.getMovieGenreMap(set)).thenReturn(movieGenreMap)
        when(mockCountryService.getMovieCountiesMap(set)).thenReturn(movieCountriesMap)

        Movie firstMovie = new Movie()
        Movie secondMovie = new Movie()

        firstMovie.setId(1)
        secondMovie.setId(2)

        def listMovies= [firstMovie, secondMovie]
        enrichService.injectGenreAndCountry(listMovies)

        /* Checking Countries */
        List<Country> firstMovieCountries = firstMovie.getCountries()
        Assert.assertEquals(1, firstMovieCountries.size())
        Assert.assertEquals('США', firstMovieCountries.get(0).getName())

        List<Country> secondMovieCountries = secondMovie.getCountries()
        Assert.assertEquals(1, secondMovieCountries.size())
        Assert.assertEquals('Франция', secondMovieCountries.get(0).getName())

        /* Checking Genres */
        List<Genre> firstMovieGenres = firstMovie.getGenres()
        Assert.assertEquals(2, firstMovieGenres.size())
        Assert.assertEquals('драма', firstMovieGenres.get(0).getName())
        Assert.assertEquals('криминал', firstMovieGenres.get(1).getName())

        List<Genre> secondMovieGenres = secondMovie.getGenres()
        Assert.assertEquals(3, secondMovieGenres.size())
        Assert.assertEquals('драма', secondMovieGenres.get(0).getName())
        Assert.assertEquals('комедия', secondMovieGenres.get(1).getName())
        Assert.assertEquals('биография', secondMovieGenres.get(2).getName())
    }

    @Test
    void injectGenreCountryReview() {
        int movieId = 1

        when(mockGenreService.getByMovie(movieId)).thenReturn(movieGenreList)
        when(mockCountryService.getByMovie(movieId)).thenReturn(movieCountryList)
        when(mockReviewService.getByMovie(movieId)).thenReturn(movieReviewList)

        Movie movie = new Movie()
        movie.setId(movieId)

        enrichService.injectMovieData(movie)

        List<Country> movieCountries = movie.getCountries()
        Assert.assertEquals(2, movieCountries.size())
        Assert.assertEquals('США', movieCountries.get(0).getName())
        Assert.assertEquals('Франция', movieCountries.get(1).getName())

        List<Genre> movieGenres = movie.getGenres()
        Assert.assertEquals(2, movieGenres.size())
        Assert.assertEquals('драма', movieGenres.get(0).getName())
        Assert.assertEquals('криминал', movieGenres.get(1).getName())

        List<Review> movieReviews = movie.getReviews()
        Assert.assertEquals(1, movieReviews.size())
        Assert.assertEquals(review, movieReviews.get(0))

    }

    @Test(expected = RuntimeException.class)
    void injectMovieDataTestNegative() {
        int movieId = 1;

        Movie movie = new Movie()
        movie.setId(movieId)

        when(mockGenreService.getByMovie(movieId)).thenReturn(movieGenreList)
        when(mockCountryService.getByMovie(movieId)).thenReturn(movieCountryList)
        //when(mockReviewService.getByMovie(movieId)).thenReturn(movieReviewList)
        when(mockReviewService.getByMovie(movieId)).then(new Answer<Object>() {
            @Override
            Object answer(InvocationOnMock invocation) throws Throwable {
                Thread.sleep(20000)
                return movieReviewList
            }
        })

        enrichService.injectMovieData(movie)
    }
}
