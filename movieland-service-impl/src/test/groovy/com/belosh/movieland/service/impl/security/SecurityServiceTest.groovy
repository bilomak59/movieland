package com.belosh.movieland.service.impl.security

import com.belosh.movieland.entity.User
import com.belosh.movieland.service.impl.security.entity.AuthPrincipal
import com.belosh.movieland.service.impl.security.exception.AuthRequiredException
import com.belosh.movieland.service.impl.security.exception.UuidExpirationException
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.runners.MockitoJUnitRunner
import org.springframework.test.util.ReflectionTestUtils

import java.time.LocalDateTime

@RunWith(MockitoJUnitRunner.class)
class SecurityServiceTest {
    @InjectMocks
    private SecurityService securityService

    @InjectMocks
    private User expectedUser

    @Before
    void setup() {
        expectedUser.setId(1)
        expectedUser.setNickname("Max")
        expectedUser.setEmail("1@gmail.com")
    }

    @Test
    void testGetFromCacheExpiredPrincipal() {
        ReflectionTestUtils.setField(securityService, "defaultPrincipleExpireHours", 0)

        String uuid = securityService.cacheAuthUser(expectedUser)
        try {
            AuthPrincipal principal = securityService.getPrincipalFromCache(uuid)
        } catch (UuidExpirationException e) {
            Assert.assertEquals(0, securityService.UUID_PRINCIPAL_CACHE.size())
            return;
        }
        Assert.assertEquals(1,securityService.UUID_PRINCIPAL_CACHE.size())
    }

    @Test(expected = AuthRequiredException.class)
    void testGetFromCacheInvalidUuid() {
        ReflectionTestUtils.setField(securityService, "defaultPrincipleExpireHours", 2)

        String uuid = securityService.cacheAuthUser(expectedUser)
        AuthPrincipal principal = securityService.getPrincipalFromCache("invalid-uuid")
        User actualUser = principal.getUser()
        Assert.assertEquals(expectedUser, actualUser)
    }

    @Test
    void testPutToCacheAndGetFromCacheValidScenario() {
        ReflectionTestUtils.setField(securityService, "defaultPrincipleExpireHours", 2)

        String uuid = securityService.cacheAuthUser(expectedUser)
        AuthPrincipal principal = securityService.getPrincipalFromCache(uuid)
        User actualUser = principal.getUser()
        Assert.assertEquals(expectedUser, actualUser)
    }

    @Test
    void prolongExpire() {
        ReflectionTestUtils.setField(securityService, "defaultPrincipleExpireHours", 0)

        AuthPrincipal principal = new AuthPrincipal(expectedUser)

        securityService.prolongExpire(principal)

        Assert.assertTrue(principal.getExpire().isAfter(LocalDateTime.now()))
    }

    @Test
    void testRemoveFromCache() {
        ReflectionTestUtils.setField(securityService, "defaultPrincipleExpireHours", 2)

        String uuid = securityService.cacheAuthUser(expectedUser)
        securityService.removeFromCache(uuid)
        Assert.assertEquals(0, securityService.UUID_PRINCIPAL_CACHE.size())
    }
}
