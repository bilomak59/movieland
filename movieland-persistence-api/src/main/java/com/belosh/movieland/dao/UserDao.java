package com.belosh.movieland.dao;

import com.belosh.movieland.entity.User;
import com.belosh.movieland.model.Credentials;

public interface UserDao {

    User getUserByCredentials(Credentials credentials);

}
