package com.belosh.movieland.dao;

import com.belosh.movieland.entity.User;
import com.belosh.movieland.report.ReportMovie;
import com.belosh.movieland.report.ReportRequest;
import com.belosh.movieland.report.ReportUser;

import java.time.LocalDate;
import java.util.List;

public interface ReportDao {
    List<ReportMovie> getAllMovies();

    List<ReportMovie> getMoviesByPeriod(LocalDate period);

    List<ReportUser> getTopActiveUsers();

    List<ReportRequest> getFinishedReportRequests(User user);

    void saveReportDetails(ReportRequest reportRequest);
}
