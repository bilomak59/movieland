package com.belosh.movieland.dao;

import com.belosh.movieland.entity.Review;

import java.util.List;

public interface ReviewDao {

    List<Review> getByMovie(int id);

    void add(Review review);
}
