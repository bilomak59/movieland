package com.belosh.movieland.dao;

import com.belosh.movieland.entity.Country;
import com.belosh.movieland.entity.Genre;
import com.belosh.movieland.entity.Movie;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface CountryDao {

    Map<Integer, List<Country>> getMovieCountiesMap(Set<Integer> movieIDs);

    List<Country> getByMovie(int id);

    List<Country> getAll();

    void removeFromMovie(Movie movie);

    void addToMovie(Movie movie);

    void addToImportedMovie(Movie movie);
}
