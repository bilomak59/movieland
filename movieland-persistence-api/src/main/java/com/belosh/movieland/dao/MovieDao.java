package com.belosh.movieland.dao;

import com.belosh.movieland.entity.Movie;
import com.belosh.movieland.model.SortStrategy;

import java.util.List;

public interface MovieDao {

    List<Movie> getAll(List<SortStrategy> sortStrategies);

    List<Movie> getThreeRandom();

    List<Movie> getByGenre(int genreId, List<SortStrategy> sortStrategies);

    Movie getById(int id);

    void add(Movie movie);

    void update(Movie movie);

    List<Movie> searchByTitle(String searchWord, int page);

    List<Movie> getMoviesForExport();

}
