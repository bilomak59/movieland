package com.belosh.movieland.dao;

import com.belosh.movieland.entity.Genre;
import com.belosh.movieland.entity.Movie;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface GenreDao {

    Map<Integer, List<Genre>> getMovieGenresMap(Set<Integer> movieIDs);

    List<Genre> getAll();

    List<Genre> getByMovie(int id);

    void removeFromMovie(Movie movie);

    void addToMovie(Movie movie);

    void addToImportedMovie(Movie movie);
}
