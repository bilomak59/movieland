package com.belosh.movieland.dao;

import com.belosh.movieland.model.TotalRating;
import com.belosh.movieland.model.UserRating;

import java.util.List;
import java.util.Map;

public interface RatingDao {

    Map<Integer, TotalRating> getRatings();

    void saveBufferRatings(Map<Integer, List<UserRating>> ratings);

    void deleteRelatedRatings(Map<Integer, List<UserRating>> rating_buffer);
}
