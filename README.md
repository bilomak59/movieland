*************************************
# Iteration #6
*************************************
1. [x] Add EnrichService interface
2. [x] Configuration of ThreadPoolExecutor of ParrallelEnrichService added to service-context.xml
3. [x] CopyConstructor for Movie fixed.
4. CompletableFuture cancel operation should be fixed in ParallelEnrichService
5. All Query variables should have prefix SQL
6. [x] MovieRateService.rateMovie method is not thread safe. Have added ReentrantReadWriteLock (24:00)
7. Add Atomics to MovieRateService to rate calculation (28:00)
8. Report registry should be saved in database with all relevant information about report
9. ReportIO should be refactored - (35:00 - 37:00)!!!

Questions:
1. List copy. Do we use it?

*************************************
# Iteration #5
*************************************
Backend:
1. [x] @Transactional annotation, check the Propagation types, and other configuration
2. [x] CurrencyRateService was refactored to use only restTemplate without ObjectMapper
3. [] append "SQL" to sql statements in DAO
4. [] Figure out if I could avoid using two templates
5. [] Create function in PostgreSQL to process update of movie_country and movie_genres. Or any other solution to get update method
6. [x] Fix MovieCache. Correct analyze of SoftReference and need copy of movie based on cache
7. [x] Fix ParallelEnricher
8. [] MovieRateService affect two dao calls. This should be simplified 

Question:
1. Did I correctly define ThreadPoolExecutor in ParallelEnricher service
*************************************
# Iteration #4
*************************************
Questions
1. Does final methods make sense
2. Concern about ThreadLocal - http://www.baeldung.com/java-threadlocal
3. Each time create new object - StopWatch? StopWatch is not thread safe and not recommended for PROD environment: https://www.javacodegeeks.com/2012/08/measure-execution-time-in-java-spring.html
4. Time measurement - System.nanoTime ? 
5. Transactional annotation should be placed in DAO layer or Service
6. Batch processing for countries and genres need review
7. Does each request creates a tread in some predefined ExecutorService in Spring
8. pom.xml, keep in parrent only shared resources? Where we should define properties section for versions

Backend:
1. [] Разобраться и дополнительно почитать про task:executor and task:scheduler
2. [x] ReentrantReadWriteLock добавить в CurrencyRateService вместо СoncurrentHashMap (21:07)
3. [x] Create LoggedUserPricpipal. UseThreadLocal (27:00 - 32:00)
4. [-] Add StopWatch to loggers instead of System.currentTimeMills
5. [x] Add transactions, to Jdbc
6. [] Remind the purpose of ResponseEntityExceptionHandler. Fix GeneralExceptionHandler (43:10)
7. [x] Add, edit movie with countries and genres
8. [x] Movie Cache
9. [] CI
10. [] Parallel enrichment for movie

UI:
1. [] Сортировка для всех фильмов и 3 случайных должна работать
2. [] Add Promisses, remove async:false from ajax 
3. [] To Tolik: UI need review.
4. [] Movie view should return error message when parallel enricher failed 

*************************************
# Iteration #3
*************************************
UI TODO LIST:
- [x] 25:00 Make pictures resizable
- [x] 28:00 Replace var with let or const in jQuery. Figureout why?
- [x] 31:49 Add Mustache template
- [] 31:54 Tests for jQuery
- [] 34:09 Add progress ingicator while content is loading
- [] 35:00 Preload data to array and after success render UI. Otherwise represent an error that server unavailable
- [x] 36:48 Figureout what is data-id. And migrate to it. 
- [x] 38:00 (!) Fix issue with open api link in new tab. Should not be the json content. But base window with rendered content as per the genre
- [] 42:00 Problem with document ready
- [] 44:00 Semantic HTML (BEM)
- [] 01:11 Link should change even in case of SPA part rendering
- [] Login via facebook
- [] GoogleMap with location
- [] Fix design for select item, replace it with unordered list
- [] Template for Genre should be resolved via Mustache
- [x] Dynamicly reload content on link change

Propositions:
* Ability to delete movie

UI Questions:
* Use strict what is this?

BACKEND TODO LIST
- [x] Change all field injections to constructor or setter injections
- [x] JSON NOT NULL should not be declared on root
- [x] 48:00 Collect ExceptionHandlers in one class, check : http://www.baeldung.com/exception-handling-for-rest-with-spring
- [x] 55:00 Exception should return json instead of html page
- [x] 01:21 Make principle immutable and create it with each valid request / or add prolong to computeIfAbsent to ConcurrentHashMap
- [x] 01:29 CredentialsVO should be used instead of map of JSON fields (!!!)
- [x] 01:40 CurrencyRateService should use Annotation based configuration / Genre Cache as well
- [x] 01:45 task scheduler xml configuration should be moved to root-context 
- [x] 01:59 Controller parse Currency enum as parameter
- [] 01:57 import test resource only
- [] Tests for Review/Country/Movie Controllers

BACKEND QUESTIONS:
* Don't use field injection ever, but why? Constructor injection? is it correct?
* SPRING ANTI Patterns / Refactoring tutorials
* Don't use variable which change their state (in if statement for example) 1:12:57
* What is ConcurrentHashMap? 
* Remind Autoboxing/Unboxing.
* Default value after semi colon in @Value("${security.principal.default.expire.hours:40_000}") [not working]
* Volatile
* XML configuration versus Annotation based configuration
* Spring: two beens with equal id's. What will happen?
* Difference between DTO and VO
* Should POST request in Review Controller return confirmation or JSON of saved review
* (?) To Tolik: How @Sheduler could work if schedulers and executors are not created? How should I choose pool size?
* (?) To Tolik: Json Not Null values fixed via MovieVO, is it correct
* (?) To Tolik: Should I force to use Java8 features: Optional, Streams
* (?) For invalid input target controller is not reacting, and I dont have logs about interaction
