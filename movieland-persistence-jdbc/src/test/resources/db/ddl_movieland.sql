SET MODE PostgreSQL;

CREATE TABLE country (
    id          SERIAL PRIMARY KEY  NOT NULL,
    name        TEXT                NOT NULL
);

CREATE TABLE genre (
    id          SERIAL PRIMARY KEY  NOT NULL,
    name        TEXT                NOT NULL
);

CREATE TABLE role (
    id          SERIAL PRIMARY KEY  NOT NULL,
    name        TEXT                NOT NULL
);

CREATE TABLE users (
    id          SERIAL PRIMARY KEY  NOT NULL,
    name        TEXT                NOT NULL,
    email       TEXT                NOT NULL,
    password    TEXT                NOT NULL,
    role_id     INT     REFERENCES role (id)
);

CREATE TABLE movie (
    id          SERIAL PRIMARY KEY  NOT NULL,
    name_russian TEXT                NOT NULL,
    name_native TEXT                NOT NULL,
    release_year SMALLINT           NOT NULL,
    description TEXT                NOT NULL,
    rating      REAL                NOT NULL,
    price       REAL                NOT NULL,
    picture     TEXT
);

ALTER TABLE movie ADD COLUMN add_date DATE DEFAULT current_date;
ALTER TABLE movie ADD COLUMN modified_date DATE DEFAULT current_date;

CREATE TABLE movie_country (
    id          SERIAL PRIMARY KEY  NOT NULL,
    movie_id    INT    REFERENCES movie (id),
    country_id  INT    REFERENCES country (id)
);

CREATE TABLE movie_genre (
    id          SERIAL PRIMARY KEY  NOT NULL,
    movie_id    INT    REFERENCES movie (id),
    genre_id    INT    REFERENCES genre (id)
);

CREATE TABLE review (
    id          SERIAL PRIMARY KEY  NOT NULL,
    movie_id    INT    REFERENCES movie (id),
    user_id     INT    REFERENCES users (id),
    text        TEXT                NOT NULL
);

CREATE TABLE rating (
    id          SERIAL PRIMARY KEY  NOT NULL,
    movie_id    INT    REFERENCES movie (id),
    user_id     INT    REFERENCES users (id),
    rating      REAL                NOT NULL
);

CREATE TABLE report (
    id          SERIAL PRIMARY KEY  NOT NULL,
    reportId    TEXT                NOT NULL,
    period      TEXT                NOT NULL,
    reportType  TEXT                NOT NULL,
    reportOutputType TEXT           NOT NULL,
    requester   INT    REFERENCES users (id),
    reportStatus TEXT               NOT NULL,
    reportLink  TEXT                NOT NULL
);

/*

DROP TABLE review;
DROP TABLE users;
DROP TABLE movie_country;
DROP TABLE movie_genre;
DROP TABLE movie;
DROP TABLE genre;

*/

