package com.belosh.movieland.dao.jdbc.mapper

import com.belosh.movieland.entity.Review
import com.belosh.movieland.entity.User
import org.junit.Test;

import java.sql.ResultSet;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ReviewRowMapperTest {
    private ReviewRowMapper reviewRowMapper = new ReviewRowMapper();

    @Test
    void mapRow() throws Exception {
        int reviewId = 1
        String reviewText = "Acceptable with Bear"
        int userId = 1
        String userName = "Maks"

        User expectedUser = new User()
        expectedUser.setId(userId)
        expectedUser.setNickname(userName)

        Review expectedReview = new Review()
        expectedReview.setId(reviewId)
        expectedReview.setText(reviewText)
        expectedReview.setUser(expectedUser)

        ResultSet resultSet = mock(ResultSet.class)
        when(resultSet.next()).thenReturn(true).thenReturn(false)
        when(resultSet.getInt("id")).thenReturn(reviewId)
        when(resultSet.getString("text")).thenReturn(reviewText)
        when(resultSet.getInt("user_id")).thenReturn(userId)
        when(resultSet.getString("name")).thenReturn(userName)

        def actualReview = reviewRowMapper.mapRow(resultSet, 0)
        assertEquals(expectedReview, actualReview)
    }

}