package com.belosh.movieland.dao.jdbc

import com.belosh.movieland.dao.GenreDao
import com.belosh.movieland.entity.Country
import com.belosh.movieland.entity.Genre
import com.belosh.movieland.entity.Movie
import org.junit.Assert
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired

class JdbcGenreDaoFTest extends SpringTestConfiguration {

    @Autowired
    private GenreDao genreDao

    @Test
    void testGetMovieGenresMap() {
        def movieIDs = [1, 5, 6].toSet()

        Map<Integer, List<Genre>> movieGenresMap = genreDao.getMovieGenresMap(movieIDs)
        Assert.assertEquals(3, movieGenresMap.size())

        for (Map.Entry<Integer, List<Genre>> entry : movieGenresMap) {
            Assert.assertNotNull(entry.getKey())
            Assert.assertNotEquals(0, entry.getValue().size())
        }

        List<Genre> genres = movieGenresMap.get(1) // Movie with ID = 1
        Assert.assertEquals(2, genres.size())

        Genre firstGenre = genres.get(0)
        Assert.assertEquals(1, firstGenre.getId())
        Assert.assertEquals("драма", firstGenre.getName())

        Genre secondGenre = genres.get(1)
        Assert.assertEquals(2, secondGenre.getId())
        Assert.assertEquals("криминал", secondGenre.getName())
    }

    @Test
    void testGetAll() {
        List<Genre> genres = genreDao.getAll()
        Assert.assertEquals(15, genres.size());
    }

    @Test
    void testRemoveFromMovie() {
        Movie movie = new Movie()
        movie.setId(1)

        def movieIDs = [1].toSet()
        Map<Integer, List<Genre>> movieGenreMap = genreDao.getMovieGenresMap(movieIDs)
        List<Country> genres = movieGenreMap.get(1)
        Assert.assertNotEquals(0, genres.size())

        genreDao.removeFromMovie(movie)

        movieGenreMap = genreDao.getMovieGenresMap(movieIDs)
        genres = movieGenreMap.get(1)
        Assert.assertNull(genres)
    }

    @Test
    void testAddToMovie() {
        List<Genre> genres = new ArrayList<>()
        for (int i = 1; i < 4; i++) {
            genres.add(new Genre(i))
        }

        Movie movie = new Movie()
        movie.setId(1)
        movie.setGenres(genres)

        def movieIDs = [movie.getId()].toSet()
        Map<Integer, List<Genre>> movieGenreMap = genreDao.getMovieGenresMap(movieIDs)
        List<Genre> genresFromDb = movieGenreMap.get(movie.getId())
        Assert.assertEquals(2, genresFromDb.size())

        genreDao.addToMovie(movie)

        movieGenreMap = genreDao.getMovieGenresMap(movieIDs)
        genres = movieGenreMap.get(1)
        Assert.assertEquals(5, genres.size())
    }
}
