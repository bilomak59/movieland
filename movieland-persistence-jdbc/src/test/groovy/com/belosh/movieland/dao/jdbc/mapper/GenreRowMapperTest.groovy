package com.belosh.movieland.dao.jdbc.mapper

import com.belosh.movieland.entity.Genre
import org.junit.Assert
import org.junit.Test

import java.sql.ResultSet

import static org.mockito.Mockito.mock
import static org.mockito.Mockito.when

class GenreRowMapperTest {

    private GenreRowMapper genreRowMapper = new GenreRowMapper()

    @Test
    void testMapRow() {

        int expectedId = 1
        String expectedName = "драма"

        Genre expectedGenre = new Genre()
        expectedGenre.setId(expectedId)
        expectedGenre.setName(expectedName)

        ResultSet resultSet = mock(ResultSet.class)
        when(resultSet.next()).thenReturn(true).thenReturn(false)
        when(resultSet.getInt("id")).thenReturn(1)
        when(resultSet.getString("name")).thenReturn(expectedName)

        def actualGenre = genreRowMapper.mapRow(resultSet, 0)
        Assert.assertEquals(actualGenre, expectedGenre)
    }
}