package com.belosh.movieland.dao.jdbc.extractor

import com.belosh.movieland.entity.Country
import org.junit.Assert
import org.junit.Test

import java.sql.ResultSet

import static org.mockito.Mockito.mock
import static org.mockito.Mockito.when

class MovieCountriesExtractorTest {
    private MovieCountriesExtractor movieCountriesExtractor = new MovieCountriesExtractor()

    @Test
    void testExtractData() {
        ResultSet resultSet = mock(ResultSet.class)
        when(resultSet.next()).thenReturn(true).thenReturn(false)
        when(resultSet.getInt("movie_id")).thenReturn(1)
        when(resultSet.getInt("id")).thenReturn(1)
        when(resultSet.getString("name")).thenReturn('США')

        Map<Integer, List<Country>> movieCountriesMap = movieCountriesExtractor.extractData(resultSet)
        Assert.assertTrue(movieCountriesMap.containsKey(1))

        List<Country> countries = movieCountriesMap.get(1)
        Assert.assertEquals(1, countries.size())

        Country country = countries.get(0)
        Assert.assertEquals(1, country.getId())
        Assert.assertEquals("США", country.getName())
    }
}
