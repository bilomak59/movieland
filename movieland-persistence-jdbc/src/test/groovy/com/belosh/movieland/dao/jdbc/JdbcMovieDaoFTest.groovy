package com.belosh.movieland.dao.jdbc

import com.belosh.movieland.dao.MovieDao
import com.belosh.movieland.entity.Movie
import com.belosh.movieland.model.SortStrategy
import org.junit.Assert
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired

class JdbcMovieDaoFTest extends SpringTestConfiguration {

    private MovieDao movieDao
    private JdbcMovieDao jdbcMovieDao

    private List<SortStrategy> sortStrategies = new ArrayList<>()

    @Test
    void testGetAll() {
        List<Movie> movies = movieDao.getAll(sortStrategies)

        Movie firstMovie = movies.get(0)
        Assert.assertEquals(1, firstMovie.getId())
        Assert.assertEquals("Побег из Шоушенка", firstMovie.getNameRussian())
        Assert.assertEquals("The Shawshank Redemption", firstMovie.getNameNative())
        Assert.assertEquals(1994, firstMovie.getYearOfRelease())

        double rating = 8.9d
        Assert.assertEquals(rating, firstMovie.getRating(), 0.001)

        double price = 123.45d
        Assert.assertEquals(price, firstMovie.getPrice(), 0.001)

        Assert.assertEquals("https://images-na.ssl-images-amazon.com/images/M/MV5BODU4MjU4NjIwNl5BMl5BanBnXkFtZTgwMDU2MjEyMDE@._V1._SY209_CR0,0,140,209_.jpg",
                firstMovie.getPicturePath())

        Assert.assertNull(firstMovie.getDescription())

        Assert.assertEquals(25, movies.size())

        for (Movie movie : movies) {
            Assert.assertNotNull(movie.getId())
            Assert.assertNotNull(movie.getNameRussian())
            Assert.assertNotNull(movie.getNameNative())
            Assert.assertNotNull(movie.getYearOfRelease())
            Assert.assertNotNull(movie.getRating())
            Assert.assertNotNull(movie.getPrice())
            Assert.assertNotNull(movie.getPicturePath())

            Assert.assertNull(movie.getDescription())
            Assert.assertNull(movie.getCountries())
            Assert.assertNull(movie.getGenres())
            Assert.assertNull(movie.getReviews())
        }
    }

    @Test
    void getThreeRandom() {
        List<Movie> movies = movieDao.getThreeRandom()
        Assert.assertEquals(3, movies.size())

        for (Movie movie : movies) {
            Assert.assertNotNull(movie.getId())
            Assert.assertNotNull(movie.getNameRussian())
            Assert.assertNotNull(movie.getNameNative())
            Assert.assertNotNull(movie.getYearOfRelease())
            Assert.assertNotNull(movie.getRating())
            Assert.assertNotNull(movie.getPrice())
            Assert.assertNotNull(movie.getPicturePath())
            Assert.assertNotNull(movie.getDescription())

            Assert.assertNull(movie.getCountries())
            Assert.assertNull(movie.getGenres())
            Assert.assertNull(movie.getReviews())
        }
    }

    @Test
    void getByGenre() {
        int genreId = 1

        List<Movie> movies = movieDao.getByGenre(genreId, sortStrategies)
        Assert.assertEquals(16, movies.size())

        for (Movie movie : movies) {
            Assert.assertNotNull(movie.getId())
            Assert.assertNotNull(movie.getNameRussian())
            Assert.assertNotNull(movie.getNameNative())
            Assert.assertNotNull(movie.getYearOfRelease())
            Assert.assertNotNull(movie.getRating())
            Assert.assertNotNull(movie.getPrice())
            Assert.assertNotNull(movie.getPicturePath())

            Assert.assertNull(movie.getDescription())
            Assert.assertNull(movie.getCountries())
            Assert.assertNull(movie.getGenres())
            Assert.assertNull(movie.getReviews())
        }
    }

    @Test
    void getAllSortByPriceDesc() {

        sortStrategies.add(SortStrategy.PRICE_DESC)

        List<Movie> movies = movieDao.getAll(sortStrategies)

        Movie movie = movies.get(0)
        Assert.assertEquals(3, movie.getId())
        Assert.assertEquals("Forrest Gump", movie.getNameNative())
        Assert.assertEquals(1994, movie.getYearOfRelease())

        double price = 200.60d
        Assert.assertEquals(price, movie.getPrice(), 0.001)
    }

    @Test
    void testGetQueryWithSortClause() {
        List<SortStrategy> sortStrategies = new ArrayList<>();
        sortStrategies.add(SortStrategy.RATING_DESC)
        sortStrategies.add(SortStrategy.PRICE_ASC)

        String initialQuery = "SELECT * FROM movie"
        String expectedQuery = "SELECT * FROM movie ORDER BY RATING DESC, PRICE ASC"

        String sortQuery = jdbcMovieDao.getQueryWithSortClause(sortStrategies, initialQuery)

        Assert.assertEquals(expectedQuery, sortQuery)
    }

    @Test
    void add() {
        Movie expectedMovie = new Movie()
        expectedMovie.setNameRussian("test_russian")
        expectedMovie.setNameNative("test_native")
        expectedMovie.setYearOfRelease(2018)
        expectedMovie.setDescription("test_description")
        expectedMovie.setRating(10.0)
        expectedMovie.setPrice(100)
        expectedMovie.setPicturePath("test_picture_path")

        movieDao.add(expectedMovie)
        Movie actualMovie = movieDao.getById(26)

        Assert.assertEquals(26, expectedMovie.getId())
        Assert.assertEquals(expectedMovie, actualMovie)
    }

    @Test
    void update() {
        Movie expectedMovie = new Movie()
        expectedMovie.setId(1)
        expectedMovie.setNameRussian("test_russian")
        expectedMovie.setNameNative("test_native")
        expectedMovie.setYearOfRelease(2018)
        expectedMovie.setDescription("test_description")
        expectedMovie.setRating(10.0)
        expectedMovie.setPrice(100)
        expectedMovie.setPicturePath("test_picture_path")

        movieDao.update(expectedMovie)

        Movie actualMovie = movieDao.getById(1)

        Assert.assertEquals(expectedMovie, actualMovie)
    }

    @Autowired
    void setMovieDao(MovieDao movieDao) {
        this.movieDao = movieDao
    }

    @Autowired
    void setJdbcMovieDao(JdbcMovieDao jdbcMovieDao) {
        this.jdbcMovieDao = jdbcMovieDao
    }
}
