package com.belosh.movieland.dao.jdbc.mapper

import com.belosh.movieland.entity.Movie
import org.junit.Assert
import org.junit.Test

import java.sql.Date
import java.sql.ResultSet

import static org.mockito.Mockito.mock
import static org.mockito.Mockito.when

class MovieRowMappersTest {

    private MovieRowMapper movieRowMapper = new MovieRowMapper()
    private AllMoviesRowMapper allMovieRowMapper = new AllMoviesRowMapper()

    @Test
    void testMapRow() {
        int expectedId = 1
        String expectedNameRussian = "Побег из Шоушенка"
        String expectedNameNative = "The Shawshank Redemption"
        int expectedReleaseYear = 1993
        String expectedDescription = "Успешный банкир Энди Дюфрейн обвинен в убийстве"
        double expectedRating = 8.90d
        double expectedPrice = 123.45d
        String expectedPicture = "Shawshank_Redemption_1.jpg"

        Movie expectedMovie = new Movie()
        expectedMovie.setId(expectedId)
        expectedMovie.setNameRussian(expectedNameRussian)
        expectedMovie.setNameNative(expectedNameNative)
        expectedMovie.setYearOfRelease(expectedReleaseYear)
        expectedMovie.setDescription(expectedDescription)
        expectedMovie.setRating(expectedRating)
        expectedMovie.setPrice(expectedPrice)
        expectedMovie.setPicturePath(expectedPicture)

        ResultSet resultSet = mock(ResultSet.class)
        when(resultSet.next()).thenReturn(true).thenReturn(false)
        when(resultSet.getInt("id")).thenReturn(expectedId)
        when(resultSet.getString("name_russian")).thenReturn(expectedNameRussian)
        when(resultSet.getString("name_native")).thenReturn(expectedNameNative)
        when(resultSet.getInt("release_year")).thenReturn(expectedReleaseYear)
        when(resultSet.getString("description")).thenReturn(expectedDescription)
        when(resultSet.getDouble("rating")).thenReturn(expectedRating)
        when(resultSet.getDouble("price")).thenReturn(expectedPrice)
        when(resultSet.getString("picture")).thenReturn(expectedPicture)

        Movie actualMovie = movieRowMapper.mapRow(resultSet, 0)
        Assert.assertEquals(expectedMovie, actualMovie)

        actualMovie = allMovieRowMapper.mapRow(resultSet, 0)
        expectedMovie.setDescription(null)
        Assert.assertEquals(expectedMovie, actualMovie)
    }
}
