package com.belosh.movieland.dao.jdbc;

import com.belosh.movieland.dao.ReviewDao;
import com.belosh.movieland.entity.Review;
import com.belosh.movieland.entity.User
import com.belosh.movieland.model.Role
import org.junit.Assert
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*

class JdbcReviewDaoTest extends SpringTestConfiguration {

    private ReviewDao reviewDao;

    @Test
    void getByMovie() throws Exception {
    }

    @Test
    void add() throws Exception {
        User user = new User()
        user.setId(1)
        user.setNickname("Рональд Рейнольдс")

        Review expectedReview = new Review()
        expectedReview.setId(33)
        expectedReview.setMovieId(2)
        expectedReview.setText("Test review")
        expectedReview.setUser(user)

        reviewDao.add(expectedReview)

        List<Review> reviews = reviewDao.getByMovie(2)
        assertEquals(2, reviews.size())
        Review retrievedReview = reviews.get(1)
        assertEquals(expectedReview, retrievedReview)
    }

    @Autowired
    void setReviewDao(ReviewDao reviewDao) {
        this.reviewDao = reviewDao
    }
}