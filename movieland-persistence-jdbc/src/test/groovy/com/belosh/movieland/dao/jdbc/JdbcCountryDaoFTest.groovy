package com.belosh.movieland.dao.jdbc

import com.belosh.movieland.dao.CountryDao
import com.belosh.movieland.dao.MovieDao
import com.belosh.movieland.entity.Country
import com.belosh.movieland.entity.Movie
import org.junit.Assert
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired


class JdbcCountryDaoFTest extends SpringTestConfiguration {
    private CountryDao countryDao

    @Test
    void testGetMovieCounties() {
        def movieIDs = [1, 5, 6].toSet()

        Map<Integer, List<Country>> movieCountriesMap = countryDao.getMovieCountiesMap(movieIDs)
        Assert.assertEquals(3, movieCountriesMap.size())

        for (Map.Entry<Integer, List<Country>> entry : movieCountriesMap) {
            Assert.assertNotNull(entry.getKey())
            Assert.assertNotEquals(0, entry.getValue().size())
        }

        List<Country> countries = movieCountriesMap.get(6) // Movie with ID = 6
        Assert.assertEquals(2, countries.size())

        Country firstCountry = countries.get(0)
        Assert.assertEquals(4, firstCountry.getId())
        Assert.assertEquals("США", firstCountry.getName())

        Country secondCountry = countries.get(1)
        Assert.assertEquals(5, secondCountry.getId())
        Assert.assertEquals("Великобритания", secondCountry.getName())
    }

    @Test
    void testRemoveFromMovie() {
        Movie movie = new Movie()
        movie.setId(1)

        def movieIDs = [1].toSet()
        Map<Integer, List<Country>> movieCountriesMap = countryDao.getMovieCountiesMap(movieIDs)
        List<Country> countries = movieCountriesMap.get(1)
        Assert.assertNotEquals(0, countries.size())

        countryDao.removeFromMovie(movie)

        movieCountriesMap = countryDao.getMovieCountiesMap(movieIDs)
        countries = movieCountriesMap.get(1)
        Assert.assertNull(countries)
    }

    @Test
    void testAddToMovie() {
        List<Country> countries = new ArrayList<>()
        for (int i = 1; i < 4; i++) {
            countries.add(new Country(i))
        }

        Movie movie = new Movie()
        movie.setId(1)
        movie.setCountries(countries)

        def movieIDs = [movie.getId()].toSet()
        Map<Integer, List<Country>> movieCountriesMap = countryDao.getMovieCountiesMap(movieIDs)
        List<Country> countriesFromDb = movieCountriesMap.get(movie.getId())
        Assert.assertEquals(1, countriesFromDb.size())

        countryDao.addToMovie(movie)

        movieCountriesMap = countryDao.getMovieCountiesMap(movieIDs)
        countries = movieCountriesMap.get(1)
        Assert.assertEquals(4, countries.size())
    }

    @Test
    void testGetAll() {
        List<Country> countries = countryDao.getAll();
        Assert.assertEquals(7, countries.size())
    }

    @Autowired
    void setCountryDao(CountryDao countryDao) {
        this.countryDao = countryDao
    }
}
