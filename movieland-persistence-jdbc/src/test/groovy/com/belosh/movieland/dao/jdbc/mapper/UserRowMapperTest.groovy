package com.belosh.movieland.dao.jdbc.mapper

import com.belosh.movieland.entity.User
import com.belosh.movieland.model.Role
import org.junit.Assert
import org.junit.Test

import java.sql.ResultSet

import static org.mockito.Mockito.mock
import static org.mockito.Mockito.when

class UserRowMapperTest {

    private UserRowMapper userRowMapper = new UserRowMapper()

    @Test
    void testMapRow() {
        User expectedUser = new User();
        expectedUser.setId(1)
        expectedUser.setNickname("Max")
        expectedUser.setEmail("1@gmail.com")
        expectedUser.setRole(Role.ADMIN)

        ResultSet resultSet = mock(ResultSet.class)
        when(resultSet.next()).thenReturn(true).thenReturn(false)
        when(resultSet.getInt("id")).thenReturn(expectedUser.getId())
        when(resultSet.getString("name")).thenReturn(expectedUser.getNickname())
        when(resultSet.getString("email")).thenReturn(expectedUser.getEmail())
        when(resultSet.getString("role_name")).thenReturn(expectedUser.getRole().toString())

        def actualUser = userRowMapper.mapRow(resultSet, 0)
        Assert.assertEquals(actualUser, expectedUser)
    }
}
