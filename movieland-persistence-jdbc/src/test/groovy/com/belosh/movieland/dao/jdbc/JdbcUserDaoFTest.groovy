package com.belosh.movieland.dao.jdbc

import com.belosh.movieland.dao.UserDao
import com.belosh.movieland.entity.User
import com.belosh.movieland.model.Credentials
import com.belosh.movieland.model.Role
import org.junit.Assert
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired

class JdbcUserDaoFTest extends SpringTestConfiguration {

    @Autowired
    private UserDao userDao

    @Test
    void testGetUserByCredentials() {
        User expectedUser = new User()
        expectedUser.setId(1)
        expectedUser.setNickname('Рональд Рейнольдс')
        expectedUser.setEmail('ronald.reynolds66@example.com')
        expectedUser.setRole(Role.ADMIN)

        Credentials credentials = new Credentials();
        credentials.setEmail('ronald.reynolds66@example.com')
        credentials.setPassword('paco')

        User actualUser = userDao.getUserByCredentials(credentials)

        Assert.assertEquals(expectedUser, actualUser)
    }


}
