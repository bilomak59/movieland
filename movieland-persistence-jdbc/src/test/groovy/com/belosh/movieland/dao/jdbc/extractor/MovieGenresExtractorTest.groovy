package com.belosh.movieland.dao.jdbc.extractor

import com.belosh.movieland.entity.Genre
import org.junit.Assert
import org.junit.Test

import java.sql.ResultSet

import static org.mockito.Mockito.mock
import static org.mockito.Mockito.when

class MovieGenresExtractorTest {

    private MovieGenresExtractor movieGenresExtractor = new MovieGenresExtractor()

    @Test
    void testMapRow() {
        ResultSet resultSet = mock(ResultSet.class)
        when(resultSet.next()).thenReturn(true).thenReturn(false)
        when(resultSet.getInt("movie_id")).thenReturn(1)
        when(resultSet.getInt("id")).thenReturn(1)
        when(resultSet.getString("name")).thenReturn('драма')

        Map<Integer, List<Genre>> movieGenresMap = movieGenresExtractor.extractData(resultSet)
        Assert.assertTrue(movieGenresMap.containsKey(1))

        List<Genre> genres = movieGenresMap.get(1)
        Assert.assertEquals(1, genres.size())

        Genre genre = genres.get(0)
        Assert.assertEquals(1, genre.getId())
        Assert.assertEquals("драма", genre.getName())
    }

}
