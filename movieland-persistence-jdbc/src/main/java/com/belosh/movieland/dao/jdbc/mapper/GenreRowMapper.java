package com.belosh.movieland.dao.jdbc.mapper;

import com.belosh.movieland.entity.Genre;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class GenreRowMapper implements RowMapper<Genre> {

    @Override
    public Genre mapRow(ResultSet resultSet, int i) throws SQLException {
        int genreId = resultSet.getInt("id");
        String genreName = resultSet.getString("name");

        return new Genre(genreId, genreName);
    }
}
