package com.belosh.movieland.dao.jdbc;

import com.belosh.movieland.dao.CountryDao;
import com.belosh.movieland.dao.jdbc.extractor.MovieCountriesExtractor;
import com.belosh.movieland.dao.jdbc.mapper.CountryRowMapper;
import com.belosh.movieland.entity.Country;
import com.belosh.movieland.entity.Movie;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Repository
public class JdbcCountryDao implements CountryDao {

    private final Logger log = LoggerFactory.getLogger(getClass());
    private final CountryRowMapper COUNTRY_ROW_MAPPER = new CountryRowMapper();
    private final MovieCountriesExtractor MOVIE_COUNTRIES_EXTRACTOR = new MovieCountriesExtractor();

    private JdbcTemplate jdbcTemplate;
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private String getCountriesByMoviesSql;
    private String getCountriesByMovieSql;
    private String getAllCountriesSql;
    private String addCountryToMovieSql;
    private String removeCountryFromMovieSql;
    private String addCountryToImportedMovieSql;

    @Override
    public Map<Integer, List<Country>> getMovieCountiesMap(Set<Integer> movieIDs) {

        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("ids", movieIDs);

        log.info("Start processing query to get all countries by set of movies IDs");
        long startTime = System.currentTimeMillis();

        Map<Integer, List<Country>> movieCountryMap =
                namedParameterJdbcTemplate.query(getCountriesByMoviesSql, parameters, MOVIE_COUNTRIES_EXTRACTOR);

        log.info("Finish processing query to get all countries by set of movies IDs. It took {} ms", System.currentTimeMillis() - startTime);

        return movieCountryMap;
    }

    @Override
    public List<Country> getByMovie(int id) {
        log.info("Start query to get countries by movie");
        long startTime = System.currentTimeMillis();
        List<Country> countries = jdbcTemplate.query(getCountriesByMovieSql, COUNTRY_ROW_MAPPER, id);
        log.info("Finish query to get countries by movie. It took {} ms", System.currentTimeMillis() - startTime);
        return countries;
    }

    @Override
    public List<Country> getAll() {
        log.info("Start query to get countries by movie");
        long startTime = System.currentTimeMillis();
        List<Country> countries = jdbcTemplate.query(getAllCountriesSql, COUNTRY_ROW_MAPPER);
        log.info("Finish query to get countries by movie. It took {} ms", System.currentTimeMillis() - startTime);
        return countries;
    }

    @Override
    public void removeFromMovie(Movie movie) {
        log.info("Start execution query to remove countries from movie");
        long startTime = System.currentTimeMillis();

        int removedCountries = jdbcTemplate.update(removeCountryFromMovieSql, movie.getId());

        log.info("Finish execution query to remove countries from movie. Removed countries: {}. It took {} ms",
                removedCountries,
                System.currentTimeMillis() - startTime);
    }

    @Override
    public void addToMovie(Movie movie) {
        int movieId = movie.getId();

        log.info("Start execution query to add country to movie: ", movieId);
        long startTime = System.currentTimeMillis();

        List<Country> countries = movie.getCountries();

        jdbcTemplate.batchUpdate(addCountryToMovieSql, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                ps.setInt(1, movieId);
                ps.setInt(2, countries.get(i).getId());
            }

            @Override
            public int getBatchSize() {
                return countries.size();
            }
        });

        log.info("Finish execution query to add country to movie: {}. It took {} ms",
                movieId,
                System.currentTimeMillis() - startTime);
    }

    @Override
    public void addToImportedMovie(Movie movie) {
        int movieId = movie.getId();

        log.info("Start execution query to add country to imported movie: ", movieId);
        long startTime = System.currentTimeMillis();

        List<Country> countries = movie.getCountries();

        jdbcTemplate.batchUpdate(addCountryToImportedMovieSql, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                ps.setInt(1, movieId);
                ps.setString(2, countries.get(i).getName());
            }

            @Override
            public int getBatchSize() {
                return countries.size();
            }
        });

        log.info("Finish execution query to add country to imported movie: {}. It took {} ms",
                movieId,
                System.currentTimeMillis() - startTime);
    }

    @Autowired
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Autowired
    public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    @Autowired
    public void setGetCountriesByMoviesSql(String getCountriesByMoviesSql) {
        this.getCountriesByMoviesSql = getCountriesByMoviesSql;
    }

    @Autowired
    public void setGetCountriesByMovieSql(String getCountriesByMovieSql) {
        this.getCountriesByMovieSql = getCountriesByMovieSql;
    }

    @Autowired
    public void setGetAllCountriesSql(String getAllCountriesSql) {
        this.getAllCountriesSql = getAllCountriesSql;
    }

    @Autowired
    public void setAddCountryToMovieSql(String addCountryToMovieSql) {
        this.addCountryToMovieSql = addCountryToMovieSql;
    }

    @Autowired
    public void setRemoveCountryFromMovieSql(String removeCountryFromMovieSql) {
        this.removeCountryFromMovieSql = removeCountryFromMovieSql;
    }

    @Autowired
    public void setAddCountryToImportedMovieSql(String addCountryToImportedMovieSql) {
        this.addCountryToImportedMovieSql = addCountryToImportedMovieSql;
    }
}
