package com.belosh.movieland.dao.jdbc.extractor;

import com.belosh.movieland.model.TotalRating;
import org.springframework.jdbc.core.ResultSetExtractor;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;


public class MovieRatingExtractor implements ResultSetExtractor<Map<Integer, TotalRating>> {

    @Override
    public Map<Integer, TotalRating> extractData(ResultSet resultSet) throws SQLException {
        Map<Integer, TotalRating> ratings = new HashMap<>();

        while (resultSet.next()) {
            Integer movieId = resultSet.getInt("movie_id");
            double rating = resultSet.getDouble("rating");
            int votes = resultSet.getInt("votes");

            TotalRating totalRating = new TotalRating(votes, rating);

            ratings.put(movieId, totalRating);
        }
        return ratings;
    }
}
