package com.belosh.movieland.dao.jdbc.extractor;

import com.belosh.movieland.dao.jdbc.mapper.GenreRowMapper;
import com.belosh.movieland.entity.Genre;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MovieGenresExtractor implements ResultSetExtractor<Map<Integer, List<Genre>>> {

    private final GenreRowMapper GENRE_ROW_MAPPER = new GenreRowMapper();

    @Override
    public Map<Integer, List<Genre>> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
        Map<Integer, List<Genre>> movieGenres = new HashMap<>();

        while (resultSet.next()) {
            Integer movieId = resultSet.getInt("movie_id");
            List<Genre> genres = movieGenres.computeIfAbsent(movieId, k -> new ArrayList<>());

            Genre genre = GENRE_ROW_MAPPER.mapRow(resultSet, 0);
            genres.add(genre);
        }
        return movieGenres;
    }
}
