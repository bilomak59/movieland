package com.belosh.movieland.dao.jdbc.mapper;

import com.belosh.movieland.report.ReportMovie;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ReportMovieRowMapper implements RowMapper<ReportMovie> {
    @Nullable
    @Override
    public ReportMovie mapRow(ResultSet rs, int rowNum) throws SQLException {
        ReportMovie reportMovie = new ReportMovie();
        reportMovie.setMovieId(rs.getInt("id"));
        reportMovie.setTitle(rs.getString("title"));
        reportMovie.setDescription(rs.getString("description"));
        reportMovie.setGenre(rs.getString("genre"));
        reportMovie.setPrice(rs.getDouble("price"));

        reportMovie.setAddDate(rs.getDate("add_date").toLocalDate());
        reportMovie.setModifiedDate(rs.getDate("modified_date").toLocalDate());

        reportMovie.setRating(rs.getDouble("rating"));
        reportMovie.setReviewsCount(rs.getInt("reviews"));
        return reportMovie;
    }
}
