package com.belosh.movieland.dao.jdbc.mapper;

import com.belosh.movieland.report.ReportMovie;
import com.belosh.movieland.report.ReportUser;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ReportUserRowMapper implements RowMapper<ReportUser> {
    @Nullable
    @Override
    public ReportUser mapRow(ResultSet rs, int rowNum) throws SQLException {
        ReportUser reportUser = new ReportUser();
        reportUser.setUserId(rs.getInt("id"));
        reportUser.setEmail(rs.getString("email"));
        reportUser.setName(rs.getString("name"));
        reportUser.setCountReviews(rs.getInt("reviews"));
        reportUser.setAvgRate(rs.getInt("rates"));

        return reportUser;
    }
}
