package com.belosh.movieland.dao.jdbc.mapper;

import com.belosh.movieland.entity.User;
import com.belosh.movieland.model.Role;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserRowMapper implements RowMapper<User> {

    @Override
    public User mapRow(ResultSet resultSet, int i) throws SQLException {
        User user = new User();

        user.setId(resultSet.getInt("id"));
        user.setNickname(resultSet.getString("name"));
        user.setEmail(resultSet.getString("email"));

        Role role = Role.getRoleByName(resultSet.getString("role_name"));
        user.setRole(role);

        return user;
    }
}
