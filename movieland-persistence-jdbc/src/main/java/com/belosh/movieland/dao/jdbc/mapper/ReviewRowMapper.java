package com.belosh.movieland.dao.jdbc.mapper;

import com.belosh.movieland.entity.Review;
import com.belosh.movieland.entity.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ReviewRowMapper implements RowMapper<Review> {

    @Override
    public Review mapRow(ResultSet resultSet, int i) throws SQLException {
        Review review = new Review();
        User user = new User();

        review.setId(resultSet.getInt("id"));
        review.setText(resultSet.getString("text"));
        review.setMovieId(resultSet.getInt("movie_id"));

        user.setId(resultSet.getInt("user_id"));
        user.setNickname(resultSet.getString("name"));

        review.setUser(user);

        return review;
    }
}
