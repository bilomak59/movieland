package com.belosh.movieland.dao.jdbc;

import com.belosh.movieland.dao.UserDao;
import com.belosh.movieland.dao.jdbc.mapper.UserRowMapper;
import com.belosh.movieland.entity.User;
import com.belosh.movieland.model.Credentials;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class JdbcUserDao implements UserDao {
    private final Logger log = LoggerFactory.getLogger(getClass());
    private final UserRowMapper USER_ROW_MAPPER = new UserRowMapper();

    private JdbcTemplate jdbcTemplate;

    private String getUserByCredentialsSql;

    @Autowired
    public JdbcUserDao(JdbcTemplate jdbcTemplate, String getUserByCredentialsSql) {
        this.jdbcTemplate = jdbcTemplate;
        this.getUserByCredentialsSql = getUserByCredentialsSql;
    }

    @Override
    public User getUserByCredentials(Credentials credentials) {
        log.info("Start processing query to get user by credentials");

        try {
            long startTime = System.currentTimeMillis();
            String email = credentials.getEmail();
            String password = credentials.getPassword();

            User user = jdbcTemplate.queryForObject(getUserByCredentialsSql, USER_ROW_MAPPER, email, password);

            log.info("Finish processing query to get user by credentials. It took {} ms", System.currentTimeMillis() - startTime);
            return user;
        } catch (EmptyResultDataAccessException e) {
            throw new EmptyResultDataAccessException("User not found with credentials: " + credentials, 1, e);
        }

    }
}
