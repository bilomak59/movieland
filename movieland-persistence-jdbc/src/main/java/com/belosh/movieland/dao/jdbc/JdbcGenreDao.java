package com.belosh.movieland.dao.jdbc;

import com.belosh.movieland.dao.GenreDao;
import com.belosh.movieland.dao.jdbc.extractor.MovieGenresExtractor;
import com.belosh.movieland.dao.jdbc.mapper.GenreRowMapper;
import com.belosh.movieland.entity.Genre;
import com.belosh.movieland.entity.Movie;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Repository
public class JdbcGenreDao implements GenreDao {

    private final Logger log = LoggerFactory.getLogger(getClass());
    private final GenreRowMapper GENRE_ROW_MAPPER = new GenreRowMapper();
    private final MovieGenresExtractor MOVIE_GENRES_EXTRACTOR = new MovieGenresExtractor();

    private JdbcTemplate jdbcTemplate;
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private String getGenresByMoviesSql;
    private String getAllGenresSql;
    private String getGenresByMovieSql;
    private String addGenreToMovieSql;
    private String removeGenreFromMovieSql;
    private String addGenreToImportedMovieSql;

    @Override
    public Map<Integer, List<Genre>> getMovieGenresMap(Set<Integer> movieIDs) {
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("ids", movieIDs);

        log.info("Start processing query to get all genres by set of movies IDs");
        long startTime = System.currentTimeMillis();

        Map<Integer, List<Genre>> movieGenreMap =
                namedParameterJdbcTemplate.query(getGenresByMoviesSql, parameters, MOVIE_GENRES_EXTRACTOR);

        log.info("Finish processing query to get all genres by set of movies IDs. It took {} ms", System.currentTimeMillis() - startTime);

        return movieGenreMap;
    }

    @Override
    public List<Genre> getAll() {
        log.info("Start query to get all genres");
        long startTime = System.currentTimeMillis();

        List<Genre> genres = jdbcTemplate.query(getAllGenresSql, GENRE_ROW_MAPPER);

        log.info("Finish query to get all genres. It took {} ms", System.currentTimeMillis() - startTime);
        return genres;
    }

    @Override
    public List<Genre> getByMovie(int id) {
        log.info("Start query to get genres by movie");
        long startTime = System.currentTimeMillis();
        List<Genre> genres = jdbcTemplate.query(getGenresByMovieSql, new GenreRowMapper(), id);
        log.info("Finish query to get genres by movie. It took {} ms", System.currentTimeMillis() - startTime);
        return genres;
    }

    @Override
    public void removeFromMovie(Movie movie) {
        log.info("Start execution query to remove genres from movie");
        long startTime = System.currentTimeMillis();

        int removedCountries = jdbcTemplate.update(removeGenreFromMovieSql, movie.getId());

        log.info("Finish execution query to remove genres from movie. Removed countries: {}. It took {} ms",
                removedCountries,
                System.currentTimeMillis() - startTime);
    }

    @Override
    public void addToMovie(Movie movie) {
        int movieId = movie.getId();

        log.info("Start execution query to add genres to movie: ", movieId);
        long startTime = System.currentTimeMillis();

        List<Genre> genres = movie.getGenres();

        jdbcTemplate.batchUpdate(addGenreToMovieSql, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                ps.setInt(1, movieId);
                ps.setInt(2, genres.get(i).getId());
            }

            @Override
            public int getBatchSize() {
                return genres.size();
            }
        });

        log.info("Finish execution query to add genres to movie: {}. It took {} ms",
                movieId,
                System.currentTimeMillis() - startTime);
    }

    @Override
    public void addToImportedMovie(Movie movie) {
        int movieId = movie.getId();

        log.info("Start execution query to add genres to imported movie: ", movieId);
        long startTime = System.currentTimeMillis();

        List<Genre> genres = movie.getGenres();

        jdbcTemplate.batchUpdate(addGenreToImportedMovieSql, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                ps.setInt(1, movieId);
                ps.setString(2, genres.get(i).getName());
            }

            @Override
            public int getBatchSize() {
                return genres.size();
            }
        });

        log.info("Finish execution query to add genres to imported movie: {}. It took {} ms",
                movieId,
                System.currentTimeMillis() - startTime);
    }

    @Autowired
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Autowired
    public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    @Autowired
    public void setGetGenresByMoviesSql(String getGenresByMoviesSql) {
        this.getGenresByMoviesSql = getGenresByMoviesSql;
    }

    @Autowired
    public void setGetAllGenresSql(String getAllGenresSql) {
        this.getAllGenresSql = getAllGenresSql;
    }

    @Autowired
    public void setGetGenresByMovieSql(String getGenresByMovieSql) {
        this.getGenresByMovieSql = getGenresByMovieSql;
    }

    @Autowired
    public void setAddGenreToMovieSql(String addGenreToMovieSql) {
        this.addGenreToMovieSql = addGenreToMovieSql;
    }

    @Autowired
    public void setRemoveGenreFromMovieSql(String removeGenreFromMovieSql) {
        this.removeGenreFromMovieSql = removeGenreFromMovieSql;
    }

    @Autowired
    public void setAddGenreToImportedMovieSql(String addGenreToImportedMovieSql) {
        this.addGenreToImportedMovieSql = addGenreToImportedMovieSql;
    }
}
