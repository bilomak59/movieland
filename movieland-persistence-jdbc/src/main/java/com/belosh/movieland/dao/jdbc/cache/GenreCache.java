package com.belosh.movieland.dao.jdbc.cache;

import com.belosh.movieland.dao.GenreDao;
import com.belosh.movieland.dao.jdbc.JdbcGenreDao;
import com.belosh.movieland.entity.Genre;
import com.belosh.movieland.entity.Movie;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Primary
@Repository
@ManagedResource(objectName = "bean:name=genreCache")
public class GenreCache implements GenreDao {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private volatile List<Genre> cachedGenres;

    private JdbcGenreDao genreDao;

    @Autowired
    public GenreCache(JdbcGenreDao genreDao) {
        this.genreDao = genreDao;
    }

    @ManagedOperation
    @PostConstruct
    @Scheduled(fixedRateString = "${scheduler.genre.cache.fixed.rate}",
            initialDelayString = "${scheduler.genre.cache.fixed.rate}")
    public void refreshCache() {
        log.info("Start refreshing Genre Cache");
        cachedGenres = genreDao.getAll();
        log.info("Genre Cache refreshed");
    }

    @Override
    public Map<Integer, List<Genre>> getMovieGenresMap(Set<Integer> movieIDs) {
        return genreDao.getMovieGenresMap(movieIDs);
    }

    @Override
    public List<Genre> getAll() {
        log.info("Returning cached genres.");
        return new ArrayList<>(cachedGenres);
    }

    @Override
    public List<Genre> getByMovie(int id) {
        return genreDao.getByMovie(id);
    }

    @Override
    public void removeFromMovie(Movie movie) {
        genreDao.removeFromMovie(movie);
    }

    @Override
    public void addToMovie(Movie movie) {
        genreDao.addToMovie(movie);
    }

    @Override
    public void addToImportedMovie(Movie movie) {
        genreDao.addToImportedMovie(movie);
    }
}
