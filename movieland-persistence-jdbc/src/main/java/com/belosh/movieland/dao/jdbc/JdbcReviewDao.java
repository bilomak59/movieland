package com.belosh.movieland.dao.jdbc;

import com.belosh.movieland.dao.ReviewDao;
import com.belosh.movieland.dao.jdbc.mapper.ReviewRowMapper;
import com.belosh.movieland.entity.Review;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class JdbcReviewDao implements ReviewDao {
    private final Logger log = LoggerFactory.getLogger(getClass());
    private final ReviewRowMapper REVIEW_ROW_MAPPER = new ReviewRowMapper();

    private JdbcTemplate jdbcTemplate;

    private String getReviewByMovieSql;
    private String addReviewSql;

    @Override
    public List<Review> getByMovie(int id) {
        log.info("Start processing query to get review by movie id");
        long startTime = System.currentTimeMillis();

        List<Review> reviews = jdbcTemplate.query(getReviewByMovieSql, REVIEW_ROW_MAPPER, id);

        log.info("Finish processing query to get review by movie id. It took {} ms", System.currentTimeMillis() - startTime);
        return reviews;
    }

    @Override
    public void add(Review review) {
        log.info("Start processing query to add review");
        long startTime = System.currentTimeMillis();

        jdbcTemplate.update(addReviewSql, review.getMovieId(), review.getUser().getId(), review.getText());

        log.info("Finish processing query to add review. It took {} ms", System.currentTimeMillis() - startTime);
    }



    @Autowired
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Autowired
    public void setGetReviewByMovieSql(String getReviewByMovieSql) {
        this.getReviewByMovieSql = getReviewByMovieSql;
    }

    @Autowired
    public void setAddReviewSql(String addReviewSql) {
        this.addReviewSql = addReviewSql;
    }
}
