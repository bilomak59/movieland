package com.belosh.movieland.dao.jdbc;

import com.belosh.movieland.dao.MovieDao;
import com.belosh.movieland.dao.jdbc.mapper.AllMoviesRowMapper;
import com.belosh.movieland.dao.jdbc.mapper.MovieRowMapper;
import com.belosh.movieland.entity.Movie;
import com.belosh.movieland.model.SortStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Repository
public class JdbcMovieDao implements MovieDao {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private final RowMapper<Movie> MOVIE_ROW_MAPPER = new MovieRowMapper();
    private final RowMapper<Movie> ALL_MOVIES_ROW_MAPPER = new AllMoviesRowMapper();

    private JdbcTemplate jdbcTemplate;
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private int moviesPerPage;

    private String getAllMoviesSql;
    private String getThreeRandomMovieSql;
    private String getByGenreSql;
    private String getByIdSql;
    private String addMovieSql;
    private String updateMovieSql;
    private String searchByTitleSql;
    private String getMoviesForExportSql;

    @Override
    public List<Movie> getAll(List<SortStrategy> sortStrategies) {
        log.info("Start processing query to get all movies");
        long startTime = System.currentTimeMillis();

        String queryWithSortClause = getQueryWithSortClause(sortStrategies, getAllMoviesSql);

        List<Movie> movies = jdbcTemplate.query(queryWithSortClause, ALL_MOVIES_ROW_MAPPER);

        log.info("Finish processing query to get all movies. It took {} ms", System.currentTimeMillis() - startTime);
        return movies;
    }

    @Override
    public List<Movie> getThreeRandom() {
        log.info("Start processing query to get three random movies");
        long startTime = System.currentTimeMillis();

        List<Movie> movies = jdbcTemplate.query(getThreeRandomMovieSql, MOVIE_ROW_MAPPER);

        log.info("Finish processing query to get three random movies. It took {} ms", System.currentTimeMillis() - startTime);
        return movies;
    }

    @Override
    public List<Movie> getByGenre(int genreId, List<SortStrategy> sortStrategies) {
        log.info("Start processing query to get movies by genre");
        long startTime = System.currentTimeMillis();

        String queryWithSortClause = getQueryWithSortClause(sortStrategies, getByGenreSql);

        List<Movie> movies = jdbcTemplate.query(queryWithSortClause, ALL_MOVIES_ROW_MAPPER, genreId);

        log.info("Finish processing query to get movies by genre. It took {} ms", System.currentTimeMillis() - startTime);
        return movies;
    }

    @Override
    public Movie getById(int id) {
        log.info("Start processing query to get movie by id: {}", getByIdSql);
        long startTime = System.currentTimeMillis();

        Movie movie = jdbcTemplate.queryForObject(getByIdSql, MOVIE_ROW_MAPPER, id);

        log.info("Finish processing query to get movie by id. It took {} ms", System.currentTimeMillis() - startTime);
        return movie;
    }

    @Override
    public void add(Movie movie) {
        log.info("Start processing query to add movie");
        long startTime = System.currentTimeMillis();

        MapSqlParameterSource parameterSource = getSQLMapByMovie(movie);

        KeyHolder keyHolder = new GeneratedKeyHolder();
        int updated = namedParameterJdbcTemplate.update(addMovieSql, parameterSource, keyHolder);

        Map<String, Object> keyValue = keyHolder.getKeys();

        if (keyValue != null && keyValue.get("id") instanceof Integer) {
            int movieId = (Integer) keyValue.get("id");
            movie.setId(movieId);
        } else {
            throw new RuntimeException("Unable to retrieve generated id for newly added movie");
        }

        log.info("Finish processing query to add movie. Updated records {}, It took {} ms",
                updated,
                System.currentTimeMillis() - startTime);
    }

    @Override
    public void update(Movie movie) {
        log.info("Start processing query to update movie");
        long startTime = System.currentTimeMillis();

        MapSqlParameterSource parameterSource = getSQLMapByMovie(movie);
        namedParameterJdbcTemplate.update(updateMovieSql, parameterSource);

        log.info("Finish processing query to update movie. It took {} ms", System.currentTimeMillis() - startTime);
    }

    @Override
    public List<Movie> searchByTitle(String searchWord, int page) {
        log.info("Start processing query to search movies by title: {}. Page requested: {}", searchWord, page);
        long startTime = System.currentTimeMillis();

        int offset = moviesPerPage * page;
        String likeClause = "%" + searchWord + "%";

        List<Movie> movies =
                jdbcTemplate.query(searchByTitleSql, ALL_MOVIES_ROW_MAPPER, likeClause, likeClause, moviesPerPage, offset);

        log.info("Finish processing query to search movies by title. It took {} ms", System.currentTimeMillis() - startTime);
        return movies;
    }

    @Override
    public List<Movie> getMoviesForExport() {
        log.info("Start processing query to get all movies for export");
        long startTime = System.currentTimeMillis();

        List<Movie> movies = jdbcTemplate.query(getMoviesForExportSql, MOVIE_ROW_MAPPER);

        log.info("Finish processing query to get all movies for export. It took {} ms", System.currentTimeMillis() - startTime);
        return movies;
    }

    String getQueryWithSortClause(List<SortStrategy> sortStrategies, String initialQuery) {
        if (sortStrategies.size() == 0) {
            return initialQuery;
        }

        log.debug("Valid sort parameters found. Generating sort clause");

        StringBuilder sortClauseBuilder = new StringBuilder(initialQuery)
                .append(" ORDER BY ");

        Iterator<SortStrategy> iterator = sortStrategies.iterator();
        while (iterator.hasNext()) {
            SortStrategy sortStrategy = iterator.next();

            String sortField = sortStrategy.getField();
            String sortType = sortStrategy.getType();
            sortClauseBuilder.append(sortField).append(" ").append(sortType);

            if (iterator.hasNext()) {
                sortClauseBuilder.append(", ");
            }
        }

        String sortQuery = sortClauseBuilder.toString();

        log.debug("Sort clause generated: {}", sortQuery);

        return sortQuery;
    }

    private MapSqlParameterSource getSQLMapByMovie(Movie movie) {
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("movieId", movie.getId());
        parameters.addValue("nameRussian", movie.getNameRussian());
        parameters.addValue("nameNative", movie.getNameNative());
        parameters.addValue("yearOfRelease", movie.getYearOfRelease());
        parameters.addValue("description", movie.getDescription());
        parameters.addValue("rating", movie.getRating());
        parameters.addValue("price", movie.getPrice());
        parameters.addValue("picturePath", movie.getPicturePath());
        return parameters;
    }

    @Autowired
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Autowired
    public void setGetAllMoviesSql(String getAllMoviesSql) {
        this.getAllMoviesSql = getAllMoviesSql;
    }

    @Autowired
    public void setGetThreeRandomMovieSql(String getThreeRandomMovieSql) {
        this.getThreeRandomMovieSql = getThreeRandomMovieSql;
    }

    @Autowired
    public void setGetByGenreSql(String getByGenreSql) {
        this.getByGenreSql = getByGenreSql;
    }

    @Autowired
    public void setGetByIdSql(String getByIdSql) {
        this.getByIdSql = getByIdSql;
    }

    @Autowired
    public void setAddMovieSql(String addMovieSql) {
        this.addMovieSql = addMovieSql;
    }

    @Autowired
    public void setUpdateMovieSql(String updateMovieSql) {
        this.updateMovieSql = updateMovieSql;
    }

    @Autowired
    public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    @Autowired
    public void setSearchByTitleSql(String searchByTitleSql) {
        this.searchByTitleSql = searchByTitleSql;
    }

    @Autowired
    public void setGetMoviesForExportSql(String getMoviesForExportSql) {
        this.getMoviesForExportSql = getMoviesForExportSql;
    }

    @Value("${pagination.movies.per.page}")
    public void setMoviesPerPage(int moviesPerPage) {
        this.moviesPerPage = moviesPerPage;
    }
}
