package com.belosh.movieland.dao.jdbc.extractor;

import com.belosh.movieland.dao.jdbc.mapper.CountryRowMapper;
import com.belosh.movieland.entity.Country;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MovieCountriesExtractor implements ResultSetExtractor<Map<Integer, List<Country>>> {

    private final CountryRowMapper COUNTRY_ROW_MAPPER = new CountryRowMapper();

    @Override
    public Map<Integer, List<Country>> extractData(ResultSet resultSet) throws SQLException {
        Map<Integer, List<Country>> movieCountries = new HashMap<>();

        while (resultSet.next()) {
            Integer movieId = resultSet.getInt("movie_id");

            Country country = COUNTRY_ROW_MAPPER.mapRow(resultSet, 0);

            List<Country> countries = movieCountries.computeIfAbsent(movieId, k -> new ArrayList<>());
            countries.add(country);
        }
        return movieCountries;
    }
}
