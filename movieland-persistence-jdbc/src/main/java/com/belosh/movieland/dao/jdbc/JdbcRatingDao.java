package com.belosh.movieland.dao.jdbc;

import com.belosh.movieland.dao.RatingDao;
import com.belosh.movieland.dao.jdbc.extractor.MovieRatingExtractor;
import com.belosh.movieland.model.TotalRating;
import com.belosh.movieland.model.UserRating;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

@Repository
public class JdbcRatingDao implements RatingDao {
    private final Logger log = LoggerFactory.getLogger(getClass());
    private final MovieRatingExtractor RATING_EXTRACTOR = new MovieRatingExtractor();

    private JdbcTemplate jdbcTemplate;

    private String saveRatingSql;
    private String getRatingSql;
    private String deleteRatingSql;

    public Map<Integer, TotalRating> getRatings() {
        log.info("Start execution query to get all ratings");
        long startTime = System.currentTimeMillis();

        Map<Integer, TotalRating> ratings = jdbcTemplate.query(getRatingSql, RATING_EXTRACTOR);

        log.info("Finish execution query to get all ratings: It took {} ms", System.currentTimeMillis() - startTime);
        return ratings;
    }

    public void saveBufferRatings(Map<Integer, List<UserRating>> ratings) {
        log.info("Start execution query to save movie ratings");
        long startTime = System.currentTimeMillis();

        ratings.forEach((movieId, userRatings) -> {
            jdbcTemplate.batchUpdate(saveRatingSql, new BatchPreparedStatementSetter() {
                @Override
                public void setValues(PreparedStatement ps, int i) throws SQLException {
                    ps.setInt(1, movieId);
                    ps.setInt(2, userRatings.get(i).getUserId());
                    ps.setDouble(3, userRatings.get(i).getRating());
                }

                @Override
                public int getBatchSize() {
                    return userRatings.size();
                }
            });
        });

        log.info("Finish execution query to save movie ratings: It took {} ms", System.currentTimeMillis() - startTime);
    }

    @Override
    public void deleteRelatedRatings(Map<Integer, List<UserRating>> ratings) {
        log.info("Start execution query to delete old movie ratings");
        long startTime = System.currentTimeMillis();

        ratings.forEach((movieId, userRatings) -> {
            jdbcTemplate.batchUpdate(deleteRatingSql, new BatchPreparedStatementSetter() {
                @Override
                public void setValues(PreparedStatement ps, int i) throws SQLException {
                    ps.setInt(1, movieId);
                    ps.setInt(2, userRatings.get(i).getUserId());
                }

                @Override
                public int getBatchSize() {
                    return userRatings.size();
                }
            });
        });

        log.info("Finish execution query to delete old movie ratings: It took {} ms", System.currentTimeMillis() - startTime);
    }

    @Autowired
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Autowired
    public void setSaveRatingSql(String saveRatingSql) {
        this.saveRatingSql = saveRatingSql;
    }

    @Autowired
    public void setGetRatingSql(String getRatingSql) {
        this.getRatingSql = getRatingSql;
    }

    @Autowired
    public void setDeleteRatingSql(String deleteRatingSql) {
        this.deleteRatingSql = deleteRatingSql;
    }
}
