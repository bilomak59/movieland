package com.belosh.movieland.dao.jdbc.mapper;

import com.belosh.movieland.entity.User;
import com.belosh.movieland.report.ReportOutputType;
import com.belosh.movieland.report.ReportRequest;
import com.belosh.movieland.report.ReportStatus;
import com.belosh.movieland.report.ReportType;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ReportRequestRowMapper implements RowMapper<ReportRequest> {
    @Nullable
    @Override
    public ReportRequest mapRow(ResultSet rs, int rowNum) throws SQLException {
        Date date = rs.getDate("period");

        User user = new User();
        user.setId(rs.getInt("requester"));
        user.setNickname(rs.getString("name"));
        user.setEmail(rs.getString("email"));

        ReportRequest reportRequest = new ReportRequest();
        reportRequest.setReportId(rs.getString("reportId"));
        reportRequest.setPeriod(date == null ? null : date.toLocalDate());
        reportRequest.setReportType(ReportType.getByName(rs.getString("reportType")));
        reportRequest.setReportOutputType(ReportOutputType.getByName(rs.getString("reportOutputType")));
        reportRequest.setRequester(user);
        reportRequest.setReportStatus(ReportStatus.getByName(rs.getString("reportStatus")));
        reportRequest.setReportLink(rs.getString("reportLink"));

        return reportRequest;
    }
}
