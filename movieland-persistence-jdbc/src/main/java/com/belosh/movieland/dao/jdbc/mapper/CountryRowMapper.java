package com.belosh.movieland.dao.jdbc.mapper;

import com.belosh.movieland.entity.Country;
import com.belosh.movieland.entity.Genre;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CountryRowMapper implements RowMapper<Country> {
    @Nullable
    @Override
    public Country mapRow(ResultSet resultSet, int i) throws SQLException {
        Country country = new Country();
        country.setId(resultSet.getInt("id"));
        country.setName(resultSet.getString("name"));

        return country;
    }
}
