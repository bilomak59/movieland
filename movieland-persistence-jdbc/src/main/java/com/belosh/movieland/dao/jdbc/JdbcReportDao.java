package com.belosh.movieland.dao.jdbc;

import com.belosh.movieland.dao.ReportDao;
import com.belosh.movieland.dao.jdbc.mapper.ReportMovieRowMapper;
import com.belosh.movieland.dao.jdbc.mapper.ReportRequestRowMapper;
import com.belosh.movieland.dao.jdbc.mapper.ReportUserRowMapper;
import com.belosh.movieland.entity.User;
import com.belosh.movieland.report.ReportMovie;
import com.belosh.movieland.report.ReportRequest;
import com.belosh.movieland.report.ReportUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public class JdbcReportDao implements ReportDao {
    private final Logger log = LoggerFactory.getLogger(getClass());

    private final static ReportMovieRowMapper MOVIE_MAPPER = new ReportMovieRowMapper();
    private final static ReportUserRowMapper USER_MAPPER = new ReportUserRowMapper();
    private final static ReportRequestRowMapper REPORT_MAPPER = new ReportRequestRowMapper();

    private JdbcTemplate jdbcTemplate;
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private String getAllMoviesReportSql;
    private String getAllMoviesReportByPeriodSql;
    private String getTopFiveActiveUsersSql;
    private String getFinishedReportsSql;
    private String insertFinishedReportSql;

    @Override
    public List<ReportMovie> getAllMovies() {
        log.info("Start processing query to get all movies for report");
        long startTime = System.currentTimeMillis();

        List<ReportMovie> movies = jdbcTemplate.query(getAllMoviesReportSql, MOVIE_MAPPER);

        log.info("Finish processing query to get all movies for report. It took {} ms", System.currentTimeMillis() - startTime);
        return movies;
    }

    @Override
    public List<ReportMovie> getMoviesByPeriod(LocalDate period) {
        log.info("Start processing query to get all movies for report by period");
        long startTime = System.currentTimeMillis();

        List<ReportMovie> movies = jdbcTemplate.query(getAllMoviesReportByPeriodSql, MOVIE_MAPPER, period);

        log.info("Finish processing query to get all movies for report by period. It took {} ms", System.currentTimeMillis() - startTime);
        return movies;
    }

    @Override
    public List<ReportUser> getTopActiveUsers() {
        log.info("Start processing query to get top active users");
        long startTime = System.currentTimeMillis();

        List<ReportUser> users = jdbcTemplate.query(getTopFiveActiveUsersSql, USER_MAPPER);

        log.info("Finish processing query to get all movies for report by period. It took {} ms", System.currentTimeMillis() - startTime);
        return users;
    }

    @Override
    public List<ReportRequest> getFinishedReportRequests(User user) {
        log.info("Start processing query to get finished report requests by user: {}", user);
        long startTime = System.currentTimeMillis();

        List<ReportRequest> users = jdbcTemplate.query(getFinishedReportsSql, REPORT_MAPPER, user.getId());

        log.info("Finish processing query to get finished report requests by user. It took {} ms", System.currentTimeMillis() - startTime);
        return users;
    }

    @Override
    public void saveReportDetails(ReportRequest reportRequest) {
        log.info("Start processing query to save report request: {}", reportRequest);
        long startTime = System.currentTimeMillis();

        MapSqlParameterSource reportRequestParameters = getReportRequestParameters(reportRequest);

        namedParameterJdbcTemplate.update(insertFinishedReportSql, reportRequestParameters);

        log.info("Finish processing query to save report request. It took {} ms", System.currentTimeMillis() - startTime);
    }

    private MapSqlParameterSource getReportRequestParameters(ReportRequest reportRequest) {
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("reportId", reportRequest.getReportId());
        parameters.addValue("period", reportRequest.getPeriod());
        parameters.addValue("reportType", reportRequest.getReportType().getName());
        parameters.addValue("reportOutputType", reportRequest.getReportOutputType().getName());
        parameters.addValue("requester", reportRequest.getRequester().getId());
        parameters.addValue("reportStatus", reportRequest.getReportStatus().getName());
        parameters.addValue("reportLink", reportRequest.getReportLink());
        return parameters;
    }

    @Autowired
    public void setGetAllMoviesReportSql(String getAllMoviesReportSql) {
        this.getAllMoviesReportSql = getAllMoviesReportSql;
    }

    @Autowired
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Autowired
    public void setGetAllMoviesReportByPeriodSql(String getAllMoviesReportByPeriodSql) {
        this.getAllMoviesReportByPeriodSql = getAllMoviesReportByPeriodSql;
    }

    @Autowired
    public void setGetTopFiveActiveUsersSql(String getTopFiveActiveUsersSql) {
        this.getTopFiveActiveUsersSql = getTopFiveActiveUsersSql;
    }

    @Autowired
    public void setGetFinishedReportsSql(String getFinishedReportsSql) {
        this.getFinishedReportsSql = getFinishedReportsSql;
    }

    @Autowired
    public void setInsertFinishedReportSql(String insertFinishedReportSql) {
        this.insertFinishedReportSql = insertFinishedReportSql;
    }

    @Autowired
    public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }
}
