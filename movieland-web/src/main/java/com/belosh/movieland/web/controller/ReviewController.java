package com.belosh.movieland.web.controller;

import com.belosh.movieland.entity.Review;
import com.belosh.movieland.model.Role;
import com.belosh.movieland.service.ReviewService;
import com.belosh.movieland.service.impl.security.entity.AuthPrincipal;
import com.belosh.movieland.web.security.RequiredRole;
import com.belosh.movieland.web.security.SecurityContextHolder;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;


@RestController
@RequestMapping(path = "/review", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class ReviewController {
    private final Logger log = LoggerFactory.getLogger(getClass());

    private ReviewService reviewService;
    private ObjectMapper objectMapper;

    @RequestMapping(method = RequestMethod.POST)
    @RequiredRole({Role.ADMIN, Role.USER})
    public void add(@RequestBody String request) {

        try {
            log.info("Parsing POST request to add review: {}", request);

            Review review = objectMapper.readValue(request, Review.class);
            AuthPrincipal principal = (AuthPrincipal) SecurityContextHolder.getPrincipal();
            review.setUser(principal.getUser());

            log.info("Sending request to add review: {}", review);
            long startTime = System.currentTimeMillis();
            reviewService.add(review);
            log.info("Review was added. It took {} ms", System.currentTimeMillis() - startTime);
        } catch (IOException e) {
            log.error("Unable to parse request from user", e);
            throw new RuntimeException("Unable to parse request from user", e);
        }
    }

    @Autowired
    public void setReviewService(ReviewService reviewService) {
        this.reviewService = reviewService;
    }

    @Autowired
    public void setObjectMapper(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }
}
