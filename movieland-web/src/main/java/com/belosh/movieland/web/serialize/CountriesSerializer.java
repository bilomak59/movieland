package com.belosh.movieland.web.serialize;

import com.belosh.movieland.entity.Country;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.util.List;

public class CountriesSerializer extends JsonSerializer<List<Country>> {

    @Override
    public void serialize(List<Country> value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        gen.writeStartArray();
        for(Country country : value) {
            gen.writeString(country.getName());
        }
        gen.writeEndArray();
    }
}
