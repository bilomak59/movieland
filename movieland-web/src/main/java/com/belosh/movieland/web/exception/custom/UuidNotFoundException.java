package com.belosh.movieland.web.exception.custom;

public class UuidNotFoundException extends RuntimeException {

    public UuidNotFoundException(String message) {
        super(message);
    }
}
