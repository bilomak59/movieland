package com.belosh.movieland.web.controller;

import com.belosh.movieland.entity.User;
import com.belosh.movieland.model.Credentials;
import com.belosh.movieland.service.UserService;
import com.belosh.movieland.service.impl.security.SecurityService;
import com.belosh.movieland.service.impl.security.entity.AuthPrincipal;
import com.belosh.movieland.web.exception.custom.UuidNotFoundException;
import com.belosh.movieland.web.security.SecurityContextHolder;
import com.belosh.movieland.web.vo.UserVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class SecurityController {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private UserService userService;
    private SecurityService securityService;

    public SecurityController(UserService userService, SecurityService securityService) {
        this.userService = userService;
        this.securityService = securityService;
    }

    @RequestMapping(path = "/login", method = RequestMethod.POST, consumes = "application/json")
    public UserVO login(@RequestBody Credentials credentials) {
        log.info("Starting processing request to get user by credentials: {}", credentials);
        long startTime = System.currentTimeMillis();

        if (credentials.getEmail() == null) {
            throw new IllegalArgumentException("Email was not specified in request");
        }
        if (credentials.getPassword() == null) {
            throw new IllegalArgumentException("Password wasn't specified in request");
        }

        User user = userService.getUserByCredentials(credentials);
        String uuid = securityService.cacheAuthUser(user);

        log.info("Finished processing request to get user by credentials. It took {} ms", System.currentTimeMillis() - startTime);

        return new UserVO(user.getNickname(), user.getRole(), uuid);
    }

    @RequestMapping(path = "/logout", method = RequestMethod.DELETE)
    public ResponseEntity<String> logout(@RequestHeader("uuid") String uuid) {
        log.info("User {} request logout from the system");
        securityService.removeFromCache(uuid);
        return new ResponseEntity<>(HttpStatus.OK); // https://tools.ietf.org/html/rfc7231#section-4.3.5
    }

    @RequestMapping(path = "/security/user", method = RequestMethod.GET)
    public UserVO role() {
        AuthPrincipal authPrincipal = (AuthPrincipal) SecurityContextHolder.getPrincipal();
        if (authPrincipal == null) {
            throw new UuidNotFoundException("Uuid has not been provided to get related user");
        }
        log.info("User details requested for principal: {}", authPrincipal);
        User user = authPrincipal.getUser();
        return new UserVO(user.getNickname(), user.getRole(), "");
    }
}
