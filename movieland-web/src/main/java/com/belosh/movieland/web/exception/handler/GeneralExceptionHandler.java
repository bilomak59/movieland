package com.belosh.movieland.web.exception.handler;

import com.belosh.movieland.service.impl.security.exception.AuthRequiredException;
import com.belosh.movieland.service.impl.security.exception.UuidExpirationException;
import com.belosh.movieland.web.exception.custom.UuidNotFoundException;
import com.belosh.movieland.web.vo.ExceptionVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class GeneralExceptionHandler extends ResponseEntityExceptionHandler {

    private final Logger log = LoggerFactory.getLogger(getClass());

    /*
    MOVIE CONTROLLER EXCEPTIONS
     */
    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<Object> handleInvalidRequest(Exception exception, WebRequest request) {
        log.warn("Request failed with exception: ", exception);

        ExceptionVO exceptionVO = new ExceptionVO();
        exceptionVO.setTitle("Not valid request parameters");
        exceptionVO.setReason(exception.getMessage());

        return handleExceptionInternal(exception, exceptionVO, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    /*
    SECURITY CONTROLLER EXCEPTIONS
     */
    @ExceptionHandler(EmptyResultDataAccessException.class)
    public ResponseEntity<Object> handleInvalidCredentials(Exception exception, WebRequest request) {
        log.warn("Request failed with invalid credentials exception: ", exception);

        ExceptionVO exceptionVO = new ExceptionVO();
        exceptionVO.setTitle("Invalid credentials");
        exceptionVO.setReason(exception.getMessage());

        return handleExceptionInternal(exception, exceptionVO, new HttpHeaders(), HttpStatus.UNAUTHORIZED, request);
    }

    /*
    SECURITY INTERCEPTOR
     */
    @ExceptionHandler(AuthRequiredException.class)
    public ResponseEntity<Object> handleAuthRequired(Exception exception, WebRequest request) {
        log.warn("Request failed. Provided UUID is not valid: ", exception);

        ExceptionVO exceptionVO = new ExceptionVO();
        exceptionVO.setTitle("Authentication required");
        exceptionVO.setReason(exception.getMessage());

        return handleExceptionInternal(exception, exceptionVO, new HttpHeaders(), HttpStatus.UNAUTHORIZED, request);
    }

    @ExceptionHandler({UuidExpirationException.class, UuidNotFoundException.class})
    public ResponseEntity<Object> uuidExpired(Exception exception, WebRequest request) {
        log.warn("Request failed. Provided UUID has been expired: ", exception);

        ExceptionVO exceptionVO = new ExceptionVO();
        exceptionVO.setTitle("UUID expired. Authentication required");
        exceptionVO.setReason(exception.getMessage());

        return handleExceptionInternal(exception, exceptionVO, new HttpHeaders(), HttpStatus.UNAUTHORIZED, request);
    }

    /*
    UNHANDLED EXCEPTION
     */
    @ExceptionHandler(Throwable.class)
    public ResponseEntity<Object> handleUncaughtException(Exception exception, WebRequest request) {
        log.error("Request failed with internal error exception: ", exception);

        ExceptionVO exceptionVO = new ExceptionVO();
        exceptionVO.setTitle("Internal server error");
        exceptionVO.setReason(exception.getMessage());

        return handleExceptionInternal(exception, exceptionVO, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }

}