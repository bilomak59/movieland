package com.belosh.movieland.web.security;

import java.security.Principal;

public class SecurityContextHolder {

    private static final ThreadLocal<Principal> PRINCIPAL_THREAD_LOCAL = new ThreadLocal<>();

    public static Principal getPrincipal() {
        return PRINCIPAL_THREAD_LOCAL.get();
    }

    public static void setPrincipal(Principal user) {
        PRINCIPAL_THREAD_LOCAL.set(user);
    }

    public static void clearPrincipal() {
        PRINCIPAL_THREAD_LOCAL.remove();
    }
}
