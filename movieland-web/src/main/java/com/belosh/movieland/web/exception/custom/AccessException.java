package com.belosh.movieland.web.exception.custom;

public class AccessException extends RuntimeException {
    public AccessException(String message) {
        super(message);
    }
}
