package com.belosh.movieland.web.vo;

import com.belosh.movieland.model.Role;

public class UserVO {
    private String nickname;
    private Role role;
    private String uuid;

    public UserVO(String nickname, Role role, String uuid) {
        this.nickname = nickname;
        this.role = role;
        this.uuid = uuid;
    }

    public String getNickname() {
        return nickname;
    }

    public String getUuid() {
        return uuid;
    }

    public Role getRole() {
        return role;
    }

    @Override
    public String toString() {
        return "UserVO{" +
                "nickname='" + nickname + '\'' +
                ", role=" + role +
                ", uuid='" + uuid + '\'' +
                '}';
    }
}
