package com.belosh.movieland.web.serialize;

import com.belosh.movieland.entity.Movie;
import com.belosh.movieland.web.vo.ExportMovieVO;
import com.belosh.movieland.web.vo.MovieVO;

import java.util.ArrayList;
import java.util.List;

public class VOMapper {
    public static List<MovieVO> map(List<Movie> movies) {
        List<MovieVO> movieVOs = new ArrayList<>();
        for (Movie movie : movies) {
            movieVOs.add(map(movie));
        }
        return movieVOs;
    }

    public static List<ExportMovieVO> mapForExport(List<Movie> movies) {
        List<ExportMovieVO> exportMovieVOS = new ArrayList<>();
        for (Movie movie : movies) {
            exportMovieVOS.add(mapForExport(movie));
        }
        return exportMovieVOS;
    }

    public static ExportMovieVO mapForExport(Movie movie) {
        ExportMovieVO movieVO = new ExportMovieVO();
        movieVO.setNameRussian(movie.getNameRussian());
        movieVO.setNameNative(movie.getNameNative());
        movieVO.setDescription(movie.getDescription());
        movieVO.setYearOfRelease(movie.getYearOfRelease());
        movieVO.setRating(movie.getRating());
        movieVO.setPrice(movie.getPrice());
        movieVO.setPicturePath(movie.getPicturePath());
        movieVO.setCountries(movie.getCountries());
        movieVO.setGenres(movie.getGenres());
        return movieVO;
    }

    public static MovieVO map(Movie movie) {
        MovieVO movieVO = new MovieVO();
        movieVO.setId(movie.getId());
        movieVO.setNameRussian(movie.getNameRussian());
        movieVO.setNameNative(movie.getNameNative());
        movieVO.setYearOfRelease(movie.getYearOfRelease());
        movieVO.setRating(movie.getRating());
        movieVO.setPrice(movie.getPrice());
        movieVO.setPicturePath(movie.getPicturePath());
        return movieVO;
    }
}
