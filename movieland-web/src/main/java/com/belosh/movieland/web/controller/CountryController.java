package com.belosh.movieland.web.controller;

import com.belosh.movieland.entity.Country;
import com.belosh.movieland.service.CountryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/country", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class CountryController {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private CountryService countryService;

    @RequestMapping(method = RequestMethod.GET)
    public List<Country> getAll() {
        log.info("Start processing request to get all countries");
        long startTime = System.currentTimeMillis();
        List<Country> countries = countryService.getAll();
        log.info("Finish processing request to get all countries. It took {} ms", System.currentTimeMillis() - startTime);
        return countries;
    }

    @Autowired
    public void setCountryService(CountryService countryService) {
        this.countryService = countryService;
    }
}
