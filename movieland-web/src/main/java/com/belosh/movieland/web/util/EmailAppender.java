package com.belosh.movieland.web.util;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.net.SMTPAppender;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.boolex.EvaluationException;
import ch.qos.logback.core.boolex.EventEvaluator;
import ch.qos.logback.core.boolex.EventEvaluatorBase;

public class EmailAppender extends SMTPAppender {

    public EmailAppender() {
        EventEvaluator<ILoggingEvent> onWarn = new EventEvaluatorBase<ILoggingEvent>() {
            @Override
            public boolean evaluate(ILoggingEvent event) throws NullPointerException, EvaluationException {
                return event.getLevel().levelInt >= Level.WARN_INT;
            }
        };

        onWarn.setContext(getContext());
        onWarn.setName("MovieLandIssue");
        onWarn.start();

        eventEvaluator = onWarn;
        super.start();
    }
}
