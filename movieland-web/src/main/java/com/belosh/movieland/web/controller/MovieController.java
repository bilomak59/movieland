package com.belosh.movieland.web.controller;

import com.belosh.movieland.entity.Movie;
import com.belosh.movieland.model.*;

import com.belosh.movieland.model.Currency;
import com.belosh.movieland.service.MovieService;
import com.belosh.movieland.service.impl.security.entity.AuthPrincipal;
import com.belosh.movieland.service.impl.util.MovieRateService;
import com.belosh.movieland.web.security.RequiredRole;
import com.belosh.movieland.web.security.SecurityContextHolder;
import com.belosh.movieland.web.serialize.VOMapper;
import com.belosh.movieland.web.vo.ExportMovieVO;
import com.belosh.movieland.web.vo.MovieRateVO;
import com.belosh.movieland.web.vo.MovieVO;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.*;

import static com.belosh.movieland.model.Currency.UAH;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping(path = "movie", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class MovieController {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private MovieService movieService;
    private MovieRateService movieRateService;
    private ObjectMapper objectMapper;

    @RequestMapping(method = RequestMethod.GET)
    public List<MovieVO> getAll(@RequestParam(required = false) LinkedHashMap<String, String> sortParams) {
        log.info("Starting processing request to get all movies");
        long startTime = System.currentTimeMillis();

        List<SortStrategy> sortStrategies = validateSortParams(sortParams);

        List<Movie> movies = movieService.getAll(sortStrategies);
        List<MovieVO> movieVOList = VOMapper.map(movies);

        log.info("Finished processing request to get all movies. It took {} ms", System.currentTimeMillis() - startTime);
        return movieVOList;
    }

    @RequestMapping(method = POST)
    @RequiredRole(Role.ADMIN)
    public void add(@RequestBody Movie movie) {
        log.info("Parsing POST request to add movie: {}", movie);
        long startTime = System.currentTimeMillis();

        log.info("Sending request to add movie: {}", movie);
        movieService.add(movie);
        log.info("Movie has been added. It took {} ms", System.currentTimeMillis() - startTime);
    }

    @RequestMapping(path = "/{movieId}", method = RequestMethod.PUT)
    @RequiredRole(Role.ADMIN)
    public void update(@PathVariable("movieId") int movieId, @RequestBody Movie movie) {
        log.info("Parsing PUT request to update movie: {}", movie);
        long startTime = System.currentTimeMillis();

        movie.setId(movieId);

        log.info("Sending request to update movie: {}", movie);
        movieService.update(movie);
        log.info("Movie has been updated. It took {} ms", System.currentTimeMillis() - startTime);
    }

    @RequestMapping(path = "/random", method = RequestMethod.GET)
    public List<Movie> getThreeRandom() {
        log.info("Starting processing request to get three random movies");
        long startTime = System.currentTimeMillis();

        List<Movie> movies = movieService.getThreeRandom();

        log.info("Finished processing request to get three random movies. It took {} ms", System.currentTimeMillis() - startTime);
        return movies;
    }

    @RequestMapping(path = "/genre/{genreId}", method = RequestMethod.GET)
    public List<Movie> getByGenre(@PathVariable("genreId") int genreId, @RequestParam(required = false) LinkedHashMap<String, String> sortParams) {
        log.info("Starting processing request to get movies by genre id");
        long startTime = System.currentTimeMillis();

        List<SortStrategy> sortStrategies = validateSortParams(sortParams);

        List<Movie> movies = movieService.getByGenre(genreId, sortStrategies);

        log.info("Finished processing request to get movies by genre id. It took {} ms", System.currentTimeMillis() - startTime);
        return movies;
    }

    @RequestMapping(path="/{movieId}/rate", method = POST)
    @RequiredRole(Role.USER)
    public void rateMovie(@PathVariable("movieId") int movieId, @RequestBody MovieRateVO movieRateVO) {
        log.info("Starting processing request to set movie rating: {} to movie: {}", movieRateVO, movieId);
        long startTime = System.currentTimeMillis();

        int userId = ((AuthPrincipal) SecurityContextHolder.getPrincipal()).getUser().getId();

        double rating = movieRateVO.getRating();

        UserRating userRating = new UserRating();
        userRating.setUserId(userId);
        userRating.setRating(rating);

        movieRateService.rateMovie(movieId, userRating);

        log.info("Finished processing request to set movie rating. It took {} ms", System.currentTimeMillis() - startTime);
    }

    @RequestMapping(path = "/{movieId}", method = RequestMethod.GET)
    public Movie getById(@PathVariable("movieId") int movieId, @RequestParam(required = false, name = "currency") Currency currencyParam) {
        log.info("Starting processing request to get movie by id. Currency parameter: {}", currencyParam);
        long startTime = System.currentTimeMillis();

        Currency currency = currencyParam == null ? UAH : currencyParam;
        Movie movie = movieService.getById(movieId, currency);

        log.info("Finished processing request to get movie by id. It took {} ms", System.currentTimeMillis() - startTime);
        return movie;
    }

    @RequestMapping(path = "/import", method = RequestMethod.POST)
    public void importMovies(@RequestBody List<Movie> movies) {
        log.info("Starting processing import for movies: {}", movies);
        long startTime = System.currentTimeMillis();

        movieService.importMovies(movies);

        log.info("Finished processing exportMovies. It took {} ms", System.currentTimeMillis() - startTime);
    }

    @RequestMapping(path = "/export",
            method = RequestMethod.POST,
            produces = {MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE})
    public List<ExportMovieVO> exportMovies() {
        log.info("Starting processing exportMovies.");
        long startTime = System.currentTimeMillis();

        List<Movie> movies = movieService.exportMovies();

        log.info("Finished processing exportMovies. It took {} ms", System.currentTimeMillis() - startTime);
        return VOMapper.mapForExport(movies);
    }

    @RequestMapping(path = "/search", method = RequestMethod.GET)
    public List<MovieVO> getById(@RequestParam(name = "title") String searchWord,
                               @RequestParam(required = false, name = "page") int page) {
        log.info("Starting processing request to search for movie by title with word: {}", searchWord);
        long startTime = System.currentTimeMillis();

        List<Movie> movies = movieService.searchByTitle(searchWord, page);
        List<MovieVO> movieVOList = VOMapper.map(movies);

        log.info("Finished processing request to search for movie by title. It took {} ms", System.currentTimeMillis() - startTime);
        return movieVOList;
    }

    List<SortStrategy> validateSortParams(LinkedHashMap<String, String> sortParams) {
        List<SortStrategy> sortStrategies = new ArrayList<>();
        for (Map.Entry<String, String> entry : sortParams.entrySet()) {
            SortStrategy sortStrategy = SortStrategy.getSortStrategy(entry.getKey(), entry.getValue());
            sortStrategies.add(sortStrategy);
        }
        return sortStrategies;
    }

    @Autowired
    public void setMovieService(MovieService movieService) {
        this.movieService = movieService;
    }

    @Autowired
    public void setObjectMapper(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Autowired
    public void setMovieRateService(MovieRateService movieRateService) {
        this.movieRateService = movieRateService;
    }
}
