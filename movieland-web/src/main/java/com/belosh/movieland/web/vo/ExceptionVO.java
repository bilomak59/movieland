package com.belosh.movieland.web.vo;

public class ExceptionVO {
    private String title;
    private String reason;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Override
    public String toString() {
        return "ExceptionVO{" +
                "title='" + title + '\'' +
                ", reason='" + reason + '\'' +
                '}';
    }
}
