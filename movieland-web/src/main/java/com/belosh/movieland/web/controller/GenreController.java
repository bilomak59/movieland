package com.belosh.movieland.web.controller;

import com.belosh.movieland.entity.Genre;
import com.belosh.movieland.service.GenreService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "genre", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class GenreController {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private GenreService genreService;

    @Autowired
    public GenreController(GenreService genreService) {
        this.genreService = genreService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<Genre> getAll() {
        log.info("Starting processing request to get all genres");
        long startTime = System.currentTimeMillis();

        List<Genre> genres = genreService.getAll();

        log.info("Finished processing request to get all genres. It took {} ms", System.currentTimeMillis() - startTime);
        return genres;
    }


}
