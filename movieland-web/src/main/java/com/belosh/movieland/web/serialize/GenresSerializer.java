package com.belosh.movieland.web.serialize;

import com.belosh.movieland.entity.Genre;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.util.List;

public class GenresSerializer extends JsonSerializer<List<Genre>> {
    @Override
    public void serialize(List<Genre> value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        gen.writeStartArray();
        for(Genre genre : value) {
            gen.writeString(genre.getName());
        }
        gen.writeEndArray();
    }
}
