package com.belosh.movieland.web.vo;

public class MovieRateVO {
    private double rating;

    public void setRating(double rating) {
        this.rating = rating;
    }

    public double getRating() {
        return rating;
    }

    @Override
    public String toString() {
        return "MovieRateVO{" +
                "rating=" + rating +
                '}';
    }
}
