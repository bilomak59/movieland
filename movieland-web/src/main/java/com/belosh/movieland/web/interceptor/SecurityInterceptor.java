package com.belosh.movieland.web.interceptor;

import com.belosh.movieland.entity.User;
import com.belosh.movieland.model.Role;
import com.belosh.movieland.service.impl.security.SecurityService;
import com.belosh.movieland.service.impl.security.entity.AuthPrincipal;
import com.belosh.movieland.service.impl.security.exception.AuthRequiredException;
import com.belosh.movieland.web.exception.custom.AccessException;
import com.belosh.movieland.web.security.RequiredRole;
import com.belosh.movieland.web.security.SecurityContextHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;

import static java.lang.String.format;

public class SecurityInterceptor implements HandlerInterceptor {
    private final Logger log = LoggerFactory.getLogger(getClass());

    private SecurityService securityService;

    @Autowired
    public SecurityInterceptor(SecurityService securityService) {
        this.securityService = securityService;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String uuidHeader = request.getHeader("uuid");
        Optional<Role[]> requiredRoles = getRequiredRoles(handler);

        if (!request.getPathInfo().equals("/login") && uuidHeader != null) {
            AuthPrincipal principal = securityService.getPrincipalFromCache(uuidHeader);
            User user = principal.getUser();

            requiredRoles.ifPresent(roles -> {
                if (!validateRole(roles, user.getRole())) {
                    String message = "User role doesn't match required roles: " + Arrays.toString(roles);
                    log.warn(message);
                    throw new AccessException(message);
                }
            });

            // Old approach with wrapper. Keep it for history
            //((UserRequestWrapper) request).setPrincipal(principal);

            // New approach with ThreadLocal
            SecurityContextHolder.setPrincipal(principal);

            MDC.put("user", user.getEmail());
            log.info("User successfully authenticated as: {}. Principal expires at: {}", user, principal.getExpire());
        } else {
            if (requiredRoles.isPresent()) {
                String message = "Request header doesn't contain uuid";
                log.warn(message);
                throw new AuthRequiredException(message);
            }

            log.info("Uuid is not provided. Assuming as a Guest session");
            MDC.put("user", Role.GUEST.toString());
        }

        MDC.put("requestId", UUID.randomUUID().toString());
        return true;
    }

    private Optional<Role[]> getRequiredRoles(Object handler) {
        if (handler instanceof HandlerMethod) {
            HandlerMethod handlerMethod = (HandlerMethod) handler;
            Method method = handlerMethod.getMethod();
            RequiredRole annotation = method.getAnnotation(RequiredRole.class);
            if (annotation != null) {
                return Optional.of(annotation.value());
            }
        }
        return Optional.empty();
    }

    private boolean validateRole(Role[] requiredRoles, Role userRole) {
        for (Role requiredRole : requiredRoles) {
            if (requiredRole.equals(userRole)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, @Nullable Exception ex) throws Exception {
        MDC.clear();
        SecurityContextHolder.clearPrincipal();
    }
}
