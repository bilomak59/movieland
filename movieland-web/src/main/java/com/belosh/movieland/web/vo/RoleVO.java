package com.belosh.movieland.web.vo;

public class RoleVO {
    private String name;

    public RoleVO(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "RoleVO{" +
                "name='" + name + '\'' +
                '}';
    }
}
