package com.belosh.movieland.web.controller.view;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class WebViewController {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @GetMapping(path = "/**", produces = "text/html;charset=UTF-8")
    public String index() {
        log.info("Default home page requested");
        return "index";
    }
}
