package com.belosh.movieland.web.controller;

import com.belosh.movieland.entity.User;
import com.belosh.movieland.report.ReportRequest;
import com.belosh.movieland.model.Role;
import com.belosh.movieland.report.ReportStatus;
import com.belosh.movieland.service.ReportService;
import com.belosh.movieland.service.impl.security.entity.AuthPrincipal;
import com.belosh.movieland.web.security.RequiredRole;
import com.belosh.movieland.web.security.SecurityContextHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(path = "report", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class ReportController {
    private final Logger log = LoggerFactory.getLogger(getClass());

    private ReportService reportService;

    @RequiredRole(Role.ADMIN)
    @RequestMapping(method = RequestMethod.POST)
    public void addReportRequest(@RequestBody ReportRequest reportRequest) {
        log.info("Received request to generate report: {}", reportRequest);
        long startTime = System.currentTimeMillis();

        User user = ((AuthPrincipal) SecurityContextHolder.getPrincipal()).getUser();
        reportRequest.setRequester(user);

        reportService.addReportRequest(reportRequest);

        log.info("Request for report has been added. It tooks {} ms", System.currentTimeMillis() - startTime);
    }

    @RequiredRole(Role.ADMIN)
    @RequestMapping(method = RequestMethod.GET)
    public List<ReportRequest> getFinishedReportLinks() {
        User user = ((AuthPrincipal) SecurityContextHolder.getPrincipal()).getUser();

        log.info("Received request to get Report links related to user: {}", user);
        long startTime = System.currentTimeMillis();

        List<ReportRequest> userReports = reportService.getReportLinks(user);

        log.info("Report requests with link retrieved. It tooks {} ms", System.currentTimeMillis() - startTime);
        return userReports;
    }

    @RequiredRole(Role.ADMIN)
    @RequestMapping(method = RequestMethod.PUT)
    public List<ReportRequest> checkStatus() {
        User user = ((AuthPrincipal) SecurityContextHolder.getPrincipal()).getUser();

        log.info("Received request to check statuses of ReportRequests requested by {}", user);
        long startTime = System.currentTimeMillis();

        List<ReportRequest> userReports = reportService.checkStatus(user);

        log.info("Finished processing request to check statuses of active Report Requests. It tooks {} ms",
                System.currentTimeMillis() - startTime);
        return userReports;
    }

    @RequiredRole(Role.ADMIN)
    @RequestMapping(value = "/{reportId}", method = RequestMethod.DELETE)
    public void removeReport(@PathVariable String reportId) {
        log.info("Received request to remove ReportRequest with ID: {}", reportId);
        long startTime = System.currentTimeMillis();

        reportService.removeReport(reportId);

        log.info("Report has been removed. It tooks {} ms", System.currentTimeMillis() - startTime);
    }

    @RequestMapping(value = "/{filename:.+}", method = RequestMethod.GET)
    public void download(@PathVariable String filename, HttpServletResponse response) throws IOException {
        log.info("Sending request to get report: {}", filename);
        long startTime = System.currentTimeMillis();

        response.addHeader("Content-Disposition", "inline; filename=" + filename);
        String mimeType = "application/octet-stream";
        response.setContentType(mimeType);

        StreamUtils.copy(reportService.getReport(filename), response.getOutputStream());

        log.info("Report has been got. It tooks {} ms", System.currentTimeMillis() - startTime);
    }

    @Autowired
    public void setReportService(ReportService reportService) {
        this.reportService = reportService;
    }
}
