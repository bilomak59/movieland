const genreApi = "/api/v1/genre";

class GenreController {
    render(data) {
        let items = [];
        items.push("<li class='filter-active' id='random-movies'>3 случайные</li>");
        items.push("<li id='all-movies'>Все</li>");

        $.each(data, function (key, val) {
            items.push("<li class='genre-item' data-id='" +  val.id + "'>" + val.name + "</li>");
        });

        let genres = $('#genre-filter');
        genres.append(items);
        console.info("Genres data from backend successfully rendered");
    }

    setUpGenreListener() {
        let that = this;
        $("#genre-filter li").click(function(e){
            let genreId = $(this).data().id;
            GenreController.contentFadeOut();
            $(this).addClass('filter-active');
            setTimeout(function() {
                movieController.refreshByGenre(genreId);
                GenreController.contentFadeIn();
            }, 300);
            history.pushState('', '', '/web/genre/' + genreId);
            console.info("User requested movies by genre: %s. Successfully finished", genreId);
        });
    }

    setUpGetAllMovieListener() {
        $("#all-movies").click(function(e){
            GenreController.contentFadeOut();
            setTimeout(function() {
                movieController.refreshAllMovies();
                GenreController.contentFadeIn();
            }, 300);
            history.pushState('', '', '/web/all');
            console.info("User requested all movies. Successfully finished");
        });
    }

    setUpGetRandomMovieListener() {
        $("#random-movies").click(function(e){
            GenreController.contentFadeOut();
            setTimeout(function() {
                movieController.refreshRandom();
                GenreController.contentFadeIn();
            }, 300);
            history.pushState('', '', '/web/');
            console.info("User requested random movies. Successfully finished");
        });
    }

    refresh() {
        let data = Util.getAjaxData(genreApi);
        this.render(data);
        this.setUpGenreListener();
        this.setUpGetAllMovieListener();
        this.setUpGetRandomMovieListener();
        console.info("Getting out all genres and setup listeners. Successfully finished");

    }

    static contentFadeOut() {
        $('#film-content').removeClass('hide');
        $('#ajaxProgress').removeClass('hide');

        $("#genre-filter li").removeClass('filter-active');
        $(this).addClass('filter-active');

        $("#film-wrapper").fadeTo(100, 0);
        $(".content-item").fadeOut().css('transform', 'scale(0)');
    }

    static contentFadeIn() {
        $(".content-item").fadeIn(100).css('transform', 'scale(1)');
        $("#film-wrapper").fadeTo(300, 1);
        $('#ajaxProgress').addClass('hide');
    }
}