const reviewApi = "/api/v1/review/";

class ReviewController {

    sendAddReview(id) {
        console.info('Starting add review');
        let review= {};
        review.movieId = id;
        review.text = $("#reviewText").val();

        let json = JSON.stringify(review);

        function success() {console.info("Requested review has been added: %s" + json)}
        function error(xhr) {console.error("Request to add review failed: %s", xhr.responseText.title)}

        Util.postAjaxRequest(reviewApi, json, success, error);
        console.info("Request to add review successfully finished: %s", review);
    }
}