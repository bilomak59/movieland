'use strict';

let movieController = new MovieController();
let genreController = new GenreController();
let loginController = new LoginController();
let reviewController = new ReviewController();

// -------------------------------------------------
// # Init Modals
// -------------------------------------------------

function openLoginModal() {
    loginController.showLoginForm();
    setTimeout(function () {
        $('#loginModal').modal('show');
    }, 230);
}

function openRegisterModal() {
    loginController.showRegisterForm();
    setTimeout(function () {
        $('#loginModal').modal('show');
    }, 230);
}

function openLogoutModal() {
    loginController.showLogoutForm();
    setTimeout(function () {
        $('#logoutModal').modal('show');
    }, 230);
}

function openAddMovieModal() {
    movieController.renderAddMovieModal();
    setTimeout(function () {
        $('#addUpdateMovieModal').modal('show');
    }, 230);
}

function openUpdateMovieModal() {
    movieController.renderUpdateMovieModal();
    setTimeout(function () {
        $('#addUpdateMovieModal').modal('show');
    }, 230);
}

// -------------------------------------------------
// # Actions
// -------------------------------------------------
function getMovieDetails(id) {
    history.pushState('', 'MovieLand', '/web/movie/' + id);
    movieController.refreshMovie(id);
}

function sendUpdateMovie() {
    movieController.sendUpdateMovie();
}

function sendAddMovie() {
    movieController.sendAddMovie();
}

function sendAddReview() {
    let filmId = $("#reviewText").val();
    reviewController.sendAddReview(filmId);
    movieController.refreshMovie(filmId);
}

function loginAjax() {
    let jsonCredentials = loginController.getJsonLoginRequest();
    loginController.cleanModal();
    loginController.sendLoginRequest(jsonCredentials);
}

function logoutAjax() {
    loginController.sendLogoutRequest();
}



// ----------------------------------------------------------------
// # On document load
// ----------------------------------------------------------------
jQuery(document).ready(function( $ ) {
    // Header fixed and Back to top button
    $(window).scroll(function() {
      if ($(this).scrollTop() > 100) {
        $('.back-to-top').fadeIn('slow');
        $('#header').addClass('header-fixed');
      } else {
        $('.back-to-top').fadeOut('slow');
        $('#header').removeClass('header-fixed');
      }
    });
    $('.back-to-top').click(function(){
      $('html, body').animate({scrollTop : 0},1500, 'easeInOutExpo');
      return false;
    });

    // Initiate the wowjs
    new WOW().init();

    // Initiate superfish on nav menu
    $('.nav-menu').superfish({
      animation: {opacity:'show'},
      speed: 400
    });

    // Mobile Navigation
    if( $('#nav-menu-container').length ) {
      var $mobile_nav = $('#nav-menu-container').clone().prop({ id: 'mobile-nav'});
      $mobile_nav.find('> ul').attr({ 'class' : '', 'id' : '' });
      $('body').append( $mobile_nav );
      $('body').prepend( '<button type="button" id="mobile-nav-toggle"><i class="fa fa-bars"></i></button>' );
      $('body').append( '<div id="mobile-body-overly"></div>' );
      $('#mobile-nav').find('.menu-has-children').prepend('<i class="fa fa-chevron-down"></i>');

      $(document).on('click', '.menu-has-children i', function(e){
        $(this).next().toggleClass('menu-item-active');
        $(this).nextAll('ul').eq(0).slideToggle();
        $(this).toggleClass("fa-chevron-up fa-chevron-down");
      });

      $(document).on('click', '#mobile-nav-toggle', function(e){
        $('body').toggleClass('mobile-nav-active');
        $('#mobile-nav-toggle i').toggleClass('fa-times fa-bars');
        $('#mobile-body-overly').toggle();
      });

      $(document).click(function (e) {
        var container = $("#mobile-nav, #mobile-nav-toggle");
        if (!container.is(e.target) && container.has(e.target).length === 0) {
         if ( $('body').hasClass('mobile-nav-active') ) {
            $('body').removeClass('mobile-nav-active');
            $('#mobile-nav-toggle i').toggleClass('fa-times fa-bars');
            $('#mobile-body-overly').fadeOut();
          }
        }
      });
    } else if ( $("#mobile-nav, #mobile-nav-toggle").length ) {
      $("#mobile-nav, #mobile-nav-toggle").hide();
    }

    // Smoth scroll on page hash links
    $('a[href*="#"]:not([href="#"])').on('click', function() {
      if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {

        var target = $(this.hash);
        if (target.length) {
          var top_space = 0;

          if( $('#header').length ) {
            top_space = $('#header').outerHeight();

            if( ! $('#header').hasClass('header-fixed') ) {
              top_space = top_space - 20;
            }
          }

          $('html, body').animate({
            scrollTop: target.offset().top - top_space
          }, 1500, 'easeInOutExpo');

          if ( $(this).parents('.nav-menu').length ) {
            $('.nav-menu .menu-active').removeClass('menu-active');
            $(this).closest('li').addClass('menu-active');
          }

          if ( $('body').hasClass('mobile-nav-active') ) {
            $('body').removeClass('mobile-nav-active');
            $('#mobile-nav-toggle i').toggleClass('fa-times fa-bars');
            $('#mobile-body-overly').fadeOut();
          }
          return false;
        }
      }
    });

    // jQuery counterUp
    $('[data-toggle="counter-up"]').counterUp({
      delay: 10,
      time: 1000
    });

//----------------------------------------------------------------
// # Save user info / Init user validation
// ----------------------------------------------------------------
    loginController.validateLocalStorageParameters();

// ----------------------------------------------------------------
//  # Get all genres
// ----------------------------------------------------------------
    genreController.refresh();

    $('#sorting').on("change", function (e) {
        console.log("Requested movies by genre with sort criteria, %s", $('#sorting').val());
        console.log("DEBUG", $(this).val());


        let currentGenreId = $(".filter-active").data().id;
        movieController.refreshByGenre(currentGenreId);
    });

// ----------------------------------------------------------------
// # Parse requested url
// ----------------------------------------------------------------
    var pathname = window.location.pathname;
    console.info('Requested path: %s', pathname);

    if (pathname === "/web/") {
        // # Get random movies
        movieController.refreshRandom();

    } else if (pathname === "/web/all") {
        movieController.refreshAllMovies();

    } else if (pathname.startsWith("/web/genre/")) {
        let genreId = pathname.split('/')[3];
        console.log('Requested genre id: ', genreId);
        movieController.refreshByGenre(genreId);

    } else if (pathname.startsWith('/web/movie/')) {
        let movieId = pathname.split('/')[3];
        console.log('Requested movie id: ', movieId);
        movieController.refreshMovie(movieId);
    } else {
        $('#film-details').html("<h2 class='error text-center'>Ошибочка! Мы старательно искали запрошенную страницу: " + pathname + " , но такой не существует.<h2>")
    }
});
