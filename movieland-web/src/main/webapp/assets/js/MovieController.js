const movieApi = "/api/v1/movie/";
const movieGenreApi = "/api/v1/movie/genre/";
const movieRandomApi = "/api/v1/movie/random";
const movieTemplate = "/assets/template/content-item.htm";

const template = $.ajax({url: movieTemplate,
    success: function () {console.log("[INFO] Template successfully retrieved");},
    error: function (error) {console.log("[ERROR] Unable to read movie template due to: " + JSON.stringify(error));},
    async: false}).responseText;

class MovieController {
    //-----------------------------------------------------------------
    // Rendering
    // ----------------------------------------------------------------
    renderMovies(data) {
        console.log("Start render movies");
        let items = [];
        let contentItemTemplate = $(template).filter('#tpl-film-list').html();


        $.each(data, function (key, val) {
            if (val.picturePath !== undefined) {
                val.picturePath = val.picturePath.split("._V1._")[0] + "._V1._SX215.jpg";
            }
            items.push(Mustache.render(contentItemTemplate, val));
        });

        let filmWrapper = $('#film-wrapper');
        filmWrapper.empty();
        filmWrapper.append(items);

        $('#film-list').removeClass('hide');
        $('#film-details').addClass('hide');
        console.log("Finished rendering movies");
    }

    renderMovie(data) {
        console.log("Start render movie");
        if (data.picturePath !== undefined) {
            data.picturePath = data.picturePath.split("._V1._")[0] + "._V1._SX350.jpg";
        }

        this.enrichAuthData(data);

        let filmDetailsTemplate = $(template).filter('#tpl-film-details').html();
        let renderedTemplate = Mustache.render(filmDetailsTemplate, data);
        let filmDetails = $('#film-details');

        filmDetails.empty();
        filmDetails.append(renderedTemplate);
        filmDetails.removeClass('hide');
        $('#film-list').addClass('hide');
        console.info('Request to render movie has been successfully finished: %s', JSON.stringify(data));
    }

    renderUpdateMovieModal() {
        let description = $('#description').text();
        let nameRussian = $('#nameRussian').text();
        let nameNative = $('#nameNative').text();
        let yearOfRelease = $('#yearOfRelease').text();
        let rating = $('#rating').text();
        let price = $('#price').text();
        let picturePath = $('#picturePath').attr('src');

        console.log(nameRussian + ' ' + picturePath);

        $('#inputDescription').val(description);
        $('#inputNameRussian').val(nameRussian);
        $('#inputNameNative').val(nameNative);
        $('#inputReleaseYear').val(yearOfRelease);
        $('#inputRating').val(rating);
        $('#inputPrice').val(price);
        $('#inputPicturePath').val(picturePath);

        // Change controls
        $('#add-movie-button').addClass('hide');
        $('#update-movie-button').removeClass('hide');
        $('#addUpdateMovieModalTitle').text('Обновить фильм: ' + nameRussian);
        console.info('Rendered update movie modal for movie: %s', nameRussian);
    }

    renderAddMovieModal() {
        $('#add-movie-button').removeClass('hide');
        $('#update-movie-button').addClass('hide');
        $('#addUpdateMovieModalTitle').text('Добавить новый фильм в колекцию');
    }

    enrichAuthData(data) {
        let userRole = localStorage.getItem("movieland-role");
        if (userRole === "ADMIN") {
            data.userRole = true;
            data.adminRole = true;
        } else if (userRole === "USER") {
            data.userRole = true;
            data.adminRole = false;
        } else {
            data.userRole = false;
            data.adminRole = false;
        }
    }

    addSortQuery(api) {
        let sorting = $('#sorting').val();
        console.log("Sort request:" + sorting);
        return api + '?' + sorting;
    }

    addCurrencyQuery(api) {
        let currency = $('#currency').val();
        console.log("Currency request:" + currency);
        return api + '?' + currency;
    }

    refreshByGenre(id) {
        let movieIdApi = movieGenreApi + id;
        let movieByGenreAjaxRequestApi = this.addSortQuery(movieIdApi);
        let data = Util.getAjaxData(movieByGenreAjaxRequestApi);
        this.renderMovies(data);
    }

    refreshRandom() {
        let movieByGenreAjaxRequestApi = this.addSortQuery(movieRandomApi);
        let data = Util.getAjaxData(movieByGenreAjaxRequestApi);
        this.renderMovies(data);
    }

    refreshAllMovies() {
        let movieByGenreAjaxRequestApi = this.addSortQuery(movieApi);
        let data = Util.getAjaxData(movieByGenreAjaxRequestApi);
        this.renderMovies(data);
    }

    refreshMovie(id) {
        let movieIdApi = movieApi + id;
        let getByMovieWithCurrency = this.addCurrencyQuery(movieIdApi);
        let data = Util.getAjaxData(getByMovieWithCurrency);
        this.renderMovie(data);
    }

    //-----------------------------------------------------------------
    // UPDATE OPERATIONS
    // ----------------------------------------------------------------

    sendUpdateMovie() {
        console.info("Starting update movie details");
        let jsonString = this.getMovieDetailsFromForm();

        // Sending PUT request
        let movieId = $("#movieId").val();
        let movieIdApi = movieApi + movieId;
        function success() {console.info("Movie has been successfully updated. Final data: %s", jsonString);}
        function error (errorResponse) {console.error("Error sending update: " + errorResponse.responseText);}
        Util.putAjaxRequest(movieIdApi, jsonString, success, error);

        $('#addUpdateMovieModal').modal('toggle');
        console.info("Finished update movie details");

        // Getting updated information from server back to UI
        this.refreshMovie(movieId);
    }

    sendAddMovie() {
        console.info("Starting add new movie details");
        let jsonString = this.getMovieDetailsFromForm();

        // Sending PUT request
        function success() {
            console.info("Movie has been successfully added. Final data: %s", jsonString);
            $('#addMovieModal').modal('toggle');
        }
        function error (errorResponse) {
            console.error("Error adding new movie: " + errorResponse.responseText);
            alert("Error adding new movie: " + errorResponse.responseText.title);
        }
        Util.postAjaxRequest(movieApi, jsonString, success, error);

        console.info("Finished add new movie details");
        $('#addUpdateMovieModal').modal('toggle');

        this.refreshAllMovies();
    }

    getMovieDetailsFromForm() {
        let movieData = {};
        movieData.nameRussian = $("#inputNameRussian").val();
        movieData.nameNative = $("#inputNameNative").val();
        movieData.yearOfRelease = $("#inputReleaseYear").val();
        movieData.description = $("#inputDescription").val();
        movieData.rating = $("#inputRating").val();
        movieData.price = $("#inputPrice").val();
        movieData.picturePath = $("#inputPicturePath").val();

        return JSON.stringify(movieData);
    }
}
