class Util {
    static getAjaxData(url) {
        let jsonData = [];
        console.log("getAjaxData by url: ", url);
        $.ajax({
            url: url,
            async: false,
            dataType: 'json',
            beforeSend: function(xhr) {
                Util.setUUIDHeaderBeforeSend(xhr);
            },
            success: function (data) {
                jsonData = data;
            }
        });

        return jsonData;
    }

    static getAjaxDataConfigurable(url, success, error) {
        console.log("getAjaxDataConfigurable by url: ", url);
        return $.ajax({
            url: url,
            dataType: 'json',
            success: success,
            error: error,
            async: false,
            beforeSend: function(xhr) {
                Util.setUUIDHeaderBeforeSend(xhr);
            },
        });
    }

    static postAjaxRequest(url, data, success, error) {
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: success,
            error: error,
            contentType: "application/json; charset=UTF-8",
            async: false,
            beforeSend: function(xhr) {
                Util.setUUIDHeaderBeforeSend(xhr);
            },
        });
    }

    static putAjaxRequest(url, data, success, error) {
        $.ajax({
            type: "PUT",
            url: url,
            data: data,
            success: success,
            error: error,
            contentType: "application/json; charset=UTF-8",
            async: false,
            beforeSend: function(xhr) {
                Util.setUUIDHeaderBeforeSend(xhr);
            },
        });
    }

    static deleteAjaxRequest(url, success, error) {
        $.ajax({
            type: "DELETE",
            url: url,
            success: success,
            error: error,
            contentType: "application/json; charset=UTF-8",
            async: false,
            beforeSend: function(xhr) {
                Util.setUUIDHeaderBeforeSend(xhr);
            },
        });
    }

    static setUUIDHeaderBeforeSend(xhr) {
        let uuid = localStorage.getItem("movieland-uuid");
        if (uuid !== null) {
            xhr.setRequestHeader("uuid", uuid);
        }
        console.info("[Util.getAjaxDataConfigurable.beforeSend] uuid: %s", JSON.stringify(uuid));
    }
}