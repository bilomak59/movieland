const loginApi = "/api/v1/login";
const logoutApi = "/api/v1/logout";
const userApi = "/api/v1/security/user";

class LoginController {
    showRegisterForm() {
        $('.loginBox').fadeOut('fast', function () {
            $('.registerBox').fadeIn('fast');
            $('.login-footer').fadeOut('fast', function () {
                $('.register-footer').fadeIn('fast');
            });
            $('.login-title').html('Зарегистрироваться:');
        });
        this.cleanModal();
    }

    showLoginForm() {
        $('#loginModal .registerBox').fadeOut('fast', function () {
            $('.loginBox').fadeIn('fast');
            $('.register-footer').fadeOut('fast', function () {
                $('.login-footer').fadeIn('fast');
            });

            $('#login-title').html('Войти:');
        });
        this.cleanModal();
    }

    showLogoutForm() {
        $('#logoutModal').fadeOut('fast', function () {
            $('.loginBox').fadeIn('fast');
            $('.register-footer').fadeOut('fast', function () {
                $('.login-footer').fadeIn('fast');
            });

            $('.login-title').html('Уже уходите?');
        });
        this.cleanModal();
    }

    cleanModal() {
        $('.error').removeClass('alert alert-danger').html("");
        $('input[type="password"]').val('');
    }

    static shakeModal(title) {
        $('#loginModal .modal-dialog').addClass('shake');
        $('.error').addClass('alert alert-danger').html(title);
        $('input[type="password"]').val('');
        setTimeout(function () {
            $('#loginModal .modal-dialog').removeClass('shake');
        }, 1000);
    }

    getJsonLoginRequest() {
        let credentials = {};
        credentials.email = $("#login_email").val();
        credentials.password = $("#login_password").val();

        return JSON.stringify(credentials);
    }

    sendLoginRequest(jsonCredentials) {
        function success(response) {
            $('#loginModal').modal('toggle');
            localStorage.setItem("movieland-role", response.role);
            localStorage.setItem("movieland-uuid", response.uuid);
            LoginController.setAuthInfo(response.nickname);
            LoginController.addRoleControls(response.role);
            console.info("Session authenticated as: {}", JSON.stringify(response));
        }

        function error (errorResponse) {
            LoginController.shakeModal(JSON.parse(errorResponse.responseText).title);
            console.error("Invalid user details")
        }

        Util.postAjaxRequest(loginApi, jsonCredentials, success, error);
        location.reload();
    }

    sendLogoutRequest() {
        function success (response) {
            $('#logoutModal').modal('toggle');
            localStorage.removeItem("movieland-role");
            localStorage.removeItem("movieland-uuid");
            LoginController.removeAuthInfo();
            LoginController.removeRoleControls();
            console.info("Successfully logged out: %s" + response)
        }

        function error (errorResponse) {
            alert("Logout request failed: " + JSON.parse(errorResponse.responseText).title)
        }

        Util.deleteAjaxRequest(logoutApi, success, error);
        location.reload();
    }

    validateLocalStorageParameters(){
        function success(data) {
            console.info("[localStorage param validation] User role name confirmed from server: %s", data.role);
            localStorage.setItem("movieland-role", data.role);
            localStorage.setItem("movieland-nickname", data.nickname);
            LoginController.setAuthInfo(data.nickname);
            LoginController.addRoleControls(data.role)
        }

        function error(xhr) {
            if (xhr.status === 401) {
                console.warn("[localStorage param validation] Authorisation failed: %s",
                    JSON.parse(xhr.responseText).reason);
                localStorage.removeItem("movieland-uuid");
                localStorage.removeItem("movieland-role");
            } else {
                console.error("Uncaught error. Status: %s. Text: %s", xhr.status, xhr.responseText);
            }
        }

        Util.getAjaxDataConfigurable(userApi, success, error);
    }

    static setAuthInfo(nickname) {
        $('#logout').removeClass('hide');
        $('#nickname').html(nickname);
        $('#login').addClass('hide');
    }

    static removeAuthInfo() {
        $('#login').removeClass('hide');
        $('#logout').addClass('hide');
        $('#nickname').html("Гость");
    }

    static addRoleControls(role) {
        if(role === "ADMIN") {
            $('#add-new-film').removeClass('hide');
            console.log("Adding ADMIN role controls");
        }
    }

    static removeRoleControls() {
        $('#add-new-film').addClass('hide');
        console.log("Removed role related controls on logout");
    }
}