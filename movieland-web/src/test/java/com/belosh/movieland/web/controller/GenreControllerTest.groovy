package com.belosh.movieland.web.controller

import com.belosh.movieland.entity.Genre
import com.belosh.movieland.service.GenreService
import com.belosh.movieland.web.controller.GenreController
import org.junit.Before
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.setup.MockMvcBuilders

import static org.hamcrest.Matchers.hasSize
import static org.hamcrest.core.Is.is
import static org.mockito.Mockito.*
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class GenreControllerTest {

    private MockMvc mockMvc

    @Mock
    private GenreService mockGenreService

    @InjectMocks
    private GenreController genreController

    private List<Genre> genres = new ArrayList<>()


    @Before
    void setup() {
        MockitoAnnotations.initMocks(this)
        mockMvc = MockMvcBuilders.standaloneSetup(genreController).build()

        Genre firstGenre = new Genre(1, "драма")
        genres.add(firstGenre)

        Genre secondGenre = new Genre(2, "криминал")
        genres.add(secondGenre)
    }

    @Test
    void testGetAll() {
        when(mockGenreService.getAll()).thenReturn(genres)
        mockMvc.perform(get("/genre")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath('$', hasSize(2)))

                .andExpect(MockMvcResultMatchers.jsonPath('$[0].id', is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath('$[0].name', is('драма')))

                .andExpect(MockMvcResultMatchers.jsonPath('$[1].id', is(2)))
                .andExpect(MockMvcResultMatchers.jsonPath('$[1].name', is('криминал')))
        verify(mockGenreService, times(1)).getAll()
        verifyNoMoreInteractions(mockGenreService)

    }
}
