package com.belosh.movieland.web.serialize

import com.belosh.movieland.entity.Country
import com.belosh.movieland.entity.Genre
import com.belosh.movieland.web.vo.ExportMovieVO
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.xml.XmlMapper
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class MovieSerializerTest {

    private ExportMovieVO exportMovie = new ExportMovieVO()

    @Before
    void setup() {
        exportMovie.setNameRussian("Побег из Шоушенка")
        exportMovie.setNameNative("The Shawshank Redemption")
        exportMovie.setDescription("Description about film")
        exportMovie.setYearOfRelease(1994)
        exportMovie.setRating(8.9)
        exportMovie.setPrice(123.45)
        exportMovie.setPicturePath("https://image")

        def countryUSA = new Country(1, 'США')
        def countryFrance = new Country(2, 'Франция')

        def genreDrama = new Genre(1, 'драма')
        def genreCrime = new Genre(2, 'криминал')

        def movieGenreList = [genreDrama, genreCrime]
        def movieCountryList = [countryUSA, countryFrance]

        exportMovie.setGenres(movieGenreList)
        exportMovie.setCountries(movieCountryList)
    }

    @Test
    void testSerializersJSON() {
        def validJSON = "{\"nameRussian\":\"Побег из Шоушенка\",\"nameNative\":\"The Shawshank Redemption\",\"yearOfRelease\":1994,\"description\":\"Description about film\",\"rating\":8.9,\"price\":123.45,\"picturePath\":\"https://image\",\"countries\":[\"США\",\"Франция\"],\"genres\":[\"драма\",\"криминал\"]}"

        ObjectMapper objectMapper = new ObjectMapper()

        def response = objectMapper.writeValueAsString(exportMovie)
        print(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(exportMovie))
        Assert.assertEquals(validJSON, response)
    }

    @Test
    void testSerializersXML() {
        def validXML = "<ExportMovieVO><nameRussian>Побег из Шоушенка</nameRussian><nameNative>The Shawshank Redemption</nameNative><yearOfRelease>1994</yearOfRelease><description>Description about film</description><rating>8.9</rating><price>123.45</price><picturePath>https://image</picturePath><countries><country>США</country><country>Франция</country></countries><genres><genre>драма</genre><genre>криминал</genre></genres></ExportMovieVO>"

        XmlMapper xmlMapper = new XmlMapper()

        def response = xmlMapper.writeValueAsString(exportMovie)
        print(xmlMapper.writerWithDefaultPrettyPrinter().writeValueAsString(exportMovie));
        Assert.assertEquals(response, validXML)

    }
}
