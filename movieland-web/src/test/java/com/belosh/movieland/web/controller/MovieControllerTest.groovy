package com.belosh.movieland.web.controller

import com.belosh.movieland.entity.Country
import com.belosh.movieland.entity.Genre
import com.belosh.movieland.entity.Movie
import com.belosh.movieland.entity.Review
import com.belosh.movieland.entity.User
import com.belosh.movieland.model.Currency
import com.belosh.movieland.model.Role
import com.belosh.movieland.model.SortStrategy
import com.belosh.movieland.service.MovieService
import com.belosh.movieland.service.impl.security.SecurityService
import com.belosh.movieland.service.impl.security.entity.AuthPrincipal
import com.belosh.movieland.service.impl.util.MovieRateService
import com.belosh.movieland.web.controller.MovieController
import com.belosh.movieland.web.security.SecurityContextHolder
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.setup.MockMvcBuilders

import static org.hamcrest.Matchers.hasSize
import static org.hamcrest.core.Is.is
import static org.mockito.Mockito.*
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class MovieControllerTest {

    private MockMvc mockMvc

    @Mock private MovieService mockMovieService
    @Mock private SecurityService mockSecurityService
    @Mock private MovieRateService mockMovieRateService

    @InjectMocks
    private MovieController movieController

    private List<Movie> movies = new ArrayList<>()
    private List<SortStrategy> sortStrategies = new ArrayList<>()

    private Movie expectedMovie
    def defaultMovieId = 1

    @Before
    void setup() {
        MockitoAnnotations.initMocks(this)
        mockMvc = MockMvcBuilders.standaloneSetup(movieController).build()

        Movie firstMovie = new Movie()
        firstMovie.setId(1)
        firstMovie.setNameRussian("Побег из Шоушенка")
        firstMovie.setNameNative("The Shawshank Redemption")
        firstMovie.setYearOfRelease(1994)
        firstMovie.setRating(8.9)
        firstMovie.setPrice(123.45)
        firstMovie.setPicturePath("https://images-na.ssl-images-amazon.com/images/M/MV5BODU4MjU4NjIwNl5BMl5BanBnXkFtZTgwMDU2MjEyMDE@._V1._SY209_CR0,0,140,209_.jpg")
        movies.add(firstMovie)

        Movie secondMovie = new Movie()
        secondMovie.setId(2)
        secondMovie.setNameRussian("Зеленая миля")
        secondMovie.setNameNative("The Green Mile")
        secondMovie.setYearOfRelease(1999)
        secondMovie.setRating(8.9)
        secondMovie.setPrice(134.67)
        secondMovie.setPicturePath("https://images-na.ssl-images-amazon.com/images/M/MV5BMTUxMzQyNjA5MF5BMl5BanBnXkFtZTYwOTU2NTY3._V1._SY209_CR0,0,140,209_.jpg")
        movies.add(secondMovie)

        expectedMovie = firstMovie

        def countryUSA = new Country()
        countryUSA.setId(1)
        countryUSA.setName('США')
        def countryFrance = new Country()
        countryFrance.setId(2)
        countryFrance.setName('Франция')

        def genreDramma = new Genre(1, 'драма')
        def genreCrime = new Genre(2, 'криминал')

        def user = new User()
        user.setId(1)
        user.setNickname("Maks")

        def review = new Review();
        review.setId(1)
        review.setText("Acceptable with bear")
        review.setUser(user)

        def movieGenreList = [genreDramma, genreCrime]
        def movieCountryList = [countryUSA, countryFrance]
        def movieReviewList = [review]

        expectedMovie.setGenres(movieGenreList)
        expectedMovie.setCountries(movieCountryList)
        expectedMovie.setReviews(movieReviewList)
    }

    @Test
    // TODO: Require refactoring: javax/servlet ? ; jsonPath
    void testGetAll() {

        when(mockMovieService.getAll(sortStrategies)).thenReturn(movies)
        mockMvc.perform(get("/movie")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath('$', hasSize(2)))

                .andExpect(MockMvcResultMatchers.jsonPath('$[0].id', is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath('$[0].nameRussian', is('Побег из Шоушенка')))
                .andExpect(MockMvcResultMatchers.jsonPath('$[0].nameNative', is('The Shawshank Redemption')))
                .andExpect(MockMvcResultMatchers.jsonPath('$[0].yearOfRelease', is(1994)))
                .andExpect(MockMvcResultMatchers.jsonPath('$[0].rating', is(8.9d)))
                .andExpect(MockMvcResultMatchers.jsonPath('$[0].price', is(123.45d)))
                .andExpect(MockMvcResultMatchers.jsonPath('$[0].picturePath', is('https://images-na.ssl-images-amazon.com/images/M/MV5BODU4MjU4NjIwNl5BMl5BanBnXkFtZTgwMDU2MjEyMDE@._V1._SY209_CR0,0,140,209_.jpg')))

                .andExpect(MockMvcResultMatchers.jsonPath('$[1].id', is(2)))
                .andExpect(MockMvcResultMatchers.jsonPath('$[1].nameRussian', is('Зеленая миля')))
                .andExpect(MockMvcResultMatchers.jsonPath('$[1].nameNative', is('The Green Mile')))
                .andExpect(MockMvcResultMatchers.jsonPath('$[1].yearOfRelease', is(1999)))
                .andExpect(MockMvcResultMatchers.jsonPath('$[1].rating', is(8.9d)))
                .andExpect(MockMvcResultMatchers.jsonPath('$[1].price', is(134.67d)))
                .andExpect(MockMvcResultMatchers.jsonPath('$[1].picturePath', is('https://images-na.ssl-images-amazon.com/images/M/MV5BMTUxMzQyNjA5MF5BMl5BanBnXkFtZTYwOTU2NTY3._V1._SY209_CR0,0,140,209_.jpg')))

        verify(mockMovieService, times(1)).getAll(sortStrategies)
        verifyNoMoreInteractions(mockMovieService)
    }

    @Test
    void testGetThreeRandom() {
        when(mockMovieService.getThreeRandom()).thenReturn(movies)
        mockMvc.perform(get("/movie/random")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath('$', hasSize(2)))

                .andExpect(MockMvcResultMatchers.jsonPath('$[0].id', is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath('$[0].nameRussian', is('Побег из Шоушенка')))
                .andExpect(MockMvcResultMatchers.jsonPath('$[0].nameNative', is('The Shawshank Redemption')))
                .andExpect(MockMvcResultMatchers.jsonPath('$[0].yearOfRelease', is(1994)))
                .andExpect(MockMvcResultMatchers.jsonPath('$[0].rating', is(8.9d)))
                .andExpect(MockMvcResultMatchers.jsonPath('$[0].price', is(123.45d)))
                .andExpect(MockMvcResultMatchers.jsonPath('$[0].picturePath', is('https://images-na.ssl-images-amazon.com/images/M/MV5BODU4MjU4NjIwNl5BMl5BanBnXkFtZTgwMDU2MjEyMDE@._V1._SY209_CR0,0,140,209_.jpg')))

                .andExpect(MockMvcResultMatchers.jsonPath('$[1].id', is(2)))
                .andExpect(MockMvcResultMatchers.jsonPath('$[1].nameRussian', is('Зеленая миля')))
                .andExpect(MockMvcResultMatchers.jsonPath('$[1].nameNative', is('The Green Mile')))
                .andExpect(MockMvcResultMatchers.jsonPath('$[1].yearOfRelease', is(1999)))
                .andExpect(MockMvcResultMatchers.jsonPath('$[1].rating', is(8.9d)))
                .andExpect(MockMvcResultMatchers.jsonPath('$[1].price', is(134.67d)))
                .andExpect(MockMvcResultMatchers.jsonPath('$[1].picturePath', is('https://images-na.ssl-images-amazon.com/images/M/MV5BMTUxMzQyNjA5MF5BMl5BanBnXkFtZTYwOTU2NTY3._V1._SY209_CR0,0,140,209_.jpg')))

        verify(mockMovieService, times(1)).getThreeRandom()
        verifyNoMoreInteractions(mockMovieService)
    }

    @Test
    void testGetByGenre() {
        int genreId = 1
        when(mockMovieService.getByGenre(genreId, sortStrategies)).thenReturn(movies)
        mockMvc.perform(get("/movie/genre/1")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath('$', hasSize(2)))

                .andExpect(MockMvcResultMatchers.jsonPath('$[0].id', is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath('$[0].nameRussian', is('Побег из Шоушенка')))
                .andExpect(MockMvcResultMatchers.jsonPath('$[0].nameNative', is('The Shawshank Redemption')))
                .andExpect(MockMvcResultMatchers.jsonPath('$[0].yearOfRelease', is(1994)))
                .andExpect(MockMvcResultMatchers.jsonPath('$[0].rating', is(8.9d)))
                .andExpect(MockMvcResultMatchers.jsonPath('$[0].price', is(123.45d)))
                .andExpect(MockMvcResultMatchers.jsonPath('$[0].picturePath', is('https://images-na.ssl-images-amazon.com/images/M/MV5BODU4MjU4NjIwNl5BMl5BanBnXkFtZTgwMDU2MjEyMDE@._V1._SY209_CR0,0,140,209_.jpg')))

                .andExpect(MockMvcResultMatchers.jsonPath('$[1].id', is(2)))
                .andExpect(MockMvcResultMatchers.jsonPath('$[1].nameRussian', is('Зеленая миля')))
                .andExpect(MockMvcResultMatchers.jsonPath('$[1].nameNative', is('The Green Mile')))
                .andExpect(MockMvcResultMatchers.jsonPath('$[1].yearOfRelease', is(1999)))
                .andExpect(MockMvcResultMatchers.jsonPath('$[1].rating', is(8.9d)))
                .andExpect(MockMvcResultMatchers.jsonPath('$[1].price', is(134.67d)))
                .andExpect(MockMvcResultMatchers.jsonPath('$[1].picturePath', is('https://images-na.ssl-images-amazon.com/images/M/MV5BMTUxMzQyNjA5MF5BMl5BanBnXkFtZTYwOTU2NTY3._V1._SY209_CR0,0,140,209_.jpg')))

        verify(mockMovieService, times(1)).getByGenre(genreId, sortStrategies)
        verifyNoMoreInteractions(mockMovieService)
    }

    @Test
    // TODO: Require refactoring: javax/servlet ? ; jsonPath
    void testGetAllWithSortParams() {
        sortStrategies.add(SortStrategy.RATING_DESC)
        when(mockMovieService.getAll(sortStrategies)).thenReturn(movies)
        mockMvc.perform(get("/movie?RATING=DESC")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath('$', hasSize(2)))

                .andExpect(MockMvcResultMatchers.jsonPath('$[0].id', is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath('$[0].nameRussian', is('Побег из Шоушенка')))
                .andExpect(MockMvcResultMatchers.jsonPath('$[0].nameNative', is('The Shawshank Redemption')))
                .andExpect(MockMvcResultMatchers.jsonPath('$[0].yearOfRelease', is(1994)))
                .andExpect(MockMvcResultMatchers.jsonPath('$[0].rating', is(8.9d)))
                .andExpect(MockMvcResultMatchers.jsonPath('$[0].price', is(123.45d)))
                .andExpect(MockMvcResultMatchers.jsonPath('$[0].picturePath', is('https://images-na.ssl-images-amazon.com/images/M/MV5BODU4MjU4NjIwNl5BMl5BanBnXkFtZTgwMDU2MjEyMDE@._V1._SY209_CR0,0,140,209_.jpg')))

                .andExpect(MockMvcResultMatchers.jsonPath('$[1].id', is(2)))
                .andExpect(MockMvcResultMatchers.jsonPath('$[1].nameRussian', is('Зеленая миля')))
                .andExpect(MockMvcResultMatchers.jsonPath('$[1].nameNative', is('The Green Mile')))
                .andExpect(MockMvcResultMatchers.jsonPath('$[1].yearOfRelease', is(1999)))
                .andExpect(MockMvcResultMatchers.jsonPath('$[1].rating', is(8.9d)))
                .andExpect(MockMvcResultMatchers.jsonPath('$[1].price', is(134.67d)))
                .andExpect(MockMvcResultMatchers.jsonPath('$[1].picturePath', is('https://images-na.ssl-images-amazon.com/images/M/MV5BMTUxMzQyNjA5MF5BMl5BanBnXkFtZTYwOTU2NTY3._V1._SY209_CR0,0,140,209_.jpg')))

        verify(mockMovieService, times(1)).getAll(sortStrategies)
        verifyNoMoreInteractions(mockMovieService)
    }

    @Test
    void getById() {
        Currency currency = Currency.UAH

        when(mockMovieService.getById(defaultMovieId, currency)).thenReturn(expectedMovie)
        mockMvc.perform(get("/expectedMovie/" + defaultMovieId)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())

                .andExpect(jsonPath('$.id', is(expectedMovie.id)))
                .andExpect(jsonPath('$.nameRussian', is(expectedMovie.nameRussian)))
                .andExpect(jsonPath('$.nameNative', is(expectedMovie.nameNative)))
                .andExpect(jsonPath('$.yearOfRelease', is(expectedMovie.getYearOfRelease())))
                .andExpect(jsonPath('$.description', is(expectedMovie.description)))
                .andExpect(jsonPath('$.rating', is(expectedMovie.rating)))
                .andExpect(jsonPath('$.price', is(expectedMovie.price)))
                .andExpect(jsonPath('$.picturePath', is(expectedMovie.picturePath)))
                .andExpect(jsonPath('$.genres[0].id', is(expectedMovie.genres.get(0).id)))
                .andExpect(jsonPath('$.genres[0].name', is(expectedMovie.genres.get(0).name)))
                .andExpect(jsonPath('$.genres[1].id', is(expectedMovie.genres.get(1).id)))
                .andExpect(jsonPath('$.genres[1].name', is(expectedMovie.genres.get(1).name)))
                .andExpect(jsonPath('$.countries[0].id', is(expectedMovie.countries.get(0).id)))
                .andExpect(jsonPath('$.countries[0].name', is(expectedMovie.countries.get(0).name)))
                .andExpect(jsonPath('$.countries[1].id', is(expectedMovie.countries.get(1).id)))
                .andExpect(jsonPath('$.countries[1].name', is(expectedMovie.countries.get(1).name)))
                .andExpect(jsonPath('$.reviews[0].id', is(expectedMovie.reviews.get(0).getId().intValue())))
                .andExpect(jsonPath('$.reviews[0].text', is(expectedMovie.reviews.get(0).text)))
                .andExpect(jsonPath('$.reviews[0].user.id', is(expectedMovie.reviews.get(0).user.id)))
        verify(mockMovieService, times(1)).getById(defaultMovieId, currency)
        verifyNoMoreInteractions(mockMovieService)
    }

    @Test
    void getByIdWithCurrency() {
        Currency currency = Currency.USD
        expectedMovie.price = 100

        when(mockMovieService.getById(defaultMovieId, currency)).thenReturn(expectedMovie)
        mockMvc.perform(get("/movie/" + defaultMovieId + "?currency=UsD")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())

                .andExpect(jsonPath('$.id', is(expectedMovie.id)))
                .andExpect(jsonPath('$.nameRussian', is(expectedMovie.nameRussian)))
                .andExpect(jsonPath('$.nameNative', is(expectedMovie.nameNative)))
                .andExpect(jsonPath('$.yearOfRelease', is(expectedMovie.getYearOfRelease())))
                .andExpect(jsonPath('$.description', is(expectedMovie.description)))
                .andExpect(jsonPath('$.rating', is(expectedMovie.rating)))
                .andExpect(jsonPath('$.price', is(expectedMovie.price)))
                .andExpect(jsonPath('$.picturePath', is(expectedMovie.picturePath)))
                .andExpect(jsonPath('$.genres[0].id', is(expectedMovie.genres.get(0).id)))
                .andExpect(jsonPath('$.genres[0].name', is(expectedMovie.genres.get(0).name)))
                .andExpect(jsonPath('$.genres[1].id', is(expectedMovie.genres.get(1).id)))
                .andExpect(jsonPath('$.genres[1].name', is(expectedMovie.genres.get(1).name)))
                .andExpect(jsonPath('$.countries[0].id', is(expectedMovie.countries.get(0).id)))
                .andExpect(jsonPath('$.countries[0].name', is(expectedMovie.countries.get(0).name)))
                .andExpect(jsonPath('$.countries[1].id', is(expectedMovie.countries.get(1).id)))
                .andExpect(jsonPath('$.countries[1].name', is(expectedMovie.countries.get(1).name)))
                .andExpect(jsonPath('$.reviews[0].id', is(expectedMovie.reviews.get(0).getId().intValue())))
                .andExpect(jsonPath('$.reviews[0].text', is(expectedMovie.reviews.get(0).text)))
                .andExpect(jsonPath('$.reviews[0].user.id', is(expectedMovie.reviews.get(0).user.id)))

        verify(mockMovieService, times(1)).getById(defaultMovieId, currency)
        verifyNoMoreInteractions(mockMovieService)
    }

    @Test(expected = IllegalArgumentException.class)
    void getValidateSortParams() {
        LinkedHashMap<String, String> sortParams = new LinkedHashMap<>()
        sortParams.put("RATING", "DESC")
        sortParams.put("PRICE", "ASC")

        List<SortStrategy> sortStrategies = movieController.validateSortParams(sortParams)
        Assert.assertEquals(2, sortStrategies.size())
        Assert.assertEquals(SortStrategy.RATING_DESC, sortStrategies.get(0))
        Assert.assertEquals(SortStrategy.PRICE_ASC, sortStrategies.get(1))

        // Negative case. RATING=ASC is not valid sorting strategy
        sortParams.put("RATING", "ASC")
        movieController.validateSortParams(sortParams)
    }

    @Test
    void testRateMovie() throws Exception {
        User user = new User()
        user.setId(1)
        user.setNickname("TestNickname")
        user.setRole(Role.USER)

        AuthPrincipal authPrincipal = new AuthPrincipal(user)
        SecurityContextHolder.setPrincipal(authPrincipal)

        String json = "{\"rating\":7.2}"
        String uuid = "12345"


        doNothing().when(mockMovieRateService).rateMovie(1, 1, 7.2)

        mockMvc.perform(post("/movie/1/rate")
                .contentType(MediaType.APPLICATION_JSON)
                .header("uuid", uuid)
                .content(json)
        ).andExpect(status().isOk())
    }

    @Test
    void testAddMovie() throws Exception {
        User user = new User()
        user.setId(1)
        user.setNickname("TestNickname")
        user.setRole(Role.ADMIN)

        AuthPrincipal authPrincipal = new AuthPrincipal(user)
        SecurityContextHolder.setPrincipal(authPrincipal)

        String json = "{\n" +
                "     \"nameRussian\": \"Побег из Шоушенка\",\n" +
                "     \"nameNative\": \"The Shawshank Redemption\",\n" +
                "     \"yearOfRelease\": \"1994\",\n" +
                "     \"description\": \"Успешный банкир Энди Дюфрейн обвинен в убийстве собственной жены и ее любовника. Оказавшись в тюрьме под названием Шоушенк, он сталкивается с жестокостью и беззаконием, царящими по обе стороны решетки. Каждый, кто попадает в эти стены, становится их рабом до конца жизни. Но Энди, вооруженный живым умом и доброй душой, отказывается мириться с приговором судьбы и начинает разрабатывать невероятно дерзкий план своего освобождения.\",\n" +
                "     \"price\": 123.45,\n" +
                "     \"picturePath\": \"https://images-na.ssl-images-amazon.com/images/M/MV5BODU4MjU4NjIwNl5BMl5BanBnXkFtZTgwMDU2MjEyMDE@._V1._SY209_CR0,0,140,209_.jpg\",\n" +
                "     \"countries\": [1,2],\n" +
                "     \"genres\": [1,2,3]\n" +
                "}"
        String uuid = "12345"


        doNothing().when(mockMovieService).add(expectedMovie)

        mockMvc.perform(post("/movie/")
                .contentType(MediaType.APPLICATION_JSON)
                .header("uuid", uuid)
                .content(json)
        ).andExpect(status().isOk())
    }

    @Test
    void testUpdateMovie() throws Exception {
        User user = new User()
        user.setId(1)
        user.setNickname("TestNickname")
        user.setRole(Role.ADMIN)

        AuthPrincipal authPrincipal = new AuthPrincipal(user)
        SecurityContextHolder.setPrincipal(authPrincipal)

        String json = "{\n" +
                "     \"nameRussian\": \"Побег из Шоушенка\",\n" +
                "     \"nameNative\": \"The Shawshank Redemption\",\n" +
                "     \"picturePath\": \"https://images-na.ssl-images-amazon.com/images/M/MV5BODU4MjU4NjIwNl5BMl5BanBnXkFtZTgwMDU2MjEyMDE@._V1._SY209_CR0,0,140,209_.jpg\",\n" +
                "     \"countries\": [1,2],\n" +
                "     \"genres\": [1,2,3]\n" +
                "}"
        String uuid = "12345"

        doNothing().when(mockMovieRateService).rateMovie(1, 1, 7.2)

        mockMvc.perform(put("/movie/1")
                .contentType(MediaType.APPLICATION_JSON)
                .header("uuid", uuid)
                .content(json)
        ).andExpect(status().isOk())
    }
}
