package com.belosh.movieland.service;

import com.belosh.movieland.entity.User;
import com.belosh.movieland.report.ReportRequest;

import java.io.InputStream;
import java.util.List;

public interface ReportService {
    void addReportRequest(ReportRequest reportRequest);

    void removeReport(String reportRequest);

    List<ReportRequest> checkStatus(User user);

    InputStream getReport(String filename);

    List<ReportRequest> getReportLinks(User user);
}
