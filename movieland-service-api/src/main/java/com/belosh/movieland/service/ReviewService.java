package com.belosh.movieland.service;

import com.belosh.movieland.entity.Review;

import java.util.List;

public interface ReviewService {

    List<Review> getByMovie(int id);

    void add(Review review);

}
