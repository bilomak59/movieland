package com.belosh.movieland.service;

import com.belosh.movieland.entity.Movie;

import java.util.List;

public interface EnrichService {
    void injectMovieData(Movie movie);
}
