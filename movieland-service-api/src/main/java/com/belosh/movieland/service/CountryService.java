package com.belosh.movieland.service;

import com.belosh.movieland.entity.Country;
import com.belosh.movieland.entity.Genre;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface CountryService {

    Map<Integer, List<Country>> getMovieCountiesMap(Set<Integer> movieIDs);

    List<Country> getByMovie(int movieId);

    List<Country> getAll();
}
