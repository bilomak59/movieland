package com.belosh.movieland.service;

import com.belosh.movieland.entity.Movie;
import com.belosh.movieland.model.Currency;
import com.belosh.movieland.model.SortStrategy;

import java.util.List;

public interface MovieService {

    List<Movie> getAll(List<SortStrategy> sortStrategies);

    List<Movie> getThreeRandom();

    List<Movie> getByGenre(int genreId, List<SortStrategy> sortStrategies);

    Movie getById(int movieId, Currency currency);

    void add(Movie movie);

    void update(Movie movie);

    List<Movie> searchByTitle(String searchWord, int page);

    List<Movie> exportMovies();

    void importMovies(List<Movie> movies);
}
