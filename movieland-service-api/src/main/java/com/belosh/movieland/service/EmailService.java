package com.belosh.movieland.service;

import com.belosh.movieland.report.ReportRequest;

public interface EmailService {
    void sendEmail(ReportRequest reportRequest);
}
