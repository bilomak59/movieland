package com.belosh.movieland.service;

import com.belosh.movieland.entity.User;
import com.belosh.movieland.model.Credentials;

import java.util.Map;
import java.util.UUID;

public interface UserService {

    User getUserByCredentials(Credentials credentials);

}
