package com.belosh.movieland.service;

import com.belosh.movieland.entity.Genre;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface GenreService {

    Map<Integer, List<Genre>> getMovieGenreMap(Set<Integer> movieIDs);

    List<Genre> getAll();

    List<Genre> getByMovie(int movieId);
}
