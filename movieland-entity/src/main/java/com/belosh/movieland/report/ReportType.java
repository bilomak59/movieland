package com.belosh.movieland.report;

import java.util.StringJoiner;

public enum ReportType {
    ALL_MOVIES("allMovies"),
    ADDED_DURING_PERIOD("addedDuringPeriod"),
    TOP_ACTIVE_USERS("topActiveUsers");

    private String name;

    ReportType(String name) {
        this.name = name;
    }

    public static ReportType getByName(String name) {
        for (ReportType reportType : values()) {
            if (reportType.name.equalsIgnoreCase(name)) {
                return reportType;
            }
        }
        throw new IllegalArgumentException(
                String.format("Report type is not recognized: %s. Valid report types: %s", name, getReportTypes()));
    }

    public String getName() {
        return name;
    }

    private static String getReportTypes() {
        StringJoiner stringJoiner = new StringJoiner(", ");
        for (ReportType reportType : values()) {
            stringJoiner.add(reportType.name);
        }
        return stringJoiner.toString();
    }
}
