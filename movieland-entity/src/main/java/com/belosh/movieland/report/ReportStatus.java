package com.belosh.movieland.report;

import java.util.StringJoiner;

public enum ReportStatus {
    NEW("NEW"),
    IN_PROGRESS("IN_PROGRESS"),
    CANCELED("CANCELED"),
    FAILED("FAILED"),
    FINISHED("FINISHED");

    private String name;

    ReportStatus(String name) {
        this.name = name;
    }

    public static ReportStatus getByName(String name) {
        for (ReportStatus reportStatus : values()) {
            if (reportStatus.name.equalsIgnoreCase(name)) {
                return reportStatus;
            }
        }
        throw new IllegalArgumentException(
                String.format("Report status is not recognized: %s. Valid report statuses: %s", name, getReportTypes()));
    }

    public String getName() {
        return name;
    }

    private static String getReportTypes() {
        StringJoiner stringJoiner = new StringJoiner(", ");
        for (ReportStatus reportStatus : values()) {
            stringJoiner.add(reportStatus.name);
        }
        return stringJoiner.toString();
    }
}
