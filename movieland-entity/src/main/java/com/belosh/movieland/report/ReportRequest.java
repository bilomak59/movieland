package com.belosh.movieland.report;

import com.belosh.movieland.entity.User;

import java.time.LocalDate;

public class ReportRequest {
    private String reportId;
    private LocalDate period;
    private ReportType reportType;
    private ReportOutputType reportOutputType;
    private User requester;
    private ReportStatus reportStatus;
    private String reportLink;

    public LocalDate getPeriod() {
        return period;
    }

    public void setPeriod(LocalDate period) {
        this.period = period;
    }

    public ReportType getReportType() {
        return reportType;
    }

    public void setReportType(ReportType reportType) {
        this.reportType = reportType;
    }

    public ReportOutputType getReportOutputType() {
        return reportOutputType;
    }

    public void setReportOutputType(ReportOutputType reportOutputType) {
        this.reportOutputType = reportOutputType;
    }

    public User getRequester() {
        return requester;
    }

    public void setRequester(User requester) {
        this.requester = requester;
    }

    public String getReportId() {
        return reportId;
    }

    public void setReportId(String reportId) {
        this.reportId = reportId;
    }

    public ReportStatus getReportStatus() {
        return reportStatus;
    }

    public void setReportStatus(ReportStatus reportStatus) {
        this.reportStatus = reportStatus;
    }

    public String getReportFileName() {
        String outputType = reportOutputType.getName().toLowerCase();
        return String.format("%s-%s.%s", reportId, reportType, outputType);
    }

    public String getReportLink() {
        return reportLink;
    }

    public void setReportLink(String reportLink) {
        this.reportLink = reportLink;
    }

    @Override
    public String toString() {
        return "ReportRequest{" +
                "reportId='" + reportId + '\'' +
                ", period=" + period +
                ", reportType=" + reportType +
                ", reportOutputType=" + reportOutputType +
                ", requester=" + requester +
                ", reportStatus=" + reportStatus +
                ", reportLink='" + reportLink + '\'' +
                '}';
    }
}
