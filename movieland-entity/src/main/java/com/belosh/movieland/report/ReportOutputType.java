package com.belosh.movieland.report;

import java.util.StringJoiner;

public enum  ReportOutputType {
    PDF("PDF"),
    XLSX("XLSX");

    private String name;

    ReportOutputType(String name) {
        this.name = name;
    }

    public static ReportOutputType getByName(String name) {
        for (ReportOutputType reportOutputType : values()) {
            if (reportOutputType.name.equalsIgnoreCase(name)) {
                return reportOutputType;
            }
        }
        throw new IllegalArgumentException(
                String.format("Report type is not recognized: %s. Valid report types: %s", name, getReportOutputTypes()));
    }

    public String getName() {
        return name;
    }

    private static String getReportOutputTypes() {
        StringJoiner stringJoiner = new StringJoiner(", ");
        for (ReportOutputType reportOutputType : values()) {
            stringJoiner.add(reportOutputType.name);
        }
        return stringJoiner.toString();
    }
}
