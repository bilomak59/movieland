package com.belosh.movieland.model;

public class TotalRating {
    private int votes;
    private double rating;

    public TotalRating(int votes, double rating) {
        this.votes = votes;
        this.rating = rating;
    }

    public int getVotes() {
        return votes;
    }

    public void setVotes(int votes) {
        this.votes = votes;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    @Override
    public String toString() {
        return "TotalRating{" +
                "votes=" + votes +
                ", rating=" + rating +
                '}';
    }
}
