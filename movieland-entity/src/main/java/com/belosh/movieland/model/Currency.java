package com.belosh.movieland.model;

import java.util.StringJoiner;

public enum Currency {

    UAH("UAH"),
    USD("USD"),
    EUR("EUR");

    private String name;

    Currency(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static Currency getCurrencyByName(String name) {
        for (Currency currency : values()) {
            if (currency.getName().equalsIgnoreCase(name)) {
                return currency;
            }
        }
        String message = "Not a valid currency code: " + name + ". Valid codes: " + getCurrencyNames();
        throw new IllegalArgumentException(message);
    }

    private static String getCurrencyNames() {
        StringJoiner stringJoiner = new StringJoiner(",");
        for (Currency currency : values()) {
            stringJoiner.add(currency.name);
        }
        return stringJoiner.toString();
    }
}
