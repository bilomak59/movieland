package com.belosh.movieland.model;

import java.util.StringJoiner;

public enum Role {

    ADMIN("ADMIN"),
    USER("USER"),
    GUEST("GUEST");

    private String name;

    Role(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static Role getRoleByName(String name) {
        for (Role role : values()) {
            if (role.name.equalsIgnoreCase(name)) {
                return role;
            }
        }
        String message = "Not a valid role: " + name + ". Valid roles are: " + getRolesNames();
        throw new IllegalArgumentException(message);
    }

    private static String getRolesNames() {
        StringJoiner stringJoiner = new StringJoiner(",");
        for (Role role : values()) {
            stringJoiner.add(role.name);
        }
        return stringJoiner.toString();
    }
}
