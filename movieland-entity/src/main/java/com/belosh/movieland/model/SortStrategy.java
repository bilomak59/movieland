package com.belosh.movieland.model;

public enum SortStrategy {
    RATING_DESC("RATING", "DESC"),
    PRICE_ASC("PRICE", "ASC"),
    PRICE_DESC("PRICE", "DESC");

    private final String field;
    private final String type;

    SortStrategy(String field, String type) {
        this.field = field;
        this.type = type;
    }

    public static SortStrategy getSortStrategy(String field, String type) {
        for (SortStrategy sortStrategy : values()) {
            if (sortStrategy.field.equalsIgnoreCase(field) &&
                    sortStrategy.type.equalsIgnoreCase(type)) {
                return sortStrategy;
            }
        }
        throw new IllegalArgumentException("Not a valid sort request: " + field + " = " + type);
    }

    public String getField() {
        return field;
    }

    public String getType() {
        return type;
    }
}
