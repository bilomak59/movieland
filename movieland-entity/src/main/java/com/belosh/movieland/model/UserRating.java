package com.belosh.movieland.model;

public class UserRating {
    private int userId;
    private double rating;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    @Override
    public String toString() {
        return "UserRating{" +
                "userId=" + userId +
                ", rating=" + rating +
                '}';
    }
}
